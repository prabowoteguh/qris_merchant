"use strict";

const async = require('async');
const checkMerchant = require('../controllers/checkMerchant.js');
const merchantUpdate = require('../controllers/merchant/update.js');

/**
* @type: service
* @req: merchant_id, status
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						service: "updateStatus"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_code);

			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, true);
		},
		function checkingMerchant (index, callback) {
			checkMerchant(APP, {
				body: {
					merchant_id: req.body.merchant_code
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: {},
						info: {
							service: 'updateStatus'
						}
					});

				callback(null, result.data);
			});
		},
		function updatingMerchant (merchant, callback) {
			// Wait store pro
			merchantUpdate(APP, {
				body: {
					dataQuery: {
						code: req.body.merchant_code,
						status: 0
					},
					dataUpdate: {
						status: 2,
						note: req.body.note,
						approval_by: req.body.action_by,
						approval_at: APP.db.Sequelize.fn('NOW'),
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				callback(null, {
					merchant_nama 		: merchant.merchant_nama,
					merchant_nama_pic 	: merchant.merchant_nama_pic,
					email 				: merchant.email,
					note 				: merchant.note,
					status 				: 2
				});
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'MERCHANT_REJECT_SUCCESS',
				data: data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};