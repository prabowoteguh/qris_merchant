	"use strict";

const async = require('async');
const merchantGet = require('../controllers/merchant/get.js');

/**
* @type: function
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						function: "checkCode"
					}
				});

			var validateMerchantCode = APP.validation.true(req.body.merchant_code);

			if (validateMerchantCode !== true) return callback(validateMerchantCode);

			callback(null, true);
		},
		function gettingMerchantByCode (index, callback) {
			merchantGet(APP, {
				body: {
					msisdn: req.body.merchant_code,
					take: 1,
					limit: 0
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback(result);

				callback(null, result);
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data[0]
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};