"use strict";

const async = require('async');
const vascommkit = require('vascommkit');
const moment = require('moment');
const updateMerchantTransaction = require('../controllers/merchantTransaction/update.js');
const detailMerchantTransaction = require('../controllers/merchantTransactionDetail/get.js');
const emoneyServices = require('../config/emoneyServices.json');
const checkVa = require('../controllers/checkVa.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.emoney_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "emoney_va"
					},
					info: {
						service: "purchaseReversal"
					}
				});

			if (!req.body.poin) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "poin"
					},
					info: {
						service: "purchaseReversal"
					}
				});

			var validateEmoneyVa = APP.validation.true(req.body.emoney_va);

			if (validateEmoneyVa !== true) return callback(validateEmoneyVa);

			if (req.body.type) {
				if (req.body.type !== 'split_bill' && req.body.type !== 'reversal' && req.body.type !== 'source') {
					if (!req.body.pin) return callback({
							code: 'MISSING_KEY',
							data: {
								missing_parameter: "pin"
							},
							info: {
								service: "purchaseReversal"
							}
						});

					var validatePin = APP.validation.true(req.body.pin);

					if (validatePin !== true) return callback(validatePin);
				}
			} else {
				if (!req.body.pin) return callback({
						code: 'MISSING_KEY',
						data: {
							missing_parameter: "pin"
						},
						info: {
							service: "purchaseReversal"
						}
					});

				var validatePin = APP.validation.true(req.body.pin);

				if (validatePin !== true) return callback(validatePin);
			}

			callback(null, {});
		},
		function checkingVaAccount (data, callback) {
			APP.request.emoney(emoneyServices.checkAccount, {
				emoney_va: req.body.emoney_va
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== '00') return callback(result);

				data.account = result.data;

				callback(null, data);
			});
		},
		function prefix (data, callback) {
			data.transaction_id = req.body.transaction_id;
			data.amount = req.body.amount;
			data.merchant_va = req.body.merchant_va;

			callback(null, data);
		},
		function dataAmountDebeted (data, callback) {
			data.amountDebeted = String(Number(data.amount) - Number(req.body.poin));

			callback(null, data);
		},
		function checkingMerchantVa (data, callback) {
			checkVa(APP, {
				body: {
					merchant_va: data.merchant_va
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: req.body,
							info: {
								service: 'purchaseReversal'
							}
						});

					return callback(err);
				}

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: req.body,
						info: {
							service: 'purchaseReversal'
						}
					});

				data.merchant_reff_id = result.data.id;

				callback(null, data);
			});
		},
		function generateReffnum (data, callback) {
			data.reffnum = APP.generator.generateReffnum();

			callback(null, data);
		},
		function emoneyTFE2E (data, callback) {
			if (req.body.type && req.body.type === 'source') {
				APP.request.emoney(emoneyServices.creditSource, {
					jenis_source: req.body.jenis_source,
					card_num: req.body.card_num,
					account_num: req.body.account_num,
					amount: data.amountDebeted
				}, (err, result) => {
					if (err) {
						err.code = 'PURCHASE_PAYMENT_REVERSAL_FAILED';

						return callback(err);
					};

					if (result && result.code !== '00') return callback(result);

					data.journal = null;
					data.emoney_va = req.body.emoney_va;
					data.amount = data.amountDebeted;

					return callback(null, data);
				});
			} else {
				APP.request.emoney(emoneyServices.e2e, {
					from_va: data.merchant_va,
					to_va: req.body.emoney_va,
					amount: data.amountDebeted,
					narration: req.body.narration || "Merchant purchase payment reversal",
					pin: req.body.pin,
					type: req.body.type
				}, (err, result) => {
					if (err) {
						err.code = 'PURCHASE_PAYMENT_REVERSAL_FAILED';

						return callback(err);
					};

					if (result && result.code !== '00') return callback(result);

					data.journal = result.data.journal;
					data.emoney_va = result.data.emoney_va || req.body.emoney_va;
					data.amount = result.data.amount;

					return callback(null, data);
				});
			}
		},
		function updateTransactionStatus (data, callback) {
			const paymentAt = APP.db.Sequelize.fn('NOW');
			const paymentAtMoment = moment();

			updateMerchantTransaction(APP, {
				body: {
					dataQuery: {
						id: data.transaction_id,
						status: '1'
					},
					dataUpdate: {
						status: '0'
					}
				}
			}, (err, result) => {
				if (err) {
					if (err && err.code === 'ERR_NOTHING') return callback({
							code: 'INVALID_TRANSACTION',
							data: req.body
						});

					return callback(err);
				};

				if (result && result.code !== 'MERCHANT_TRANSACTION_UPDATE_SUCCESS') return callback({
						code: 'INVALID_TRANSACTION',
						data: req.body
					});

				data.datetime = paymentAtMoment;

				callback(null, data);
			});
		},
		function getProduct (data, callback) {
			detailMerchantTransaction(APP, {
				body: {
					transactions_merchant_id:  data.transaction_id
				}
			}, (err, product) => {
				callback(null, data, product);
			});
		},
		function output (data, product, callback) {
			callback(null, {
				code: 'PURCHASE_PAYMENT_RERVERSAL_SUCCESS',
				data: req.body
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};