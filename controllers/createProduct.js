"use strict";

const async = require('async');

/**
* @type: service
* @req: merchant_id, name, price, narration, image, type, qty,
* promo_status, active_status, created_by
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "createProduct"
					}
				});

			req.body.merchant_id = req.body.merchant_id.toUpperCase();

			var validateName = APP.validation.string(req.body.name);
			var validatePrice = APP.validation.amount(req.body.price);

			if (validateName !== true) return callback(validateName);
			if (validatePrice !== true) return callback(validatePrice);

			
			req.body.name = (req.body.name).replace(/\b\w/g, l => l.toUpperCase());
			
			callback(null, true);
		},
		function insertingProducts (index, callback) {
			const params = APP.queries.storePro('create_product', {
				body: {
					merchant_code: req.body.merchant_id,
					product_type_id: req.body.type,
					name: req.body.name,
					narration: req.body.narration,
					price: req.body.price,
					quantity: req.body.qty,
					promo_status: req.body.promo_status,
					active_status: req.body.active_status,
					image: req.body.image,
					recommended: req.body.recommended,
					action_by: req.body.action_by,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(product => {
				if (product.length < 1) return callback(null, {
						code: 'CREATE_PRODUCT_FAILED',
						data: req.body,
						info: {
							function: "createProduct"
						}
					});

				if (product[0].code && product[0].code !== '00') return callback({
						code: product[0].code,
						message: product[0].message,
						data: req.body,
						info: {
							function: "createProduct"
						}
					});

				callback(null, {
					code: 'CREATE_PRODUCT_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "createProduct"
					}
				});
			});
		},
		function output (product, callback) {
			callback(null, product);
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};