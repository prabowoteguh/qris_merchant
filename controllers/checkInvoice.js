	"use strict";

const async = require('async');
const checkMerchant = require('../controllers/checkMerchant.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.invoice) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "invoice"
					},
					info: {
						function: "checkInvoice"
					}
				});

			var validateInvoice = APP.validation.true(req.body.invoice);

			if (validateInvoice !== true) return callback(validateInvoice);

			callback(null, {});
		},
		function checkingMerchant (data, callback) {
			checkMerchant(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: req.body
						});

					return callback(err);
				}

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: req.body
					});

				callback(null, data);
			});
		},
		function checkingInvoiceExists (data, callback) {
			const params = APP.queries.storePro('check_invoice', {
				body: {
					invoice: req.body.invoice
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(reply => {
				if (reply[0].code && reply[0].code !== '00') return callback({
						code: 'INVALID_INVOICE',
						data: {
							q_code: reply[0].code,
							q_message: reply[0].message
						},
						info: {
							service: 'checkInvoice'
						}
					});

				data.invoice = {};
				data.invoice.status = String(reply[0].status);
				data.invoice.amount = String(reply[0].nominal);

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'INVALID_INVOICE',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'checkInvoice'
					}
				});
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'CHECK_INVOICE_SUCCESS',
				data: data.invoice
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};