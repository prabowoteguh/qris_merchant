"use strict";

const async = require('async');
const vascommkit = require('vascommkit');

/**
* @type: service
* @req: invoice, merchant_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.invoice) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "invoice"
					},
					info: {
						service: "purchaseInvoiceInquiry"
					}
				});

			// if (!req.body.merchant_id) return callback({
			// 		code: 'MISSING_KEY',
			// 		data: {
			// 			missing_parameter: "merchant_id"
			// 		},
			// 		info: {
			// 			service: "purchaseInvoiceInquiry"
			// 		}
			// 	});

			var validateInvoice = APP.validation.true(req.body.invoice);
			// var validateMerchantId = APP.validation.true(req.body.merchant_id);

			if (validateInvoice !== true) return callback(validateInvoice);
			// if (validateMerchantId !== true) return callback(validateMerchantId);

			// req.body.merchant_id = req.body.merchant_id.toUpperCase();

			callback(null, {});
		},
		function checkingInvoiceExists (data, callback) {
			const params = APP.queries.storePro('merchant_invoice_check', {
				body: {
					invoice: req.body.invoice
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(reply => {
				console.log(reply)
				if (reply[0].code && reply[0].code !== '00') return callback({
						code: 'INVALID_INVOICE',
						data: {
							q_code: reply[0].code,
							q_message: reply[0].message
						},
						info: {
							service: 'purchaseInvoiceInquiry'
						}
					});

				data.transaction_id = reply[0].id;
				data.amount = String(reply[0].nominal);
				data.account_number = reply[0].account_number;
				data.merchant_code = reply[0].merchant_code;
				data.merchant_id = reply[0].merchant_id;
				data.merchant_name = reply[0].name;
				data.merchant_va = reply[0].merchant_va;
				data.account_name = reply[0].account_name;

				// if (req.body.merchant_id !== data.merchant_id) return callback({
				// 		code: 'INVALID_INVOICE',
				// 		data: {
				// 			invoice: req.body.invoice,
				// 			merchant_id: req.body.merchant_id
				// 		}
				// 	});

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'INVALID_INVOICE',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'purchaseInvoiceInquiry'
					}
				});
			});
		},
		// Enhancement Get Data Merchant
		function getDataMerchant(data, callback) {		
			var query = APP.queries.rawQueris('check_merchant', {
                body: {
                    merchant_id: data.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
				data.merchant = rows[0][0]
                callback(null, data);
            })
		},
		function output (data, callback) {
			callback(null, {
				code: 'PURCHASE_INVOICE_INQUIRY_SUCCESS',
				data: {
					transaction_id: data.transaction_id,
					invoice: req.body.invoice,
					merchant_name: data.merchant_name,
					merchant_va: data.merchant_va,
					merchant_id: data.merchant_id,
					merchant_address: data.merchant.address,
					merchant_kelurahan: data.merchant.kelurahan,
					merchant_kecamatan: data.merchant.kecamatan,
					merchant_city: data.merchant.city,
					merchant_province: data.merchant.province,
					merchant_zipcode: data.merchant.zipcode,
					merchant_lat: data.merchant.lat,
					merchant_long: data.merchant.long,
					merchant_code: data.merchant_code,
					datetime: vascommkit.time.now(),
					amount: data.amount,
					currency: (data.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."),
					status: '0'
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};