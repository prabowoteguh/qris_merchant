"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.select('business_type', req, APP.models);

	params.order = [['name', 'ASC']]
	params.limit = 100

	APP.models.mysql.businessType.findAll(params).then((rows) => {
		return callback(null, {
			code: (rows && (rows.length > 0)) ? 'FOUND' : 'NOT_FOUND',
			data: rows,
			info: {
				dataCount: rows.length
			}
		});
	}).catch((err) => {
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};