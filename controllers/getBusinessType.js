"use strict";

const async = require('async');
const businessType = require('../controllers/businessType/get.js');

/**
* @type: service
* @req: limit, offset
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, true);
		},
		function gettingList (index, callback) {
			businessType(APP, {
				body: {}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'NOT_FOUND',
						data: {
							rows: [],
							count: 0
						}
				});

				callback(null, result.data);
			});
		},
		function output (listBusinessType, callback) {
			callback(null, {
				code: 'FOUND',
				data: {
					rows: listBusinessType,
					count: listBusinessType.length
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};