"use strict";

const async = require('async');
const _ = require('lodash');

// Inquiry Bank Code
exports.inquiryBankCode = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            callback(null, true)
        },
        function inquiryData (data, callback) {
            let query;
            if (req.body.bank_id !== '' && req.body.bank_name == '') {
                query = APP.queries.rawQueris('check_bank_id', {
                    body: {
                        bank_id: req.body.bank_id
                    }
                });
            } else if (req.body.name !== '' && req.body.bank_id == '') {
                query = APP.queries.rawQueris('check_bank_name', {
                    body: {
                        bank_name: req.body.bank_name
                    }
                });
            } else if (req.body.name !== '' && req.body.name !== '') {
                query = APP.queries.rawQueris('check_bank_id_name', {
                    body: {
                        bank_id: req.body.bank_id,
						bank_name: req.body.bank_name
                    }
                });
            } else {
                callback({ 
                    code: 'FAILED',
                    message: 'Key request not found.'
                })
            }

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    output = {
                        code: 'FAILED',
                        message: 'Bank Name tidak ditemukan.'
                    }
    
                    callback(output)
                } else {
                    const raw = rows[0]
                    const rowCustom = Object.keys(raw).map(e => {
                        const all = {
                            "bank_code_format": '93600' + raw[e].bank_code						
                        }

                        return all;
                    });

                    callback(null, {
                        code: 'FOUND',
                        data: _.merge(rows[0][0], rowCustom[0])
                    })
                }
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}

// List All Bank
exports.listBankCode = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            callback(null, true)
        },
        function getData (data, callback) {
            let query = {
                raw: true,
                limit: parseInt(req.body.limit) || null,
                offset: parseInt(req.body.offset) || null
            };

            query.where = {};

            if (req.body.bank_id) {
                query.where.id = req.body.bank_id
            }

            APP.models.mysql.bankCode.findAll(query).then(rows => {
                if (rows.length < 1) {
                    callback(null, {
                        code: 'NOT_FOUND'
                    })
                } else {
                    if (req.body.bank_id !== '') {
                        callback(null, {
                            code: 'FOUND',
                            data: rows[0]
                        })
                    } else {
                        callback(null, {
                            code: 'FOUND',
                            data: {
                                rows: rows,
                                count: rows.length
                            }
                        })
                    }
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}

exports.updateBankGlobalUniqueIdentifier = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            if (!req.body.bank_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "bank_id" } });
        
            callback(null, true)
        },
        function update (data, callback) {
            let values = {}   

            let whereVal = {
                where: {
                    id: req.body.bank_id
                }
            }

            if (req.body.global_unique_identifier) {
                values = {
                    global_unique_identifier: req.body.global_unique_identifier
                }
            }

            APP.models.mysql.bankCode.update(values, whereVal).then(result => {
                output = {
                    code: 'OK_UPDATE'
                }
    
                callback(null, output)
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}