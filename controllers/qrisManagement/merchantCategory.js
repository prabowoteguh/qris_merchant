"use strict";

const async = require('async');

// List Merchant Category
exports.listMerchantCategory = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            callback(null, true)
        },
        function list (data, callback) {
            let query = {
                limit: parseInt(req.body.limit) || null,
                offset: parseInt(req.body.offset) || null
            };

            query.where = {};

            if (req.body.merchant_category_id) {
                query.where.id = req.body.merchant_category_id
            }

            if (req.body.order_by) {
                switch(req.body.order) {
                    case 'mcc_asc':
                        query.order =  [['mcc', 'ASC']]
                        break;
                    case 'mcc_desc':
                        query.order =  [['mcc', 'DESC']]
                        break;
                    case 'merchant_category_asc':
                        query.order =  [['merchant_category', 'ASC']]
                        break;
                    case 'merchant_category_desc':
                        query.order =  [['merchant_category', 'DESC']]
                        break;
                }
            }

            APP.models.mysql.merchantCategory.findAll(query).then(rows => {
                if (rows.length < 1) {
                    callback(null, {
                        code: 'NOT_FOUND'
                    })
                } else {
                    callback(null, {
                        code: 'FOUND',
                        data: req.body.merchant_category_id !== '' ? rows[0] : { rows: rows }
                    })
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}