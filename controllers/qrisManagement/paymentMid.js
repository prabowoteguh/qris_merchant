"use strict";

const async = require('async');
const paymentUpdate = require('./paymentUpdate');
const notifController = require('../notification/pushNotification');
const email = require('../mail/transaksi');
const moment = require('moment');
const randomstring = require('randomstring');

const randomStr = randomstring.generate({
	length: 2,
	charset: 'numeric'
});

exports.flaggingPayment = (APP, req, callback) => {
    var statusData = '';
    var statusDataMerchant = '';
    var staticPayment = '';
    var merchantAcquirer = '';
    var merchantAcquirerDinamic = '';
    var merchantTr = '';
    var staticData = {};
    var transactionIdLog = '';
    var statucCanceled = '';

    async.waterfall([
        function init (callback) {
            callback(null, true)
        },
        function checkMerchantId (data, callback) {
            // Static
            if (req.body.additionalData.referenceLabel == null) {
                var query = APP.queries.rawQueris('check_merchant_pan_merchant_id_acquirer', {
                    body: {
                        merchant_pan: req.body.merchantPan,
						merchant_id_acquirer: req.body.merchantId
                    }
                });
    
                APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                    if (rows[0].length < 1) {
                        callback({
                            code: 'INVALID_MERCHANT'
                        })
                    } else { 
                        merchantAcquirer = rows[0][0];
                        callback(null, data)
                    }
                })
            // Dinamic
            } else {
                var query = APP.queries.rawQueris('get_merchant_id_acquirer', {
                    body: {
                        merchant_id: req.body.merchantId
                    }
                });
    
                APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                    if (rows[0].length < 1) {
                        callback({
                            code: 'INVALID_MERCHANT'
                        })
                    } else {
                        merchantAcquirerDinamic = rows[0][0]
                        callback(null, data)
                    }
                })
            }
        },
        // Check Amount if dinamic
        function checkAmount (data, callback) {
            if (req.body.additionalData.referenceLabel == null) {
                staticPayment = '1';
                return callback(null, data)
            }

            var query = APP.queries.rawQueris('check_reff_amount', {
                body: {
                    reference_id: req.body.additionalData.referenceLabel
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) callback({
                    code: '02',
                    message: 'Request Id tidak ditemukan'
                }) 
                
                if (rows[0][0].nominal !== Number(req.body.transactionAmount)) {
                    callback({
                        code: 'INVALID_AMOUNT'
                    })
                } else {
                    callback(null, data)
                }
            })
        },
        function statusMerchant (data, callback) {
            if (req.body.additionalData.referenceLabel == null) {
                statusData = '1';
                return callback(null, data)
            }

            var query = APP.queries.rawQueris('transaction_merchant_status', {
                body: {
                    request_id: req.body.additionalData.referenceLabel
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    statusData = '0'
                } else {
                    if (rows[0][0].status == 1) {
                        statusDataMerchant = '0'
                        statusData = '0'
                    } else {
                        statusDataMerchant = '1'
                        statusData = '1'
                    }
                }
                callback(null, data)
            })
        },
        function checkRequest (data, callback) {
            let requestData = {
                referenceId: req.body.referenceId,
                requestDatetime: req.body.requestDatetime,
                transactionDate: req.body.transactionDate,
                transactionTime: req.body.transactionTime,
                acquirerName: req.body.acquirerName,
                merchantName: req.body.merchantName,
                merchantCity: req.body.merchantCity,
                merchantPan: req.body.merchantPan,
                merchantId: req.body.merchantId,
                terminalId: req.body.terminalId,
                issuerName: req.body.issuerName,
                customerName: req.body.customerName,
                customerPan: req.body.customerPan,
                systemTraceAuditNumber: req.body.systemTraceAuditNumber,
                retrievalReferenceNumber: req.body.retrievalReferenceNumber,
                transactionAmount: req.body.transactionAmount
            }

            requestData.additionalData = {
                billNumber: req.body.additionalData.billNumber || null,
                mobileNumber: req.body.additionalData.mobileNumber || null,
                storeLabel: req.body.additionalData.storeLabel || null,
                loyaltyNumber: req.body.additionalData.loyaltyNumber || null,
                referenceLabel: req.body.additionalData.referenceLabel || null,
                customerLabel: req.body.additionalData.customerLabel || null,
                terminalLabel: req.body.additionalData.terminalLabel || null,
                purposeOfTransaction: req.body.additionalData.purposeOfTransaction || null,
                additionalCustomerDataRequest: req.body.additionalData.additionalCustomerDataRequest || null,
                proprietaryData: req.body.additionalData.proprietaryData || null
            }
            
            callback(null, data, requestData)
        },
        function checkPaymentCanceled (data, requestData, callback) {
            if (req.body.additionalData.referenceLabel == null) {
                return callback(null, data, requestData)
            } 

            var query = APP.queries.rawQueris('transaction_merchant_status', {
                body: {
                    request_id: req.body.additionalData.referenceLabel
                }
            });

			APP.db.sequelize.query(query.query, query.bind).then(rows => {
                if (rows[0][0].status == 2) {
                    statucCanceled = '1';
                    callback(null, data, requestData)
                } else {
                    callback(null, data, requestData)
                }
            })
        },
        function insertLog (data, requestData, callback) {
            const query = APP.queries.storePro('payment_log_insert', {
				body: {
					reference_id: requestData.referenceId,
                    request_datetime: requestData.requestDatetime,
                    transaction_date: requestData.transactionDate,
                    transaction_time: requestData.transactionTime,
                    acquirer_name: requestData.acquirerName,
                    merchant_name: requestData.merchantName,
                    merchant_city: requestData.merchantCity,
                    merchant_pan: requestData.merchantPan,
                    merchant_id: requestData.merchantId,
                    terminal_id: requestData.terminalId,
                    issuer_name: requestData.issuerName,
                    customer_name: requestData.customerName,
                    customer_pan: requestData.customerPan,
                    system_trace_audit_number: requestData.systemTraceAuditNumber,
                    retrieval_reference_number: requestData.retrievalReferenceNumber,
                    transaction_amount: requestData.transactionAmount,
                    bill_number: requestData.additionalData.billNumber,
                    mobile_number: requestData.additionalData.mobileNumber,
                    store_label: requestData.additionalData.storeLabel,
                    loyalty_number: requestData.additionalData.loyaltyNumber,
                    reference_label: requestData.additionalData.referenceLabel,
                    customer_label: requestData.additionalData.customerLabel,
                    terminal_label: requestData.additionalData.terminalLabel,
                    purpose_of_transaction: requestData.additionalData.purposeOfTransaction,
                    additional_customer_data_request: requestData.additionalData.additionalCustomerDataRequest,
                    proprietary_data: requestData.additionalData.proprietaryData,
                    status: statucCanceled == '1' ? '0' : statusData
				}
            });
            
			APP.db.sequelize.query(query.query, query.bind).then(rows => {
                let transaction = {}
                transactionIdLog = rows[0].id_log;

                if (statucCanceled == '1') {
                    callback({ 
                        code: 'INVALID_TRANSACTION_PAYMENT_CANCELED',
                        data: requestData
                    })
                }

                callback(null, data, requestData, transaction)
            }).catch(err => {
                let output = {
                    code: 'ERR_DATABASE',
                    data: err
                }
                console.log(err)
                callback(output)
            })
        },
        function updatePayment (data, requestData, transaction, callback) {
            if (statusData == 0) {
                return callback(null, data, requestData, transaction)
            }

            if (requestData.additionalData.referenceLabel == null) {
                return callback(null, data, requestData, transaction)
            } else {
                paymentUpdate(APP, {
                    body: {
                        request_id: requestData.additionalData.referenceLabel
                    }
                }, (err, result) => {
                    if (err) return callback(err);

                    if (result && result.code !== '00') return callback({
                        code: 'INVALID_TRANSACTION_QRIS'
                    });

                    transaction = result.data
                    callback(null, data, requestData, transaction);
                })
            }
        },
        function getTrLog (data, requestData, transaction, callback) {
            var query = APP.queries.rawQueris('get_log_transactions_merchant', {
                body: {
                    reference_id: requestData.referenceId
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                merchantTr = rows[0][0];

                callback(null, data, requestData, transaction)
            })
        },
        function createTransactionMerchant (data, requestData, transaction, callback) {
            if (req.body.additionalData.referenceLabel == null) { 
                const query = APP.queries.storePro('insert_transaction', {
                    body: {
                        requestId: moment().format('YYYYMMDDHHmmss').concat(randomStr),
                        account_number: merchantTr.code,
                        merchant_va: merchantAcquirer.merchant_va,
                        wallet_type_code: 'SOF01',
                        transactions_type_code: 'TRT012',
                        biller_type: '',
                        biller_code: merchantTr.merchant_pan,
                        biller_name: merchantTr.customer_name,
                        nominal: merchantTr.transaction_amount,
                        no_jurnal: '12345',
                        note: 'Pembayaran',
                        fee: '0',
                        admin_fee: '0',
                        data: '',
                        status: '1',
                        ip_address: '127.0.0.1'
                    }
                });
                
                APP.db.sequelize.query(query.query, query.bind).then(transaction => {
                    if (transaction[0].code && transaction[0].code !== '00') return callback({
                        code: transaction[0].code,
                        message: transaction[0].message,
                        data: {
                            q_code: transaction[0].code,
                            q_message: transaction[0].message
                        },
                        info: {
                            service: 'paymentFlagging'
                        }
                    });
    
                    staticData = {
                        invoice: transaction[0].invoice,
                        transaction_id: transaction[0].var_transactions_merchant_id,
                        requestId: transaction[0].requestId
                    }
    
                    callback(null, data, requestData, transaction);
                }).catch(err => {
                    callback({
                        code: 'INSERT_TRANSACTION_FAILED',
                        data: {},
                        info: {
                            err: JSON.stringify(err),
                            service: 'paymentFlagging'
                        }
                    });
                });
            } else {
                callback(null, data, requestData, transaction);
            }
        },
        function updateMerchantPayment (data, requestData, transaction, callback) {
            if (req.body.additionalData.referenceLabel == null) { 
                var query = APP.queries.rawQueris('update_payment_at_transaction', {
                    body: {
                        payment_at: moment().format('YYYY-MM-DD HH:mm:ss'),
						transaction_id: staticData.transaction_id
                    }
                });

                APP.db.sequelize.query(query.query, query.bind).then(rows => {
                    callback(null, data, requestData, transaction);
                })
            } else {
                return callback(null, data, requestData, transaction);    
            }
        },
        function updateLogTransactionStatic (data, requestData, transaction, callback) {
            if (req.body.additionalData.referenceLabel == null) {
                const query = APP.queries.storePro('update_transaction_merchant_log', {
                    body: {
                        transaction_log_id: transactionIdLog,
                        transaction_merchant_id: staticData.transaction_id,
                        reference_label: staticData.requestId
                    }
                });
                
                APP.db.sequelize.query(query.query, query.bind).then(rows => {
                    callback(null, data, requestData, transaction)
                })
            } else {
                return callback(null, data, requestData, transaction)
            }
        },
        function pushNotificationStatic (data, requestData, transaction, callback) {
            if (statusData == 0) {
                return callback(null, data, requestData, transaction)
            }

            if (req.body.additionalData.referenceLabel == null) {
                req.pushnotif = {
                    referrer_id: staticData.transaction_id,
                    emoney_va: merchantAcquirer.account_number,
                    account_number: merchantAcquirer.code,
                    type: 1,
                    title: 'Bayar Merchant',
                    content: 'Transaksi Pembelian invoice '+ staticData.invoice || '' +' Sebesar Rp '+ (merchantTr.transaction_amount || 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' Berhasil di Bayar',
                    ip_address: merchantAcquirer.ip_address
                };
    
                notifController.save(req.APP, req, (err, result) => {});
                
                callback(null, data, requestData, transaction)
            } else {
                return callback(null, data, requestData, transaction)
            }
        },
        function pushNotification (data, requestData, transaction, callback) {
            if (req.body.additionalData.referenceLabel == null) {
                return callback(null, data, requestData, transaction)
            }

            if (statusData == 0) {
                return callback(null, data, requestData, transaction)
            }

            req.pushnotif = {
                referrer_id: transaction.id,
                emoney_va: transaction.merchant_va || transaction.requestId,
                account_number: transaction.account_number,
                type: 1,
                title: 'Bayar Merchant',
                content: 'Transaksi Pembelian invoice '+ transaction.invoice || '' +' Sebesar Rp '+ (transaction.nominal || 0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' Berhasil di Bayar',
                ip_address: transaction.ip_address
            };

            notifController.save(req.APP, req, (err, result) => {});
            
            callback(null, data, requestData, transaction)
        },
        function sendMailStatic (data, requestData, transaction, callback) {
            if (statusData == 0) {
                return callback(null, data, requestData, transaction)
            }

            if (req.body.additionalData.referenceLabel == null) { 
                let payload = {
                    email: merchantAcquirer.email,
                    nama: merchantAcquirer.merchant_name,
                    created_at: merchantTr.created_at,
                    no_reff: merchantTr.reference_id,
                    biller_name: merchantTr.customer_name,
                    emoney_va: merchantAcquirer.account_number,
                    total: merchantTr.transaction_amount,
                    product: '', 
                    deskripsi: 'Pembayaran ' + merchantTr.issuer_name + '-' + merchantTr.merchant_city +' Sukses',
                    user_merchant: 'AO1' // Cashier
                }
    
                email.send(req, payload, (err, result) => { console.log(result) })
                callback(null, data, requestData, transaction);
            } else {
                return callback(null, data, requestData, transaction);
            }
        },
        function sendMail (data, requestData, transaction, callback) {
            if (req.body.additionalData.referenceLabel == null) {
                return callback(null, data, requestData, transaction)
            }

            if (statusData == 0) {
                return callback(null, data, requestData, transaction)
            }

            let payload = {
                email: transaction.email,
                nama: merchantAcquirerDinamic.merchant_name,
                created_at: transaction.created_at,
                no_reff: merchantTr.reference_id,
                biller_name: merchantTr.customer_name,
                emoney_va: transaction.merchant_va || transaction.account_number,
                total: transaction.nominal,
                product: transaction.products, 
                deskripsi: 'Pembayaran Sukses',
                user_merchant: 'AO1'
            }

            email.send(req, payload, (err, result) => { console.log(result) })
            callback(null, data, requestData, transaction);
        },
        function output (data, requestData, transaction, callback) {
            if (staticPayment == '1') {
                return callback(null, {
                    code: 'APPROVED_PAYMENT',
                    data: requestData
                })
            }

            callback(null, {
                code: statusDataMerchant == '1' ? 'APPROVED_PAYMENT' : 'INVALID_TRANSACTION_PAYMENT',
                data: requestData
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}

exports.getlogPayment = (APP, req, callback) => {
    async.waterfall([
        function init (callback) {
            callback(null, {})
        },
        function getData (data, callback) {
            const query = APP.queries.storePro('get_payment_log', {
				body: {
					reference_id: req.body.reference_id,
                    offset: req.body.offset,
                    limit: req.body.limit
				}
            });
            
            APP.db.sequelize.query(query.query, query.bind).then(rows => {
                callback(null, {
                    code: '00',
                    data: {
                        rows: rows,
                        count: rows.length
                    }
                })
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}