"use strict";

const async = require('async');
const merchantTransactionDetailGet = require('../merchantTransactionDetail/get');

module.exports = (APP, req, callback) => {
    async.waterfall([
        function init (callback) {
            callback(null, {})
        },
        function getDataInvoice (data, callback) {
            var query = APP.queries.storePro('check_invoice', {
                body: {
                    request_id: req.body.request_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                let transaction = rows
                callback(null, data, transaction)
            })
        },
		function getDataMerchant(data, transaction, callback) {
			var query = APP.queries.rawQueris('get_merchant_by_user', {
                body: {
                    username: transaction[0].account_number
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => {
                transaction[0].email = rows[0][0].email
                transaction[0].merchant_va = rows[0][0].account_number
                
				callback(null, data, transaction)
            })
		},
        function getDetailTransaksi (data, transaction, callback) {
            merchantTransactionDetailGet(APP, {
				body: {
					transactions_merchant_id: transaction[0].id,
					take: '99999999999',
					skip: '0'
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') transaction[0].products = [];

                transaction[0].products = result.data;	

				callback(null, data, transaction);
			});
        },
        function update (data, transaction, callback) {
            var query = APP.queries.storePro('update_payment', {
                body: {
                    requestId: transaction[0].requestId,
                    va: transaction[0].merchant_va,
                    invoice: transaction[0].invoice,
                    ip_address: transaction[0].ip_address
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                data.messageUpdate = 'success';
                callback(null, data, transaction)
            })
        },
        function output (data, transaction, callback) {
            callback(null, {
                code: '00',
                message: 'Success update payment',
                data: transaction[0]
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}