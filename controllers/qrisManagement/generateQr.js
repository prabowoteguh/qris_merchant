"use strict";

const async = require('async');
const request = require('../../functions/request')
const rupiah = require('rupiah-format');

module.exports = (APP, req, callback) => {
    let resultMerchantArr = [];
    let resultMerchantMaster = [];
    
    async.waterfall([
        function validation (callback) {
            if (!req.body.merchant_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_id" } });
            
            callback(null, true)
        },
        function checkPoi (data, callback) {
            if (req.body.point_of_initiation == '12') {
                if (!req.body.transaction_amount) {
                    return callback({ 
                        code: 'FAILED', 
                        message: 'transaction_amount belum diisi.'
                    });
                } else {
                    callback(null, data)
                }
            } else {
                callback(null, data)
            }
        },
        function inquiryDataMerchantArr (data, callback) {
            var query = APP.queries.rawQueris('inquiry_merchant_arr', {
                body: {
                    merchant_id: req.body.merchant_id
                }
            });
            
            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                let raw = JSON.stringify(rows[0])

                for (var i = 0; i < rows[0].length; i++) {
                    resultMerchantArr.push(JSON.parse(raw))
                }

                callback(null, data)
            })
        },
        function checkinquiryDataMerchantMaster(data, callback) {
            var query = APP.queries.rawQueris('inquiry_merchant_master', {
                body: {
                    merchant_id: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    callback({
                        code: 'FAILED',
                        message: 'Isian merchant acquirer belum lengkap, silahkan lengkapi.'
                    })
                } else {
                    callback(null, data)
                }
            })
        },
        function inquiryDataMerchantMaster (data, callback) {
            var query = APP.queries.rawQueris('inquiry_merchant_master', {
                body: {
                    merchant_id: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                let raw = rows[0][0]

                for (var i = 0; i < rows[0].length; i++) {
                    resultMerchantMaster.push(raw)
                }

                callback(null, data)
            })
        },
        function generate (data, callback) {
            let dataRequest = {
                payloadFormatIndicator: "01",
                pointOfInitiation: req.body.point_of_initiation,
                transactionAmount: req.body.transaction_amount,
                merchantAccountInformation: resultMerchantArr[0],
                nationalMerchantAccountInformation: {
                    globalUniqueIdentifier: resultMerchantMaster[0].globalUniqueIdentifier,
                    nmid: resultMerchantMaster[0].nmid,
                    merchantCriteria: resultMerchantMaster[0].merchantCriteria
                },
                merchantCategoryCode: resultMerchantMaster[0].merchantCategoryCode,
                transactionCurrency: "360",
                countryCode: "ID",
                merchantName: resultMerchantMaster[0].merchantName,
                merchantCity: resultMerchantMaster[0].merchantCity,
                postalCode: resultMerchantMaster[0].postalCode,
                additionalData: {
                    terminalLabel: "A01",
                    referenceLabel: req.body.referenceLabel || null,
                    // billNumber: req.body.billNumber || null
                }
            }

            let url = `${process.env.CORE_QR}/qr/generate`;
            let response = {};

            request.post(url, dataRequest, (err, result) => {
                if (err) {
                    callback(err)
                } else {
                    if (result.code == '00') {
                        response = {
                            code: result.code,
                            message: result.message,
                            data: {
                                point_of_initiation: req.body.point_of_initiation,
                                nmid: resultMerchantMaster[0].nmid,
                                merchant_name: resultMerchantMaster[0].merchantName,
                                payload: result.data.payload,
                                cashier: 'A01'
                            }
                        }

                        if (req.body.point_of_initiation == '12') {
                            response.data = {
                                point_of_initiation: req.body.point_of_initiation,
                                transaction_amount: req.body.transaction_amount,
                                transaction_amount_format: rupiah.convert(req.body.transaction_amount),
                                nmid: resultMerchantMaster[0].nmid,
                                merchant_name: resultMerchantMaster[0].merchantName,
                                payload: result.data.payload,
                                cashier: 'A01'
                            }
                        }

                        callback(null, response)
                    } else {
                        callback(result)
                    }
                }
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}