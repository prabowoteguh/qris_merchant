"use strict";

const async = require('async');
const request = require('../../functions/request')
const _ = require('lodash')
const rand = require('randomstring')
const inv = rand.generate(12);
const helpUrl = require('../../helpers/helpUrl');

module.exports = (APP, req, callback) => {
    let resultRes = [];
    let resultResInner = [];
    let dataRequest = {};
    let dataPayload = {};

    async.waterfall([
        function validation (callback) {
            if (!req.body.payload) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "payload" } });
            
            callback(null, true)
        },
        function checkBackSlash (data, callback) {
            let str = req.body.payload;
            req.body.payload = str.replace(/\\/g, '/');

            callback(null, data)
        },
        function generate (data, callback) {
            let url = `${process.env.CORE_QR}/qr/read`;

            request.post(url, req.body, (err, result) => {
                if (err) {
                    if (err.code !== '00') {
                        callback({ 
                            code: '01',
                            message: err.message,
                            data: err.data
                        })
                    } else {
                        callback(err)
                    }
                } else {
                    if (result.code == '00') {
                        let raw = result.data.merchantAccountInformation
                        
                        for (var i = 0; i < result.data.merchantAccountInformation.length; i++) {
                            resultRes.push(raw)
                        }

                        dataRequest = {
                            payloadFormatIndicator: result.data.payloadFormatIndicator,
                            pointOfInitiation: result.data.pointOfInitiation,
                            merchantCategoryCode: result.data.merchantCategoryCode,
                            transactionCurrency: result.data.transactionCurrency,
                            transactionAmount: result.data.pointOfInitiation == 11 ? 'Static' : 
                                result.data && (typeof result.data.transactionAmount == 'undefined') ? '50000' : 
                                    result.data.transactionAmount,
                            countryCode: result.data.countryCode,
                            merchantName: result.data.merchantName,
                            merchantCity: result.data.merchantCity,
                            postalCode: result.data.postalCode,
                            nationalMerchantAccountInformation: {
                                globalUniqueIdentifier: result.data.nationalMerchantAccountInformation.globalUniqueIdentifier,
                                nmid: result.data.nationalMerchantAccountInformation.nmid,
                                merchantCriteria: result.data.nationalMerchantAccountInformation.merchantCriteria
                            },
                            additionalData: {
                                terminalLabel: result.data && (typeof result.data.additionalData == 'undefined') ? '' : result.data.additionalData.terminalLabel,
                                referenceLabel: result.data && (typeof result.data.additionalData == 'undefined') ? '' : result.data.additionalData.referenceLabel
                            }
                        }

                        dataPayload = result.payload

                        callback(null, result)
                    } else {
                        callback(result)
                    }
                }
            })
        },
        function getBank (data, callback) {
            var query = APP.queries.rawQueris('check_global_unique_identifier', {
                body: {}
            });
            
            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                let bank = rows[0][0].global_unique_identifier

                callback(null, data, bank)
            })
        },
        function getMerchantId (data, bank, callback) {
            var query = APP.queries.rawQueris('get_merchant_id', {
                body: {
                    merchant_name: dataRequest.merchantName,
                    nmid: dataRequest.nationalMerchantAccountInformation.nmid || 0
                }
            });
            
            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    dataRequest.merchantInfo = 'Merchant tidak ditemukan'
                    dataRequest.merchantId = null
                    callback(null, data, bank)
                } else {
                    dataRequest.merchantInfo = 'Merchant ditemukan'
                    dataRequest.merchantId = rows[0][0].merchant_id
                    callback(null, data, bank)
                }
            })
        },
        function getInvoice(data, bank, callback) {
            if (dataRequest.pointOfInitiation == '11') {
                var query = APP.queries.storePro('get_invoice', {
                    body: {
                        id_merchant: dataRequest.merchantId
                    }
                });
            } else {
                var query = APP.queries.storePro('check_invoice', {
                    body: {
                        request_id: dataRequest.additionalData.referenceLabel || null
                    }
                });
            }
            
            APP.db.sequelize.query(query.query, query.bind).then(rows => {
                if (rows == null) {
                    dataRequest.transaction = {}
                } else {
                    if (dataRequest.pointOfInitiation == '11') {
                        if (rows.length < 1) {
                            dataRequest.transaction = {
                                emoneyVa: ''
                            }
                        } else {
                            dataRequest.transaction = {
                                emoneyVa: rows[0].emoney_va
                            }
                        }
                    } else if (dataRequest.pointOfInitiation == '12') {
                        if (rows.length < 1) {
                            dataRequest.transaction = {
                                accountNumber: '',
                                invoice: '',
                                requestId: ''
                            }
                        } else {
                            dataRequest.transaction = {
                                accountNumber: rows[0].account_number,
                                invoice: rows[0].invoice,
                                requestId: rows[0].requestId
                            }
                        }
                    } else {
                        dataRequest.transaction = {}
                    }
                }

                callback(null, data, bank)
            }).catch(err => {
                console.log(err)
            })
        },
        function read (data, bank, callback) {
            resultRes[0].map((i, e) => {
                resultResInner.push(i)
            })

            let checkG = _.find(resultResInner, function(z) { return z.globalUniqueIdentifier == bank })
            if (checkG == null) {
                dataRequest.merchantAccountInformation = []
            } else {
                dataRequest.merchantAccountInformation = [checkG]
            }
            
            callback(null, {
                code: '00',
                message: 'Read Data',
                data: dataRequest,
                payload: dataPayload
            })
        },
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}