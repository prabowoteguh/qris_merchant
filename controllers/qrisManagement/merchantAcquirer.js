"use strict";

const async = require('async');
const _ = require('lodash');

// List Merchant Acquirer
exports.listMerchantAcquirer = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            callback(null, true)
        },
        function list (data, callback) {
            let query = {
                raw: true,
                limit: parseInt(req.body.limit) || null,
                offset: parseInt(req.body.offset) || null
            };

            query.where = {};

            if (req.body.merchant_id_acquirer) {
                query.where.merchant_id_acquirer = req.body.merchant_id_acquirer
            }

            if (req.body.merchant_acquirer_id) {
                query.where.id = req.body.merchant_acquirer_id
            }

            if (req.body.merchant_id) {
                query.where.merchant_id = req.body.merchant_id
            }

            if (req.body.merchant_criteria_id) {
                query.where.merchant_criteria_id = req.body.merchant_criteria_id
            }

            if (req.body.status) {
                query.where.status = req.body.status
            }

            query.include = [
                {
                    model: APP.models.mysql.bankCode,
                    attributes: []
                }, 
                {
                    model: APP.models.mysql.merchantCriteria,
                    attributes: []
                }
            ];

            query.attributes = [
                'id', 'merchant_id', 'merchant_id_acquirer', 'merchant_name',
                'merchant_criteria_id', 'bank_id', 'merchant_pan', 'merchant_pan_input',
                'status', 'created_at', 'updated_at', 'action_by', 'ip_address',
                'merchant_pan_format',
                'bank_code.bank_name', 'bank_code.bank_code',
                // 'merchant_criterium.code'
                [APP.db.sequelize.col('merchant_criterium.code'), 'merchant_criteria_code'],
                [APP.db.sequelize.col('merchant_criterium.name'), 'merchant_criteria_name']
            ];

            if (req.body.order_by) {
                switch(req.body.order_by) {
                    case 'merchant_name_asc':
                        query.order = [['merchant_name', 'ASC']]
                        break;
                    case 'merchant_name_desc':
                        query.order = [['merchant_name', 'DESC']]
                        break;
                    case 'bank_name_asc':
                        query.order = [[{model: APP.models.mysql.bankCode}, 'bank_name', 'ASC']]
                        break;
                    case 'bank_name_desc':
                        query.order = [[{model: APP.models.mysql.bankCode}, 'bank_name', 'DESC']]
                        break;
                    case 'bank_code_asc':
                        query.order = [[{model: APP.models.mysql.bankCode}, 'bank_code', 'ASC']]
                        break;
                    case 'bank_code_desc':
                        query.order = [[{model: APP.models.mysql.bankCode}, 'bank_code', 'DESC']]
                        break;
                    case 'merchant_criteria_name_asc':
                        query.order = [[{model: APP.models.mysql.merchantCriteria}, 'name', 'ASC']]
                        break;
                    case 'merchant_criteria_name_desc':
                        query.order = [[{model: APP.models.mysql.merchantCriteria}, 'name', 'DESC']]
                        break;
                    case 'merchant_pan_format_asc':
                        query.order = [['merchant_pan_format', 'ASC']]
                        break;
                    case 'merchant_pan_format_desc':
                        query.order = [['merchant_pan_format', 'DESC']]
                        break;
                    case 'merchant_id_desc':
                        query.order = [['merchant_id', 'DESC']]
                        break;
                    case 'merchant_id_asc':
                        query.order = [['merchant_id', 'ASC']]
                        break;
                    case 'created_at_asc':
                        query.order = [['created_at', 'ASC']]
                        break;
                    case 'created_at_desc':
                        query.order = [['created_at', 'DESC']]
                        break;
                }
            }

            if (req.body.search) {
                query.where = {
                    $and: [
                        {
                            status: req.body.status
                        }
                    ],
                    $or: [
                        {
                            merchant_name: {
                                $like: '%'+ req.body.search +'%'
                            }
                        },
                        {
                            merchant_pan_format: {
                                $like: '%'+ req.body.search +'%'
                            }
                        },
                        {
                            merchant_id_acquirer: {
                                $like: '%'+ req.body.search +'%'
                            }
                        },
                        {
                            '$bank_code.bank_name$': {
                                $like: '%'+ req.body.search +'%'
                            }
                        },
                        {
                            '$merchant_criterium.name$': {
                                $like: '%'+ req.body.search +'%'
                            }
                        }
                    ]
                    
                }
            }

            APP.models.mysql.merchantAcquirer.findAll(query).then(rows => {
                APP.models.mysql.merchantAcquirer.findAndCountAll(query).then(rowsCount => {
                    const rowCustom = Object.keys(rows).map(e => {
                        const all = {				
                            "merchant_pan_format_un": rows[e].merchant_pan + '-' + rows[e].merchant_pan_input					
                        }

                        return all;
                    });

                    if (rows.length < 1) {
                        callback(null, {
                            code: 'NOT_FOUND'
                        })
                    } else {
                        if (req.body.merchant_acquirer_id !== '') {
                            callback(null, {
                                code: 'FOUND',
                                data: _.merge(rows[0], rowCustom[0])
                            })
                        } else {
                            callback(null, {
                                code: 'FOUND',
                                data: {
                                    rows: _.merge(rows, rowCustom),
                                    count: rowsCount.count,
                                    count_by_limit: rows.length
                                }
                            })
                        }
                    }
                }).catch(err => {
                    output = {
                        code: 'ERR_DATABASE',
                        data: err
                    }
    
                    callback(output)
                })
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}

// Add Merchant Acquirer
exports.addMerchantAcquirer = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            if (!req.body.merchant_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_id" } });
            if (!req.body.merchant_criteria_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_criteria_id" } });
            if (!req.body.merchant_id_acquirer) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_id_acquirer" } });
            if (!req.body.bank_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "bank_id" } });
            
            callback(null, true)
        },
        function checkMaxMerchantAcquirer (data, callback) {
            var query = APP.queries.rawQueris('check_max_merchant_acquier', {
                body: {
                    merchant_id: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                let checkLn = rows[0].length

                if (checkLn == 8) {
                    output = {
                        code: 'FAILED',
                        message: 'Merchant acquirer max 8.'
                    }
    
                    callback(output)
                } else {
                    callback(null, data)
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function checkMerchantAcquirerUnique (data, callback) {
            var query = APP.queries.rawQueris('check_merchant_acquirer_code', {
                body: {
                    merchant_id_acquirer: req.body.merchant_id_acquirer
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    callback(null, data)
                } else {
                    output = {
                        code: 'FAILED',
                        message: 'Merchant acquirer code sudah terpakai.'
                    }
    
                    callback(output)
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function checkMerchantMerge(data, callback) {
            let merchant_pan = req.body.merchant_pan + req.body.merchant_pan_input
            if (merchant_pan.length < 16 || merchant_pan.length > 19) {
                output = {
                    code: 'FAILED_MERCHANT_PAN',
                    message: 'Merchant PAN min = 16, max = 19.',
                    error_info: {
                        value: req.body.merchant_pan + req.body.merchant_pan_input,
                        min: 16,
                        max: 19
                    }
                }

                callback(output)
            } else {
                callback(null, data)
            }
        },
        function checkMerchantAcquirer (data, callback) {
            var query = APP.queries.rawQueris('check_merchant_acquirer', {
                body: {
                    bank_id: req.body.bank_id,
                    merchant_id: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    callback(null, data)
                } else {
                    output = {
                        code: 'FAILED',
                        message: 'Bank code sudah terpakai.'
                    }
    
                    callback(output)
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function checkMerchantCriteria (data, callback) {
            var query = APP.queries.rawQueris('check_merchant_criteria', {
                body: {
                    merchant_criteria_id: req.body.merchant_criteria_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    output = {
                        code: 'FAILED',
                        message: 'Merchant Criteria tidak ditemukan.'
                    }
    
                    callback(output)
                } else {
                    callback(null, data)
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function checkBankCode (data, callback) {
            var query = APP.queries.rawQueris('check_bank', {
                body: {
                    bank_id: req.body.bank_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length < 1) {
                    output = {
                        code: 'FAILED',
                        message: 'Bank Code tidak ditemukan.'
                    }
    
                    callback(output)
                } else {
                    callback(null, data)
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function getMerchant (data, callback) {
            var query = APP.queries.rawQueris('check_merchant', {
                body: {
                    merchant_id: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                let merchant = {
                    name: rows[0][0].name || '',
                    code: rows[0][0].code || ''
                }
                
                callback(null, data, merchant);
            })
        },
        function checkMerchantAcquierId (data, merchant, callback) {
            var query = APP.queries.rawQueris('check_merchant_id_acquirer', {
                body: {
                    merchant_id_acquirer: req.body.merchant_id_acquirer
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                if (rows[0].length > 0) {
                    output = {
                        code: 'FAILED',
                        message: 'Merchant id acquirer sudah terpakai.'
                    }
    
                    callback(output)
                }
                
                callback(null, data, merchant);
            })
        },
        function insert (data, merchant, callback) {
            let query = {
                merchant_id: req.body.merchant_id,
                merchant_name: merchant.name,
                merchant_criteria_id: req.body.merchant_criteria_id,
                merchant_id_acquirer: req.body.merchant_id_acquirer,
                bank_id: req.body.bank_id,
                merchant_pan: req.body.merchant_pan,
                merchant_pan_input: req.body.merchant_pan_input,
                merchant_pan_format: req.body.merchant_pan + req.body.merchant_pan_input,
                // merchant_pan_format: req.body.merchant_pan + '-' + req.body.merchant_pan_input,
                status: 1,
                action_by: req.body.action_by || null,
                ip_address: req.body.ip_address || '127.0.0.1'
            }

            APP.models.mysql.merchantAcquirer.create(query).then(result => {
                output = {
                    code: 'OK_INSERT'
                }
    
                callback(null, output)
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}

exports.updateMerchantAcquirer = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            if (!req.body.merchant_acquirer_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_acquirer_id" } });
        
            callback(null, true)
        },
        function update (data, callback) {
            let values = {}   

            let whereVal = {
                where: {
                    id: req.body.merchant_acquirer_id
                }
            }

            if (req.body.merchant_id) {
                values = {
                    merchant_id: req.body.merchant_id
                }
            }

            if (req.body.merchant_id_acquirer) {
                values = {
                    merchant_id_acquirer: req.body.merchant_id_acquirer
                }
            }

            if (req.body.merchant_criteria_id) {
                values = {
                    merchant_criteria_id: req.body.merchant_criteria_id
                }
            }

            if (req.body.bank_id) {
                values = {
                    bank_id: req.body.bank_id
                }
            }

            if (req.body.merchant_pan) {
                values = {
                    merchant_pan: req.body.merchant_pan
                }
            }

            if (req.body.merchant_pan_input) {
                values = {
                    merchant_pan_input: req.body.merchant_pan_input
                }
            }

            if (req.body.action_by) {
                values = {
                    action_by: req.body.action_by
                }
            }

            APP.models.mysql.merchantAcquirer.update(values, whereVal).then(result => {
                output = {
                    code: 'OK_UPDATE'
                }
    
                callback(null, output)
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}

exports.deleteMerchantAcquirer = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            if (!req.body.merchant_acquirer_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_acquirer_id" } });
            if (!req.body.type) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "type" } });
            
            callback(null, true)
        },
        // Check Id Merchant Acquirer
        function checkIdMerchantAcquirer(data, callback) {
            let query = {
                where: {
                    id: req.body.merchant_acquirer_id
                }
            }

            APP.models.mysql.merchantAcquirer.findOne(query).then(result => {
                if (result == null) {
                    callback(null, data)
                } else {
                    output = {
                        code: 'NOT_FOUND'
                    }
        
                    callback(null, output)
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function hardDelete (data, callback) {
            // Check Soft Delete
            if (req.body.type == 1) return callback(null, data)

            let whereVal = {
                where: {
                    id: req.body.merchant_acquirer_id
                }
            }

            APP.models.mysql.merchantAcquirer.destroy(whereVal).then(result => {
                output = {
                    code: 'OK_DELETE'
                }
    
                callback(null, output)
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        },
        function softDelete (data, callback) {
            let values = {}
            let whereVal = {
                where: {
                    id: req.body.merchant_acquirer_id
                }
            }

            if (req.body.type) {
                values = {
                    status: 0
                }
            }

            APP.models.mysql.merchantAcquirer.update(values, whereVal).then(result => {
                output = {
                    code: 'OK_DELETE'
                }
    
                callback(null, output)
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}