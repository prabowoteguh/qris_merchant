"use strict";

const async = require('async');

// List Merchant Criteria
exports.listMerchantCriteria = (APP, req, callback) => {
    let output;
    
    async.waterfall([
        function validation (callback) {
            callback(null, true)
        },
        function list (data, callback) {
            let query = {
                limit: parseInt(req.body.limit) || null,
                offset: parseInt(req.body.offset) || null
            };

            query.where = {};

            if (req.body.merchant_criteria_id) {
                query.where.id = req.body.merchant_criteria_id
            }

            if (req.body.order_by) {
                switch(req.body.order) {
                    case 'code_asc':
                        query.order =  [['code', 'ASC']]
                        break;
                    case 'code_desc':
                        query.order =  [['code', 'DESC']]
                        break;
                    case 'name_asc':
                        query.order =  [['name', 'ASC']]
                        break;
                    case 'name_desc':
                        query.order =  [['name', 'DESC']]
                        break;
                }
            }

            APP.models.mysql.merchantCriteria.findAll(query).then(rows => {
                if (rows.length < 1) {
                    callback(null, {
                        code: 'NOT_FOUND'
                    })
                } else {
                    callback(null, {
                        code: 'FOUND',
                        data: req.body.merchant_criteria_id !== '' ? rows[0] : { rows: rows }
                    })
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    data: err
                }

                callback(output)
            })
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}