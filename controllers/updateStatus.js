"use strict";

const async = require('async');
const checkMerchant = require('../controllers/checkMerchant.js');
const merchantUpdate = require('../controllers/merchant/update.js');

/**
* @type: service
* @req: merchant_id, status
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						service: "updateStatus"
					}
				});

			if (!req.body.status) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "status"
					},
					info: {
						service: "updateStatus"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_id);
			var validateStatus = APP.validation.true(req.body.status);

			if (validateMerchantId !== true) return callback(validateMerchantId);
			if (validateStatus !== true) return callback(validateStatus);

			callback(null, true);
		},
		function checkingMerchant (index, callback) {
			checkMerchant(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: {},
						info: {
							service: 'updateStatus'
						}
					});

				callback(null, result.data);
			});
		},
		function updatingMerchant (merchant, callback) {
			// Wait store pro
			merchantUpdate(APP, {
				body: {
					dataQuery: {
						code: req.body.merchant_id
					},
					dataUpdate: {
						status: req.body.status
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				callback(null, {
					merchant_id: req.body.merchant_id,
					status: req.body.status
				});
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'MERCHANT_UPDATE_SUCCESS',
				data: data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};