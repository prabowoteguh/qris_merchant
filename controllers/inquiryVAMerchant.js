	"use strict";

const async = require('async');
const emoneyServices = require('../config/emoneyServices.json');

/**
* @type: function
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_va"
					},
					info: {
						function: "checkAccountMerchant"
					}
				});

			callback(null, {});
		},
		function bundlingVa (data, callback) {
			APP.generator.bundleVa(APP, req.body.merchant_va, (err, result) => {
				if (err) return callback(err);

				data.merchant_va = result.data.emoney_va;

				callback(null, data);
			});
		},
		function checkingAccount (data, callback) {
			APP.request.emoney(emoneyServices.checkAccount, {
				type: '2',
				emoney_va: data.merchant_va
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== '00') return callback(result);

				callback(null, result);
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				message: output.message,
				data: output.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};