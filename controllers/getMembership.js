"use strict";

const async = require('async');
const Membership = require('../controllers/membership/list.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			// if (!req.body.merchant_id) return callback({
			// 		code: 'MISSING_KEY',
			// 		data: {
			// 			missing_parameter: "merchant_id"
			// 		},
			// 		info: {
			// 			function: "getProductType"
			// 		}
			// 	});
			callback(null, true);
		},
		function gettingList (index, callback) {
			Membership(APP, {
				body: {
					emoney_va   : req.body.emoney_va,
					merchant_id : req.body.merchant_id,
					limit		: req.body.limit,
					offset		: req.body.offset,
					search		: req.body.search,
					status		: req.body.status,
					order 		: req.body.order,
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'DATA_NOT_FOUND_CODE_01',
						data: {
							rows: [],
							count: 0
						}
					});

				callback(null, result.data);
			});
		},
		function output (list, callback) {
			callback(null, {
				code: 'LIST_MEMBERSHIP_FOUND',
				data: list
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};