"use strict";

const async = require('async');
const moment = require('moment');
const checkMerchant = require('../controllers/checkMerchant.js');

/**
* @type: service
* @req: username, password
* @return callback object
*/

module.exports = function (APP, req, callback) {
	var output;
	async.waterfall([
		function validation (callback) {
			if (!req.body.username) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "username"
					},
					info: {
						service: "login"
					}
				});

			if (!req.body.password) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "password"
					},
					info: {
						service: "login"
					}
				});

			var validateMerchantUsername = APP.validation.true(req.body.username);
			var validateMerchantPassword = APP.validation.true(req.body.password);

			if (validateMerchantUsername !== true) return callback(validateMerchantUsername);
			if (validateMerchantPassword !== true) return callback(validateMerchantPassword);

			callback(null, true);
		},
		function loginMerchant (index, callback) {
			callback(null, {});
		},
		function gettingMerchantData (login, callback) {
			checkMerchant(APP, {
				body: {
					username: req.body.username,
					password: req.body.password,
					type: req.body.type,
					login: true
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback(null,{
							code: 'LOGIN_FAILED',
							data: {}
						});

					return callback(err);
				}

				if(result && result.data.active_status == 0) return callback({
					code: 'USER_MERCHANT_BLOKIR',
					data: {}
				});

				if (result && result.code !== 'FOUND') return callback(null,{
						code: 'LOGIN_FAILED',
						data: {}
					});

				callback(null, result);
			});
		},
		function checkAttemptStatus(data,callback) {
			if(data.code == "FOUND" && data.data.attempt_status_login > 3 && moment(moment(data.data.blocked_until_login)).isAfter(moment())){
                output = {
                    code: "DBB04",
                    message: `Akun anda telah ditangguhkan sampai ${moment(data.data.blocked_until_login).format("Y-MM-DD HH:mm:ss")}`
                }
                callback(output)
			}else{
				callback(null,data)
			}
		},
		function attemptStatus(data,callback) {
			// Existing
			// let status_login = data.code == "FOUND" ? "1" : "2"
			// let query = `CALL create_attempt_login_merchant(:status_login, :username);`;
			// End Existing

			let status_login = "";
			let query = `CALL create_attempt_login_merchant(:status_login, :username);`;

			if (data.code == "FOUND") {
				if (data.data.merchant_status == 4) {
					status_login = "4";
				} else if (data.data.merchant_status == 5) {
					status_login = "5";
				} else {
					status_login = "1";
				}
			} else {
				status_login = "2";
			}

			req.APP.db.sequelize.query(query,{
                replacements: {
                    status_login: status_login,
                    username: req.body.username
                }
            }).then(result =>{
            	if(result.length > 0){
            		if (status_login == "2" && result[0].code == "DBB04") {
	                    output = {
	                        code: result[0].code,
	                        message: `Akun anda telah ditangguhkan sampai ${moment(result[0].blocked_until_login).format("Y-MM-DD HH:mm:ss")}`
	                    }
	                    callback(output)
					} else if (status_login == "4" || status_login == "5" && result[0].code == "DBB26") {
						output = {
	                        code: result[0].code,
	                        message: `Akun anda sedang direview dan diproses`
	                    }
	                    callback(output)
					} else if (status_login == "2" && result[0].code == "00") {
	                    callback(data)
	                } else {
	                    callback(null, data.data)
	                }
            	}else{
            		callback(data)
            	}
            	
            }).catch(err =>{
            	console.log(err)
                callback({
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                })
            })
		},
		function updateData (user, callback) {
			if(req.body.device_key && req.body.device_type){
				let query = {
	                device_key: req.body.device_key,
	                device_type: req.body.device_type,
	            }
	            let where = {
	                where: {
	                    username: user.username,
	                }
	            }
	            APP.models.mysql.userMerchant.update(query, where).then(result => {
	                callback(null, {
						code: 'LOGIN_SUCCESS',
						data: user
					});
	            }).catch(err => {
	            	console.log(err)
	                callback({
	                    code: 'ERR_DATABASE',
	                    ori_message: err.message
	                })
	            })	
			}else{
				callback(null, {
					code: 'LOGIN_SUCCESS',
					data: user
				});
			}
			// callback(null, {
			// 	code: 'LOGIN_SUCCESS',
			// 	data: user
			// });
			
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};