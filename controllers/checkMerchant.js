	"use strict";

const async = require('async');

/**
* @type: function
* @req: merchant_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (req.body.login) {
				if (!req.body.username) return callback({
						code: 'MISSING_KEY',
						data: {
							missing_parameter: "username"
						},
						info: {
							function: "checkMerchant"
						}
					});

				if (!req.body.password) return callback({
						code: 'MISSING_KEY',
						data: {
							missing_parameter: "password"
						},
						info: {
							function: "checkMerchant"
						}
					});

				var validateUsername = APP.validation.true(req.body.username);
				var validatePassword = APP.validation.true(req.body.password);

				if (validateUsername !== true) return callback(validateUsername);
				if (validatePassword !== true) return callback(validatePassword);

				callback(null, true);
			} else {
				if (!req.body.merchant_id) return callback({
						code: 'MISSING_KEY',
						data: {
							missing_parameter: "merchant_id"
						},
						info: {
							function: "checkMerchant"
						}
					});

				var validateMerchantId = APP.validation.true(req.body.merchant_id);

				if (validateMerchantId !== true) return callback(validateMerchantId);

				callback(null, true);
			}
		},
		function gettingMerchantById (index, callback) {
			let params = {};

			if (req.body.login) {
				params = APP.queries.storePro('merchant_login', {
					body: {
						username: req.body.username,
						password: APP.generator.generatePassword(req.body.password).encrypted,
						type: req.body.type
					}
				});
			} else {
				params = APP.queries.storePro('getProfile', {
					body: {
						code: req.body.merchant_id.toUpperCase()
					}
				});
			}

			APP.db.sequelize.query(params.query, params.bind).then(account => {
				if (account.length < 1) return callback(null, {
						code: 'NOT_FOUND',
						data: [],
						info: {
							function: "checkMerchant"
						}
					});

				if (account[0].code && account[0].code !== '00') return callback({
						code: 'NOT_FOUND',
						message: account[0].message,
						data: {
							q_code: account[0].code,
							q_message: account[0].message
						},
						info: {
							function: "checkMerchant"
						}
					});
				let data  = {
					id: account[0].id,
					merchant_nama: account[0].merchant_name || account[0].name,
					business_type_id: account[0].business_type_id,
					merchant_jenis_usaha: account[0].business_type,
					description : account[0].description,
					open_hours : account[0].open_hours,
					closed_hours : account[0].closed_hours,
					merchant_nama_pic: account[0].owner,
					merchant_hp: account[0].msisdn,
					email: account[0].email,
					telepon: account[0].phone,
					alamat: account[0].address,
					kelurahan: account[0].kelurahan,
					kecamatan: account[0].kecamatan,
					kota: account[0].city,
					provinsi: account[0].province,
					status: account[0].status,
					active_status: account[0].active_status,
					type_user: account[0].type,
					user_merchant_name: account[0].name,
					image: account[0].image ? process.env.URL_CDN_IMAGE + account[0].image : null,
					image_merchant: account[0].image_merchant ? process.env.URL_CDN_IMAGE + account[0].image_merchant : null,
					banner: account[0].banner ? process.env.URL_CDN_IMAGE + account[0].banner : null,
					menu: account[0].menu,
					username: account[0].username,
					merchant_id: account[0].merchant_id || req.body.username || req.body.merchant_id,
					merchant_reff_id: account[0].merchant_reff_id,
					nmid: account[0].nmid,
					account_num: account[0].account_number || '',
					account_name: account[0].account_name || '',
					bank_code: account[0].bank_code,
					bank_name: account[0].bank_name,
					bank_account_number: account[0].bank_account_number,
					bank_account_name: account[0].bank_account_name,
					last_update_product: account[0].last_update_product,
					merchant_status: account[0].merchant_status
				}

				if(account[0].promo_id){data.promo_id = account[0].promo_id;}
				if(account[0].promo_name){data.promo_name = account[0].promo_name;}
				if(account[0].promo_description){data.promo_description = account[0].promo_description;}
				if(account[0].start_promo){data.start_promo = account[0].start_promo;}
				if(account[0].end_promo){data.end_promo = account[0].end_promo;}
				if(account[0].image_promo){data.image_promo = account[0].image_promo ? process.env.URL_CDN_IMAGE + account[0].image_promo : null}
				if(account[0].end_promo){data.end_promo = account[0].end_promo;}
				if(account[0].min_nominal){data.min_nominal = account[0].min_nominal;}
				if(account[0].point_reward){data.point_reward = account[0].point_reward;}
				if(account[0].max_transaction){data.max_transaction = account[0].max_transaction;}

				if (req.body.login) {
					data.attempt_status_login = account[0].attempt_status_login;
					data.last_attempt_status_login = account[0].last_attempt_status_login;
					data.blocked_until_login = account[0].blocked_until_login;
				}
				
				
				if (data.type_user == '4' || data.type_user == '5') {
					data.expired_approval = account[0].expired_approval;
					data.role_id = account[0].role_id;
					data.role_name = account[0].role_name;
					var query = APP.queries.rawQueris('get_permission_residence', {
						body: {
							role_id: data.role_id
						}
					});
					APP.db.sequelize.query(query.query, query.bind).then(rows => {
						data.permission = rows;
					});
				}
				callback(null, {
					code: 'FOUND',
					data: data
				});

			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "checkMerchant"
					}
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};