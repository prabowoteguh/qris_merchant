"use strict";

const async = require('async');
const moment = require('moment');
const merchantTransactionGet = require('../controllers/merchantTransaction/get.js');
const merchantTransactionDetailGet = require('../controllers/merchantTransactionDetail/get.js');
const qrGenerate = require('../controllers/qrisManagement/generateQr');
const number = require('../functions/number');

/**
* @type: service
* @req: transaction_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	let merchant_id_key;
	let acquirer = {};

	async.waterfall([
		function validation (callback) {
			if (!req.body.transaction_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "transaction_id"
					},
					info: {
						function: "getProduct"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "getProduct"
					}
				});

			req.body.merchant_id = req.body.merchant_id.toUpperCase();

			callback(null, {});
		},
		function gettingData (data, callback) {
			merchantTransactionGet(APP, {
				body: {
					id: req.body.transaction_id
				}
			}, (err, result) => {
				if (err) return callback(err);
				console.log(err)

				if (result && result.code !== 'FOUND') return callback(result);

				data = result.data[0].dataValues;
				let mdr = 0.6;
				let mdr_calculation = 0.6 / 100;
				let mdr_nominal = data.nominal * mdr_calculation
				let penerimaan = data.nominal - mdr_nominal;

				data.mdr = mdr;
				data.mdr_calculation = mdr_calculation;
				data.mdr_nominal = mdr_nominal;
				data.mdr_nominal_rp = number.separatorRp(mdr_nominal); 
				data.penerimaan = penerimaan;
				data.penerimaan_rp = number.separatorRp(penerimaan);
				data.nominal_rp = number.separatorRp(data.nominal);
				callback(null, data);
			})
		},
		function gettingDetail (data, callback) {
			merchantTransactionDetailGet(APP, {
				body: {
					transactions_merchant_id: req.body.transaction_id,
					take: '99999999999',
					skip: '0'
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') data.products = [];

				data.products = result.data;	

				callback(null, data);
			});
		},
		// Enhancement Generate QR when Transaction Detail Get Data Merchant
		function getDataMerchant(data, callback) {
			var query = APP.queries.rawQueris('get_merchant_by_user', {
                body: {
                    username: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => {
				merchant_id_key = rows[0][0].merchant_id; 
				data.bank_account_name = rows[0][0].bank_account_name;
				data.bank_account_number = rows[0][0].bank_account_number;

				callback(null, data)
            })
		},
		// Enhancement Get Data Invoice
		function getDataInvoice (data, callback) {
            var query = APP.queries.storePro('check_invoice', {
                body: {
                    request_id: data.requestId
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                data.invoice = rows[0].invoice;
                callback(null, data)
            })
        },
		// Enhancement Generate QR when Transaction Detail
		function generateQr(data, callback) {
			qrGenerate(APP, {
				body: {
					merchant_id: merchant_id_key,
					point_of_initiation: data.transactions_type_code == 'TRT012' ? '11' : '12',
					transaction_amount: data.transactions_type_code == 'TRT012' ? JSON.stringify(data.nominal) 
						: data.products.length < 1 ? '0' 
							: JSON.stringify(data.products[0].total),
					referenceLabel: data.requestId
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== '00') return callback({
					code: 'GENERATE_QR_FAILED',
					data: {},
					info: {
						service: 'generateQr'
					}
				});

				data.qr = result.data;
				acquirer = {
					reference_id: null,
					acquirer_name: null,
					merchant_name: null,
					merchant_city: null,
					merchant_pan: null,
					merchant_id: null,
					terminal_id: null,
					issuer_name: null,
					customer_name: null,
					customer_pan: null,
					created_at: null,
					created_at_str: null
				}

				callback(null, data);
			})
		},
		// Enhancement Payment Mid Dinamic and Static
		function paymentGetLogDinamic(data, callback) {
			// Static
			// if (data.transactions_type_code !== 'TRT009') {
			// 	data.acquirer = acquirer

			// 	return callback(null, data)	
			// } 

			var query = APP.queries.rawQueris('get_transaction_merchant_log', {
                body: {
                    reference_label: data.requestId
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => {
				if (rows[0].length < 1) {
					data.acquirer = acquirer
				} else {
					data.acquirer = {
						reference_id: rows[0][0].reference_id,
						acquirer_name: rows[0][0].acquirer_name,
						merchant_name: rows[0][0].merchant_name,
						merchant_city: rows[0][0].merchant_city,
						merchant_pan: rows[0][0].merchant_pan,
						merchant_id: rows[0][0].merchant_id,
						terminal_id: rows[0][0].terminal_id,
						issuer_name: rows[0][0].issuer_name,
						customer_name: rows[0][0].customer_name,
						customer_pan: rows[0][0].customer_pan,
						created_at: rows[0][0].created_at,
						created_at_str: moment(rows[0][0].created_at).format('DD MMMM YYYY hh:mm:ss')
					}
				}

				callback(null, data)
            })
		},
		function output (data, callback) {
			callback(null, {
				code: 'MERCHANT_TRANSACTION_DETAIL_FOUND',
				data: data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};