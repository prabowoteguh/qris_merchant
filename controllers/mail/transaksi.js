'use strict';

const async = require('async');
const email = require('../../functions/email');
const moment = require('moment');
const number = require('../../functions/number');

function getProduct(list) {
    let string = '';
    
    if (list.length == '0') {
        string = '';
    } else {
        string += `<table border="1" cellpadding="10">
                            <tr>
                                <th>Nama Barang</th>
                                <th>Total Barang</th>
                                <th>Harga Per Barang</th>
                                <th>Harga Total Barang</th>
                            </tr>`;
        
        for (let i = 0; i < list.length; i++) {
            string += `<tr>
                                <td>${list[i].name}</td>
                                <td>${list[i].quantity}</td>
                                <td>${number.putRP(list[i].price)}</td>
                                <td>${number.putRP(list[i].total)}</td>
                            </tr>`;
        }

        string += `</table>`;
    }

    return string;
}

exports.send = function(req, payload, callback) { 
    async.waterfall([
        function init(callback) {
            callback(null, true);
        },
        function (data, callback) {
            let nama = payload.nama || req.nama || req.session.nama;
            let payload_email = {
                body: { email: payload.email || req.email || req.session.email }
            };

            payload_email.body.subject = '[Blink Pay] Payment Merchant Success';
            payload_email.body.body_email = `<p> Hai ${payload.nama}, <br/> 
                <br/> Terimakasih telah menggunakan fasilitas Blink Pay. <br/> 
                <br/> Berikut adalah informasi transaksi yang telah Anda lakukan: <br/> 
                <br/><table border="0"><tbody><tr><td> 
                <strong>Tanggal/Jam</strong></td><td> 
                <strong>:</strong></td><td>${moment(payload.created_at).format("DD/MM/YYYY HH:mm:ss")}</td>
                </tr><tr><td> <strong>No Reference</strong></td><td> <strong>:</strong></td>
                <td>${payload.no_reff}</td></tr><tr><td> <strong>Tipe Transaksi</strong>
                </td><td> <strong>:</strong></td><td>Pembayaran Merchant</td></tr><tr><td> 
                <strong>Pengirim</strong></td><td> <strong>:</strong></td><td>${payload.emoney_va || 'Tidak memiliki va'} - ${payload.nama}</td>
                </tr><tr><td> <strong>Penerima</strong></td><td> <strong>:</strong></td>
                <td>${payload.biller_name}</td></tr><tr><td> <strong>Kasir</strong></td>
                <td> <strong>:</strong></td><td>${payload.user_merchant}</td></tr><tr>
                <td> <strong>Total</strong></td><td> <strong>:</strong></td>
                <td>${number.putRP(payload.total)}</td></tr><tr><td valign="top"> 
                <strong>Deskripsi</strong></td><td valign="top"> <strong>:</strong></td>
                <td>${payload.deskripsi}</td>
                <td valign="top"></td></tr></tbody></table></p>${getProduct(payload.product)}`;
            email.send(req, payload_email, function(err, rows) {})

            callback(null, data);
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}