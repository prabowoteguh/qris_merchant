'use strict';

const async = require('async');
const email = require('../../functions/email');
const moment = require('moment');
const number = require('../../functions/number');

function getProduct(list) {
    let string = '';
    
    if (list.length == '0') {
        string = '';
    } else {
        string += `<table border="1" cellpadding="10">
                            <tr>
                                <th>Nama Barang</th>
                                <th>Total Barang</th>
                                <th>Harga Per Barang</th>
                                <th>Harga Total Barang</th>
                            </tr>`;
        
        for (let i = 0; i < list.length; i++) {
            string += `<tr>
                                <td>${list[i].name}</td>
                                <td>${list[i].quantity}</td>
                                <td>${number.putRP(list[i].price)}</td>
                                <td>${number.putRP(list[i].total)}</td>
                            </tr>`;
        }

        string += `</table>`;
    }

    return string;
}

exports.send = function(req, payload, callback) { 
    async.waterfall([
        function init(callback) {
            callback(null, true);
        },
        function (data, callback) {
            let payload_email = {
                body: { email: payload.email || req.email || req.session.email }
            };

            payload_email.body.subject = payload.subject;
            payload_email.body.body_email = `<p> Hai ${payload.merchant_nama_pic}, <br/> <br/> Terimakasih sudah melakukan pendaftaran merchant di Blink Pay. <br/> <br/>Maaf pendaftaran merchant Anda belum dapat kami proses lebih lanjut karena belum memenuhi syarat dan ketentuan yang berlaku. <br/> <br/>Hubungi Kami jika butuh bantuan di btncontactcenter@btn.co.id atau di 1500286 <br/> <br/> Salam Hangat, <br/> <br/> Blink Pay</p>`;
            email.send(req, payload_email, function(err, rows) {})

            callback(null, data);
        }
    ], function (err, result) {
        if (err) return callback(err);

        callback(null, result);
    })
}