	"use strict";

const async = require('async');
const emoneyServices = require('../../config/emoneyServices.json');
const checkVa = require('../../controllers/checkVa.js');
const notificationInsert = require('../../controllers/notification/insert.js');

/**
* @type: function
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function checkingMerchant (data, callback) {
			checkVa(APP, {
				body: {
					merchant_va: req.body.to_va
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: {},
							info: {
								service: 'merchantTransfer'
							}
						});

					return callback(err);
				};

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: {},
						info: {
							service: 'merchantTransfer'
						}
					});

				data.merchant = result.data;
				var validateRefferenceId = APP.validation.true(req.body.refference_id);
				if (validateRefferenceId !== true) return callback(validateRefferenceId);

				callback(null, data);
			});
		},
		function emoneyCoreTransferE2E (data, callback) {
			APP.request.emoney(emoneyServices.e2e, req.body, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== '00') return callback(result);

				data.e2e = result.data;

				callback(null, data);
			});
		},
		function insertToTransaction (data, callback) {
			const params = APP.queries.storePro('insert_transaction', {
				body: {
					requestId: req.body.refference_id,
					account_number: data.merchant.code.toUpperCase(),
					merchant_va: req.body.to_va,
					wallet_type_code: 'SOF01',
					transactions_type_code: 'TRT012',
					biller_type: '',
					biller_code: data.e2e.from_va,
					biller_name: data.e2e.from_va_name,
					nominal: req.body.amount,
					no_jurnal: data.e2e.journal,
					note: req.body.narration,
					fee: '0',
					admin_fee: '0',
					data: '',
					status: '1',
					action_by: '0',
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: transaction[0].code,
						message: transaction[0].message,
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							service: 'merchantTransfer'
						}
					});

				data.transactions_id = transaction[0].var_transactions_merchant_id;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'INSERT_TRANSACTION_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'merchantTransfer'
					}
				});
			});
		},
		function insertingNotification (data, callback) {
			notificationInsert(APP, {
				body: {
					referrer_id: req.body.refference_id,
					account_number: data.merchant.code.toUpperCase(),
					merchant_va: req.body.to_va,
					type: 1,
					title: 'Bayar Merchant',
					content: 'Transfer Merchant Sebesar Rp '+ (req.body.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' dari VA '+data.e2e.from_va+' Berhasil',
					read: 0,
					action_by: req.body.action_by,
					ip_address: req.body.ip_address
				}
			}, (err, result) => {
				callback(null, data);
			});
		},
		function output (data, callback) {
			data.e2e.merchant_code = data.merchant.code;
			data.e2e.merchant_id = data.merchant.id;

			callback(null, {
				code: 'TRANSFER_MERCHANT_SUCCESS',
				data: data.e2e
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};