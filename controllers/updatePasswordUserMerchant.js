"use strict";

const async = require('async');
const updatePasswordUserMerchant = require('../controllers/userMerchant/updatePassword.js');
/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "id"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.username) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "username"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.old_password) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "old_password"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.password) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "password"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			req.body.username = req.body.username.toUpperCase();

			callback(null, {});
		},

		function update (data, callback) {
			updatePasswordUserMerchant(APP, {
				body: {
					dataQuery: {
						id: req.body.id,
						username: req.body.username,
						old_password: APP.generator.generatePassword(req.body.old_password).encrypted,
					},
					dataUpdate: {
						password: APP.generator.generatePassword(req.body.password).encrypted
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'USER_MERCHANT_UPDATE_SUCCESS') return callback(result);

				callback(null, data);
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'UPDATE_PASSWORD_SUCCESS',
				data: req.body
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};