"use strict";

const async = require('async');

/**
* @type: service
* @req: id, merchant_id
* @return callback object
*/
module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "id"
					},
					info: {
						function: "deleteProduct"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "deleteProduct"
					}
				});

			req.body.merchant_id = req.body.merchant_id.toUpperCase();

			callback(null, true);
		},
		function deletingProducts (index, callback) {
			const params = APP.queries.storePro('delete_product', {
				body: {
					id: req.body.id,
					merchant_code: req.body.merchant_id
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(product => {
				if (product.length < 1) return callback(null, {
						code: 'DELETE_PRODUCT_FAILED',
						data: req.body,
						info: {
							function: "deleteProduct"
						}
					});

				if (product[0].code && product[0].code !== '00') return callback({
						code: product[0].code,
						message: product[0].message,
						data: req.body,
						info: {
							function: "deleteProduct"
						}
					});

				callback(null, {
					code: 'DELETE_PRODUCT_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "deleteProduct"
					}
				});
			});
		},
		function output (product, callback) {
			callback(null, product);
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};