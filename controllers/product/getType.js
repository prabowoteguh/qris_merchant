"use strict";

module.exports = function (APP, req, callback) {

    let query = {
        attributes: ['id', 'merchant_code', 'name', 'created_at'],
        order: [
            ['name', 'ASC'],
        ]
            
    }
    query.where = {
    	merchant_code : req.body.merchant_code
    };
    if (req.body.search) {
        query.where.$or = [{
            name: { $like: '%' + req.body.search + '%' }
        }]
    }
    if (req.body.limit) {
        query.limit =  parseInt(req.body.limit);
    }
    if (req.body.offset) {
        query.offset = parseInt(req.body.offset);
    }

    if (req.body.order) {
        switch(req.body.order) {
            case 'name_asc':
                query.order =  [['name', 'ASC']]
                break;
            case 'name_desc':
                query.order =  [['name', 'DESC']]
                break;
        }
    }


    let results = {
        id: 'recommended',
        merchant_code: req.body.merchant_code,
        name: 'Recommended',
        created_at: "2018-08-08T00:00:00.000Z"
    };
    

	APP.models.mysql.productType.findAndCountAll(query).then((rows) => {
        if(rows.rows && (rows.rows.length > 0)){
            if(req.body.app && (req.body.app == 'BTNPay')){
                rows.rows = (rows.rows).map(function(sensor){ return sensor.toJSON() });
                rows.rows.unshift(results);
            }
            return callback(null, {
                code: 'FOUND',
                data: rows,
            });
        }else{
            return callback(null, {
                code: 'NOT_FOUND',
                data: rows,
            });
        }
       
	}).catch((err) => {
        console.log(err)
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};