"use strict";

const async   = require('async');
const moment  = require('moment');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	var json 	= req.body;
	var output 	= {};
    async.waterfall([
        function(callback) {
            if (!json.username) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "username" } });
            if (!json.otp) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "otp" } });
            callback(null, true)
        },
        function(data, callback) {
            let query = {
                where: {
                    username: json.username,
                    otp_code: json.otp
                }
            }
            APP.models.mysql.userMerchant.findOne(query).then(result => {
                if (result) {
                    let time_db = moment(result.otp_expired);
                    console.log(result.otp_expired)
                    console.log(time_db)
                    if ((result.active_status == '1') && moment().isBefore(time_db)) {
                        callback(null, true)
                    } else if ((result.active_status == '1') && !moment().isBefore(time_db)) {
                        output = {
                            code: "EXPIRY_TOKEN"
                        }
                        callback(output)
                    } else if (result.active_status != "1") {
                        output = {
                            code: "USER_NOT_ACTIVE"
                        }
                        callback(output)
                    }
                } else {
                    output = {
                        code: 'TOKEN_NOT_FOUND'
                    }
                    callback(output);
                }

            }).catch(err => {
            	console.log(err);
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
        function(data, callback) {
            let query = {
                otp_code: null,
                otp_expired: '0000-00-00 00:00:00'
            }
            let where = {
                where: {
                    username: json.username,
                    otp_code: json.otp
                }
            }
            APP.models.mysql.userMerchant.update(query, where).then(result => {
                callback(null, {
                    code: "TOKEN_VERIFY"
                });
            }).catch(err => {

            	console.log(err);S
                callback({
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                })
            })
        },
    ], (err, result) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, result)
        }
    });
};