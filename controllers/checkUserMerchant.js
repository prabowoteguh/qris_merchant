	"use strict";

const async = require('async');
const userMerchantGet = require('../controllers/userMerchant/get.js');

/**
* @type: function
* @req: username
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function gettingUserMerchant (data, callback) {
			userMerchantGet(APP, {
				body: {
					id: req.body.id,
					username: req.body.username,
					limit: 1,
					offset: 0
				}
			}, (err, result) => {
				if (err) return callback(err);

				callback(null, result)
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};