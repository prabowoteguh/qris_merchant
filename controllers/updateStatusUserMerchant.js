"use strict";

const async = require('async');
const checkUserMerchant = require('../controllers/checkUserMerchant.js');
const userMerchantUpdate = require('../controllers/userMerchant/update.js');

/**
* @type: service
* @req: username
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.username) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "username"
					},
					info: {
						service: "updateStatusUserMerchant"
					}
				});

			var validateUsername = APP.validation.true(req.body.username);

			if (validateUsername !== true) return callback(validateUsername);

			callback(null, true);
		},
		function checkingUserMerchant (index, callback) {
			checkUserMerchant(APP, {
				body: {
					username: req.body.username
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'USER_MERCHANT_DOESNT_EXISTS',
						data: {},
						info: {
							service: 'updateStatusUserMerchant'
						}
					});

				callback(null, result.data);
			});
		},
		function updatingUserMerchant (merchant, callback) {
			// Wait store pro
			userMerchantUpdate(APP, {
				body: {
					dataQuery: {
						username: req.body.username
					},
					dataUpdate: {
						active_status: req.body.status
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				callback(null, {
					username: req.body.username,
					status: req.body.status
				});
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'USER_MERCHANT_UPDATE_SUCCESS',
				data: data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};