"use strict";

const async = require('async');
const checkHp = require('../controllers/checkHp.js');
const userMerchantInsert = require('../controllers/userMerchant/insert.js');
const emoneyServices = require('../config/emoneyServices.json');

/**
* @type: service
* @req: merchant_nama, merchant_jenis_usaha, merchant_nama_pic, merchant_hp,
*       email, telepon, alamat, kelurahan, kecamatan, kota, provinsi,
*       status, lat, long, created_by
* @return callback object
*/

module.exports = function (APP, req, callback) {
	var merchantVA = '';
	var id = '';

	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_nama) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_nama"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.merchant_jenis_usaha) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_jenis_usaha"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.merchant_nama_pic) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_nama_pic"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.merchant_hp) {
				return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_hp"
					},
					info: {
						service: "registration"
					}
				});	
			} else{
				if((req.body.merchant_hp).length > 12){
					return callback({
						code: 'INPUT_NOT_VALID',
						data: {
							missing_parameter: "merchant_hp"
						},
						info: {
							service: "registration"
						}
					});	
				}
			}

			if (!req.body.email) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "email"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.telepon) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "telepon"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.alamat) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "alamat"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.kelurahan) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "kelurahan"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.kecamatan) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "kecamatan"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.kota) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "kota"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.provinsi) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "provinsi"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.status) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "status"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.lat) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "lat"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.long) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "long"
					},
					info: {
						service: "registration"
					}
				});

			if (!req.body.created_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "created_by"
					},
					info: {
						service: "registration"
					}
				});

			var validateMerchantName = APP.validation.string(req.body.merchant_nama);
			var validateMerchantJenisUsaha = APP.validation.true(req.body.merchant_jenis_usaha);
			var validateMerchantNamaPIC = APP.validation.string(req.body.merchant_nama_pic);
			var validateMerchantHp = APP.validation.true(req.body.merchant_hp);
			var validateEmail = APP.validation.email(req.body.email);
			var validateTelepon = APP.validation.true(req.body.telepon);
			var validateAlamat = APP.validation.true(req.body.alamat);
			var validateKelurahan = APP.validation.true(req.body.kelurahan);
			var validateKecamatan = APP.validation.true(req.body.kecamatan);
			var validateKota = APP.validation.true(req.body.kota);
			var validateProvinsi = APP.validation.true(req.body.provinsi);
			var validateStatus = APP.validation.true(req.body.status);
			var validateLat = APP.validation.true(req.body.lat);
			var validateLong = APP.validation.true(req.body.long);
			var validateCreatedBy = APP.validation.true(req.body.created_by);

			if (validateMerchantName !== true) return callback(validateMerchantName);
			if (validateMerchantJenisUsaha !== true) return callback(validateMerchantJenisUsaha);
			if (validateMerchantNamaPIC !== true) return callback(validateMerchantNamaPIC);
			if (validateMerchantHp !== true) return callback(validateMerchantHp);
			if (validateEmail !== true) return callback(validateEmail);
			if (validateTelepon !== true) return callback(validateTelepon);
			if (validateAlamat !== true) return callback(validateAlamat);
			if (validateKelurahan !== true) return callback(validateKelurahan);
			if (validateKecamatan !== true) return callback(validateKecamatan);
			if (validateKota !== true) return callback(validateKota);
			if (validateProvinsi !== true) return callback(validateProvinsi);
			if (validateStatus !== true) return callback(validateStatus);
			if (validateLat !== true) return callback(validateLat);
			if (validateLong !== true) return callback(validateLong);
			if (validateCreatedBy !== true) return callback(validateCreatedBy);

			req.body.merchant_nama = (req.body.merchant_nama).replace(/\b\w/g, l => l.toUpperCase());
			callback(null, true);
		},
		function checkingMerchant (index, callback) {
			// checkHp(APP, {
			// 	body: {
			// 		merchant_hp: req.body.merchant_hp
			// 	}
			// }, (err, result) => {
			// 	if (err) {
			// 		if (err && err.code === 'NOT_FOUND') return callback(null, true);

			// 		return callback(err)
			// 	}

			// 	callback({
			// 		code: 'HP_ALREADY_EXISTS',
			// 		data: req.body,
			// 		info: {
			// 			service: 'registration'
			// 		}
			// 	});
			// });
			APP.models.mysql.merchant.findOne({
			 	where : {
					msisdn : req.body.merchant_hp,
					status: {
				        $in: [1,3,4,5] // Penecekan HP untuk Merchant dengan status 1 dan 3
				    }
				}
			}).then((rows) => {
				if(rows){
					callback({
						code: 'HP_ALREADY_EXISTS',
						data: req.body,
						info: {
							service: 'registration'
						}
					});
				}else{
					return callback(null, true)
				}
			}).catch((err) => {
				return callback({
					code: 'ERR_DATABASE',
					data: JSON.stringify(err)
				});
			});
		},
		function generateMerchantId (index, callback) {
			callback(null, APP.generator.generateMerchantId(req.body.merchant_nama));
		},
		// function generateMerchantVa (merchantId, callback) {
		// 	merchantVA = APP.generator.generateVA(req.body.merchant_hp);

		// 	callback(null, merchantId);
		// },
		function checkingHpInEmoneyCore (merchantId, callback) {
			APP.request.emoney(emoneyServices.checkHp, {
				phone_num: req.body.merchant_hp
			}, (err, result) => {
				console.log(result)

				if (err) {
					if (err.code !== 'ERR_DATABASE') return callback(null, merchantId);
					callback(err);
				};

				if (result && result.code === '00') {
					if (result.data.length > 0) return callback({
							code: 'HP_UNAVAILABLE',
							data: req.body,
							info: {
								service: 'registration'
							}
						});
				}

				callback(null, merchantId);
			});
		},
		// function insertMerchantVAInEmoneyCore (merchantId, callback) {
		// 	APP.request.emoney(emoneyServices.register, {
		// 		phone_num: req.body.merchant_hp,
		// 		first_name: req.body.merchant_nama,
		// 		email: req.body.email,
		// 		type: "2",
		// 		merchant_va: merchantVA
		// 	}, (err, result) => {
		// 		if (err) return callback(err);

		// 		if (result && result.code !== '00') return callback(result);

		// 		callback(null, merchantId, result);
		// 	});
		// },
		function insertingMerchant (merchantId, callback) {
			const params = APP.queries.storePro('registration', {
				body: {
					code: merchantId,
					name: req.body.merchant_nama,
					business_type_id: req.body.merchant_jenis_usaha,
					owner: req.body.merchant_nama_pic,
					msisdn: req.body.merchant_hp,
					email: req.body.email,
					phone: req.body.telepon,
					address: req.body.alamat,
					kelurahan: req.body.kelurahan,
					kecamatan: req.body.kecamatan,
					city: req.body.kota,
					province: req.body.provinsi,
					zipcode: req.body.zipcode,
					lat: req.body.lat,
					long: req.body.long,
					account_name: req.body.merchant_nama,
					account_number: merchantVA,
					bank_code_id : '200',
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(account => {
				if (account[0].code && account[0].code !== '00') return callback({
						code: account[0].code,
						message: account[0].message,
						data: {
							q_code: account[0].code,
							q_message: account[0].message
						},
						info: {
							service: "registration"
						}
					});

				id = account[0].id;

				callback(null, merchantId, account);
			}).catch(err => {
				callback({
					code: 'REGISTRATION_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: "registration"
					}
				});
			});
		},
		function generatePassword (merchantId,  account, callback) {
			callback(null, merchantId, account, APP.generator.generatePassword());
		},
		function insertingUserMerchant (merchantId, account, password, callback) {
			userMerchantInsert(APP, {
				body: {
					merchant_id: id,
					type: '1',
					username: account[0].merchant_code,
					name: req.body.merchant_nama,
					msisdn: req.body.merchant_hp,
					password: password.encrypted,
					menu: '',
					active_status: '0',
					image: '',
					action_by: id,
					ip_address: req.body.ip_address
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'USER_MERCHANT_INSERT_SUCCESS') return callback(result);

				callback(null, merchantId, account, password);
			});
		},
		function output (merchantId, account, password, callback) {
			req.body.merchant_code = account[0].merchant_code;
			// req.body.username = req.body.merchant_id;
			// req.body.password = password.password;
			// req.body.account_number = merchantVA;
			// req.body.account_name = req.body.merchant_nama;

			callback(null, {
				code: 'MERCHANT_REGISTER_SUCCESS',
				data: req.body
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};