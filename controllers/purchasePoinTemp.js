	"use strict";

const async = require('async');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.refference_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "refference_id"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.update_point) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "update_point"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.operation) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "operation"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.created_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "created_by"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.destination) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "destination"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.source) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "source"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			if (!req.body.emoney_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "emoney_va"
					},
					info: {
						function: "requestPoinTmp"
					}
				});

			var validateReffId = APP.validation.true(req.body.refference_id);

			if (validateReffId !== true) return callback(validateReffId);

			callback(null, {});
		},
		function filteringDestination (data, callback) {
			data.storePro = APP.queries.storePro('create_customer_merchant_budget', {
				body: {
					merchant_id: req.body.merchant_id,
					emoney_va: req.body.emoney_va,
					refference_id: req.body.refference_id,
					operation: req.body.operation,
					update_point: req.body.update_point,
					source: req.body.source,
					destination: req.body.destination,
					description_req: req.body.description,
					action_by: req.body.created_by,
					ip_address: req.body.ip_address
				}
			});

			return callback(null, data);
		},
		function insertingrequestPoinTmp (data, callback) {
			APP.db.sequelize.query(data.storePro.query, data.storePro.bind).then(reply => {console.log(reply)
				if (reply[0].code && reply[0].code !== '00') {
					return callback({
						code: 'ERR_CREATE_TMP_POIN',
						data: req.body,
						info: {
							db: reply[0],
							service: "requestPoinTmp"
						}
					});
				}

				callback(null, {
					code: 'TMP_POIN_REQUEST_SUCCESS',
					data: reply[0]
				});
			}).catch(err => {console.error(err);
				callback({
					code: 'ERR_CREATE_TMP_POIN',
					data: req.body,
					info: {
						err: JSON.stringify(err),
						service: "requestPoinTmp"
					}
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: {
					id: output.data.id,
					existing_point: output.data.existing_point,
					end_point: output.data.end_point
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};