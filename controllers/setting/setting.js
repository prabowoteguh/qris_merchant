"use strict";

const async = require('async');
var output  = {};

module.exports = (APP, req, callback) => {
    var json = req.body;
    var final_result = {}
    async.waterfall([
        function(callback) {
            var query = {
                attributes: ['params', 'value'],
                where: {
                    params: {
                        $or: ['REG_CUSTOMER', 'SPLIT_BILL', 'TOKEN_ADMIN']
                    }
                }
            };

            APP.models.mysql.setting.findAll(query).map(el => el.get({ plain: true })).then(result => {
                if (result.length > 0) {
                    result.forEach(function(data) {
                        switch (data.params) {
                            case 'REG_CUSTOMER':
                                final_result.interval_otp = data.value;
                                break;
                            case 'SPLIT_BILL':
                                final_result.interval_split_bill = data.value;
                                break;
                            case 'TOKEN_ADMIN':
                                final_result.interval_token_admin = data.value;
                                break;
                        }
                    });
                    output = {
                        code: 'FOUND',
                        data: final_result
                    }
                    callback(null, output)
                } else {
                    output = {
                        code: 'NOT_FOUND'
                    }
                    callback(output)
                }
            }).catch(err => {
                console.log(err)
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
    ], (err, result) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, result)
        }
    });
};