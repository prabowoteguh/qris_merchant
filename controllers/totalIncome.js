"use strict";

const async = require('async');

/**
* @type: service
* @req: merchant_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function gettingData (data, callback) {
			let params = APP.queries.storePro('total_income', {
				body: {
					mode: req.body.mode,
					merchant_code: req.body.merchant_id,
					start_date: req.body.start_date,
					end_date: req.body.end_date
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(income => {
				if (income.length < 1) return callback({
						code: 'REPORT_NOT_FOUND',
						data: [],
						info: {
							function: "totalIncome"
						}
					});

				if (income[0].code && income[0].code !== '00') return callback({
						code: 'REPORT_NOT_FOUND',
						message: income[0].message,
						data: {
							q_code: income[0].code,
							q_message: income[0].message
						},
						info: {
							function: "totalIncome"
						}
					});

				data.data = income;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "totalIncome"
					}
				});
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'REPORT_FOUND',
				data: {
					rows: data.data,
					count: data.data.length
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};