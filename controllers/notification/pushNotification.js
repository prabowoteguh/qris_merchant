"use strict";

const async = require('async');
const request = require('../../functions/request');

exports.save = (APP, req, callback) => {
    var id_notif;
    var emoney_va = req.pushnotif.emoney_va;
    var output = {};

    async.waterfall([
        function(callback) {
            let query = {
                referrer_id: req.pushnotif.referrer_id || null,
                emoney_va: emoney_va,
                account_number: req.pushnotif.account_number,
                type: req.pushnotif.type,
                title: req.pushnotif.title,
                content: req.pushnotif.content,
                ip_address: req.pushnotif.ip_address || '127.0.0.1'
            };

            APP.models.mysql.notification.create(query).then(result => {
                id_notif = result.toJSON().id
                callback(null, true)
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
        function getDeviceKey(data, callback) {
            var query = {
                attributes: ['device_key', 'device_type'],
                where: {
                    // emoney_va: emoney_va
                    username: req.pushnotif.account_number
                }
            }
            APP.models.mysql.userMerchant.findOne(query).then(result => {
                if (result) {
                    result = result.toJSON();

                    callback(null, result.device_key, result.device_type)
                } else {
                    output = {
                        code: 'NOT_FOUND',
                    }
                    callback(output);
                }
            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
        function send(device_key, device_type, callback) {
            let payload = {
                data: {
                    id: id_notif || null,
                    referrer_id: req.pushnotif.referrer_id || null,
                    emoney_va: emoney_va,
                    type: req.pushnotif.type,
                    message: req.pushnotif.content,
                    body: req.pushnotif.content,
                    title: req.pushnotif.title
                },
                registration_ids: [device_key],
                priority: 'high',
                message: {
                    body: req.pushnotif.content,
                    title: req.pushnotif.title
                },
                notification: {
                    body: req.pushnotif.content,
                    title: req.pushnotif.title
                }
            };

            request.notif('https://fcm.googleapis.com/fcm/send', payload, function(err, result) {});
            callback(null, true)
        }
    ], (err, result) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, result)
        }
    });
};