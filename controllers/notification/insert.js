"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.insert('notification', req, APP.models);

	APP.models.mysql.notification.build(params).save().then(notification => {
		return callback(null, {
			code: 'NOTIFICATION_INSERT_SUCCESS',
			data: notification.dataValues || params
		});
	}).catch(err => {
		if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
				code: 'ERR_DUPLICATE',
				data: params
			});

		if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
				code: 'ERR_NOTHING',
				data: params
			});

		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};