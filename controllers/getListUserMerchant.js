"use strict";

const async = require('async');
const userMerchantGet = require('../controllers/userMerchant/get.js');

/**
* @type: service
* @req: limit, offset
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, true);
		},
		function gettingList (index, callback) {
			userMerchantGet(APP, {
				body: {
					search: req.body.search,
					limit: req.body.limit,
					offset: req.body.offset,
					order: req.body.order,
					merchant_id: req.body.merchant_id,
					active_status: req.body.status
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'USER_MERCHANT_NOT_FOUND',
						data: {
							rows: [],
							count: 0
						}
					});

				callback(null, result.data);
			});
		},
		function output (userMerchant, callback) {
			callback(null, {
				code: 'USER_MERCHANT_FOUND',
				data: userMerchant
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};
