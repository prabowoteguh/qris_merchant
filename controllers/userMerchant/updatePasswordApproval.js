"use strict";

module.exports = function (APP, t, req, callback) {
	var params = APP.queries.update('user_merchant_password_approval', req, APP.models);

	if (Object.keys(req.body.dataUpdate).length < 1) return callback({
			code: 'ERR_NOTHING'
		});

	APP.models.mysql.userMerchant.update(params.dataUpdate, params.dataQuery, {transaction: t}).then(merchant => {
		if (!merchant || (merchant && !merchant[0])) return callback({
				code: 'ERR_NOTHING',
			});

		return callback(null, {
			code: 'USER_MERCHANT_UPDATE_SUCCESS',
		});
	}).catch(err => {
		if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
				code: 'ERR_NOTHING',
				data: err
			});

		if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
					code: 'ERR_DUPLICATE',
					data: err
				});
		console.log(err)
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};