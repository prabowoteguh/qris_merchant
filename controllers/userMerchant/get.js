"use strict";

module.exports = function (APP, req, callback) {
	// var params = APP.queries.select('user_merchant', req, APP.models);
	let query = {
        order: [
            ['name', 'ASC'],
        ]
    }
    if (req.body.limit) {
        query.limit =  parseInt(req.body.limit);
    }
    if (req.body.offset) {
        query.offset = parseInt(req.body.offset);
    }
    query.where = {};
    if (req.body.search) {
        query.where.$or = [{
            name: { $like: '%' + req.body.search + '%' }
        }]
    }

    if (req.body.order) {
        switch(req.body.order) {
            case 'name_asc':
                query.order =  [['name', 'ASC']]
                break;
            case 'name_desc':
                query.order =  [['name', 'DESC']]
                break;
            case 'msisdn_asc':
                query.order =  [['msisdn', 'ASC']]
                break;
            case 'msisdn_desc':
                query.order =  [['msisdn', 'DESC']]
                break;
            case 'status_asc':
                query.order =  [['active_status', 'ASC']]
                break;
            case 'status_desc':
                query.order =  [['active_status', 'DESC']]
                break;
        }
    }

    if (req.body.username) {
        query.where.username = req.body.username;
    }

    if (req.body.id) {
        query.where.id = req.body.id;
    }

    if (req.body.msisdn) {
        query.where.msisdn = req.body.msisdn;
    }

    if (req.body.merchant_id) {
        query.where.merchant_id = req.body.merchant_id;
    }

    if (req.body.type) {
        query.where.type = req.body.type;
    }
    
    if (req.body.name) {
        query.where.name = req.body.name;
    }

    if (req.body.password) {
        query.where.password = req.body.password;
    }

    if (req.body.menu) {
        query.where.menu = req.body.menu;
    }

    if (req.body.active_status) {
        query.where.active_status = req.body.active_status;
    }

    if (req.body.image) {
        query.where.image = req.body.image;
    }

	if (req.body.action_by) {
        query.where.action_by = req.body.action_by;
    }

    if (req.body.ip_address) {
        query.where.ip_address = req.body.ip_address;
    }

	APP.models.mysql.userMerchant.findAndCountAll(query).then((rows) => {
		console.log(rows.count)
		return callback(null, {
			code: (rows.rows && (rows.rows.length > 0)) ? 'FOUND' : 'NOT_FOUND',
			data: rows,
			info: {
				dataCount: rows.count
			}
		});
	}).catch((err) => {
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};
