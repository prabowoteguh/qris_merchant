"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.insert('user_merchant', req, APP.models);

	APP.models.mysql.userMerchant.build(params).save().then(userMerchant => {
		return callback(null, {
			code: 'USER_MERCHANT_INSERT_SUCCESS',
			data: userMerchant.dataValues || params
		});
	}).catch(err => {
		if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
				code: 'ERR_DUPLICATE',
				data: params
			});

		if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
				code: 'ERR_NOTHING',
				data: params
			});
		console.log(err)
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};