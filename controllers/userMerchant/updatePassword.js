"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.update('user_merchant_password', req, APP.models);

	if (Object.keys(req.body.dataUpdate).length < 1) return callback({
			code: 'PASSWORD_FALSE',
			data: params
		});

	APP.models.mysql.userMerchant.update(params.dataUpdate, params.dataQuery).then(merchant => {
		console.log(merchant)
		if (!merchant || (merchant && !merchant[0])) return callback(null, {
				code: 'PASSWORD_FALSE',
				data: params
			});

		return callback(null, {
			code: 'USER_MERCHANT_UPDATE_SUCCESS',
			data: params
		});
	}).catch(err => {
		console.log(err)
		if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
				code: 'PASSWORD_FALSE',
				data: params
			});

		if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
					code: 'ERR_DUPLICATE',
					data: params
				});

		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};