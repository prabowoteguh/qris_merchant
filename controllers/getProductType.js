"use strict";

const async = require('async');
const productTypeGet = require('../controllers/product/getType.js');
/**
* @type: service
* @req: limit, offset, merchant_id, product_id
* @return callback object
*/

// module.exports = function (APP, req, callback) {
// 	async.waterfall([
// 		function validation (callback) {
// 			if (!req.body.merchant_id) return callback({
// 					code: 'MISSING_KEY',
// 					data: {
// 						missing_parameter: "merchant_id"
// 					},
// 					info: {
// 						function: "getProductType"
// 					}
// 				});

// 			req.body.merchant_id = req.body.merchant_id.toUpperCase();

// 			callback(null, true);
// 		},
// 		function gettingProductTypes (index, callback) {
// 			const params = APP.queries.storePro('get_product_type', {
// 				body: {
// 					merchant_code: req.body.merchant_id
// 				}
// 			});
// 			let products = [];

// 			APP.db.sequelize.query(params.query, params.bind).then(product => {
// 				if (product.length < 1) return callback(null, {
// 						code: 'PRODUCT_TYPE_NOT_FOUND',
// 						data: [],
// 						info: {
// 							function: "getProductType"
// 						}
// 					});

// 				if (product[0].code && product[0].code !== '00') return callback({
// 						code: 'PRODUCT_TYPE_NOT_FOUND',
// 						data: {
// 							q_code: product[0].code,
// 							q_message: product[0].message
// 						},
// 						info: {
// 							function: "getProductType"
// 						}
// 					});

// 				product.map((val) => {
// 					products.push({
// 						id: val.id,
// 						name: val.name,
// 						created_at: val.created_at
// 					});
// 				});

// 				callback(null, {
// 					code: 'PRODUCT_TYPE_FOUND',
// 					data: {
// 						rows: products,
// 						count: products.length
// 					}
// 				});
// 			}).catch(err => {
// 				callback({
// 					code: 'GENERAL_ERR',
// 					data: {},
// 					info: {
// 						err: JSON.stringify(err),
// 						function: "getProductType"
// 					}
// 				});
// 			});
// 		},
// 		function output (product, callback) {
// 			callback(null, product);
// 		},
// 	], (err, result) => {
// 		if (err) return callback(err);

// 		callback(null, result);
// 	});
// };

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "getProductType"
					}
				});

			req.body.merchant_id = req.body.merchant_id.toUpperCase();
			callback(null, true);
		},
		function gettingList (index, callback) {
			productTypeGet(APP, {
				body: {
					merchant_code : req.body.merchant_id,
					limit	: req.body.limit,
					offset	: req.body.offset,
					search	: req.body.search,
					order 	: req.body.order,
					app 	: req.body.app
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'PRODUCT_TYPE_NOT_FOUND',
						data: {
							rows: [],
							count: 0
						}
					});

				callback(null, result.data);
			});
		},
		function output (userMerchant, callback) {
			callback(null, {
				code: 'PRODUCT_TYPE_FOUND',
				data: userMerchant
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};