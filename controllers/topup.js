"use strict";

const async = require('async');
const checkVa = require('../controllers/checkVa.js');
const emoneyServices = require('../config/emoneyServices.json');
const insertNotification = require('../controllers/notification/insert.js');

/**
* @type: service
* @req: refference_id, channel_id, channel_name, emoney_va,
*       from_account, amount, narration, created_by
* @return callback object
*/

module.exports = function (APP, req, callback) {
	let channel_name = '';

	async.waterfall([
		function validation (callback) {
			if (!req.body.refference_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "refference_id"
					},
					info: {
						service: "topUp"
					}
				});

			if (!req.body.channel_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "channel_id"
					},
					info: {
						service: "topUp"
					}
				});

			if (!req.body.channel_name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "channel_name"
					},
					info: {
						service: "topUp"
					}
				});

			if (!req.body.created_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "created_by"
					},
					info: {
						service: "topUp"
					}
				});

			var validateRefferenceId = APP.validation.true(req.body.refference_id);
			var validateChannelId = APP.validation.true(req.body.channel_id);
			var validateChannelName = APP.validation.true(req.body.channel_name);
			var validateAmount = APP.validation.amount(req.body.amount);

			if (validateRefferenceId !== true) return callback(validateRefferenceId);
			if (validateChannelId !== true) return callback(validateChannelId);
			if (validateChannelName !== true) return callback(validateChannelName);
			if (validateAmount !== true) return callback(validateAmount);

			callback(null, {});
		},
		function bundlingMerchantVA (data, callback) {
			APP.generator.bundleVa(APP, req.body.from_account, (err, result) => {
				if (err) return callback(err);

				data.merchant_va = result.data.emoney_va;

				callback(null, data);
			});
		},
		function checkingMerchant (data, callback) {
			if (req.body.channel_id.toLowerCase() === 'merchant') {
				checkVa(APP, {
					body: {
						merchant_va: data.merchant_va
					}
				}, (err, result) => {
					if (err) {
						if (err.code === 'NOT_FOUND') return callback({
								code: 'MERCHANT_DOESNT_EXISTS',
								data: req.body,
								info: {
									service: 'topUp'
								}
							});

						return callback(err);
					}

					if (result && result.code !== 'FOUND') return callback(result);

					if (result && result.data.code.toLowerCase() !== req.body.channel_name.toLowerCase()) return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: req.body,
							info: {
								service: 'topUp'
							}
						});

					channel_name = result.data.code;

					callback(null, data);
				});
			} else {
				channel_name = req.body.channel_name;

				callback(null, data);
			}
		},
		function emoneye2e (data, callback) {
			APP.request.emoney(emoneyServices.e2e, {
				type: '3',
				from_va: data.merchant_va,
				to_va: req.body.emoney_va,
				amount: req.body.amount,
				narration: req.body.narration,
				pin: req.body.pin,
				actionType: 'TOPUP_MERCHANT'
			}, (err, result) => {
				if (err) {
					err.code = 'PIN_FALSE';

					return callback(err);
				};

				if (result && result.code !== '00') return callback(result);

				return callback(null, result.data);
			});
		},
		function insertMerchantTransaction (data, callback) {
			const params = APP.queries.storePro('insert_transaction', {
				body: {
					requestId: req.body.refference_id,
					account_number: req.body.channel_name,
					merchant_va: data.from_va,
					wallet_type_code: 'SOF01',
					transactions_type_code: 'TRT011',
					biller_type: '',
					biller_code: data.to_va,
					biller_name: data.to_va_name,
					nominal: data.amount,
					no_jurnal: data.journal,
					note: req.body.narration,
					fee: '0',
					admin_fee: '0',
					data: '',
					status: '1',
					action_by: req.body.action_by,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: 'MERCHANT_TOPUP_FAILED',
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							service: 'topUp'
						}
					});

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'MERCHANT_TOPUP_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'topUp'
					}
				});
			});
		},
		function insertingNotification (data, callback) {
			insertNotification(APP, {
				body: {
					referrer_id: req.body.refference_id,
					account_number: req.body.channel_name,
					merchant_va: data.from_va,
					type: 2,
					title: 'Top Up',
					content: 'Transaksi Top Up Sebesar Rp '+ (data.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' Untuk VA '+data.to_va+' Berhasil',
					read: 0,
					action_by: req.body.action_by,
					ip_address: req.body.ip_address
				}
			}, (err, result) => {
				callback(null, data);
			});
		},
		function output (data, callback) {
			data.created_by = req.body.created_by;
			
			callback(null, {
				code: 'MERCHANT_TOPUP_SUCCESS',
				data: data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};