"use strict";

const async = require('async');
const vascommkit = require('vascommkit');
const getProduct = require('../controllers/getProduct.js');
const checkMerchant = require('../controllers/checkMerchant.js');
const merchantTransactionDelete = require('../controllers/merchantTransaction/delete.js');
const qrGenerate = require('../controllers/qrisManagement/generateQr');
const moment = require('moment');
const randomstring = require('randomstring')
/**
* @type: service
* @req: products, merchant_id
* @return callback object
*/

const randomStr = randomstring.generate({
	length: 2,
	charset: 'numeric'
});

module.exports = function (APP, req, callback) {
	let merchant_id_key;
	async.waterfall([
		function validation (callback) {
			if (!req.body.products) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "products"
					},
					info: {
						service: "purchaseInquiry"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						service: "purchaseInquiry"
					}
				});

			var validateProductId = APP.validation.true(req.body.product_id);
			var validateMerchantId = APP.validation.true(req.body.merchant_id);

			if (validateProductId !== true) return callback(validateProductId);
			if (validateMerchantId !== true) return callback(validateMerchantId);

			req.body.merchant_id = req.body.merchant_id.toUpperCase();
			req.body.customer_id = req.body.customer_id || req.body.user;

			callback(null, {});
		},
		function checkingMerchant (data, callback) {
			checkMerchant(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: {},
						info: {
							service: 'purchaseInquiry'
						}
					});

				data.merchant = result.data;

				callback(null, data);
			});
		},
		function validatingProducts (data, callback) { // PRE
			const len = req.body.products.length;
			const lenX = len - 1;
			const start = 0;
			var n = 0;
			data.products = [];
			data.total = 0;

			vascommkit.looper(start, len, (num) => {
				data.products[num] = req.body.products[num];
				req.body.products[num].invalid = false;

				getProduct(APP, {
					body: {
						merchant_id: req.body.merchant_id,
						product_id: req.body.products[num].id,
						limit: 1,
						offset: 0
					}
				}, (err, result) => {
					if (err) return callback(err);

					if ((result && result.code !== 'PRODUCT_FOUND') || (result && result.data.count < 1)) {
						req.body.products[num].invalid = true;

						return callback({
							code: 'PRODUCT_NOT_VALID',
							data: req.body
						});
					}

					data.products[num].name = result.data.rows[0].name;
					data.products[num].price = result.data.rows[0].price;
					data.products[num].ori_qty = result.data.rows[0].qty;
					data.products[num].image = result.data.rows[0].image;
					data.products[num].ori_total = String(Number(data.products[num].price) * Number(data.products[num].ori_qty));
					data.products[num].total = String(Number(data.products[num].price) * Number(data.products[num].qty));
					data.total += Number(data.products[num].total);

					data.products[num].ori_qty = undefined;
					data.products[num].ori_total = undefined;

					if (n === lenX) {
						data.total = String(data.total);

						return callback(null, data);
					}

					n++;
				});
			});
		},
		function insertingToTransaction (data, callback) {
			const params = APP.queries.storePro('insert_transaction', {
				body: {
					// requestId: '', Existing
					requestId: moment().format('YYYYMMDDHHmmss').concat(randomStr),
					account_number: req.body.merchant_id,
					// merchant_va: '', Existing
					merchant_va: data.merchant.account_num,
					// wallet_type_code: '', Existing
					wallet_type_code: 'SOF01',
					transactions_type_code: 'TRT009',
					biller_type: '',
					biller_code: req.body.customer_id,
					biller_name: '',
					nominal: data.total,
					no_jurnal: '12345',
					note: req.body.narration,
					fee: '0',
					admin_fee: '0',
					data: JSON.stringify(data.products),
					status: '0',
					action_by: req.body.action_by,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: transaction[0].code,
						message: transaction[0].message,
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							service: 'purchaseInquiry'
						}
					});

				data.invoice = transaction[0].invoice;
				data.var_transactions_merchant_id = transaction[0].var_transactions_merchant_id;
				data.requestId = transaction[0].requestId;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'INSERT_TRANSACTION_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'purchaseInquiry'
					}
				});
			});
		},
		function insertDetails (data, callback) {
			const len = data.products.length;
			var start = 0;
			var params = [];

			vascommkit.looper(start, len, (num) => {
				params[num] = {};
				params[num].transactions_merchant_id = data.var_transactions_merchant_id;
				params[num].product_id = data.products[num].id;
				params[num].quantity = data.products[num].qty;
				params[num].name = data.products[num].name;
				params[num].price = data.products[num].price;
				params[num].total = data.products[num].total;
				params[num].status = '1';
				params[num].action_by = req.body.action_by;
				params[num].ip_address = req.body.ip_address;
			}, () => {
				APP.models.mysql.merchantTransactionDetail.bulkCreate(params).then(() => { // Notice: There are no arguments here, as of right now you'll have to...
				  callback(null, data);
				}).catch(err => {
				  merchantTransactionDelete(APP, {
				  	body: {
				  		id: data.var_transactions_merchant_id
				  	}
				  }, (err, result) => {
				  	callback(null, data);
				  });
				});
			});
		},
		// Enhancement Generate QR when Inquiry Purchase Get Data User Merchant
		function getDataMerchant(data, callback) {
			var query = APP.queries.rawQueris('get_merchant_by_user', {
                body: {
                    username: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => {
				merchant_id_key = rows[0][0].merchant_id; 
				callback(null, data)
            })
		},
		// Enhancement Generate QR when Inquiry Purchase
		function generateQr(data, callback) {
			qrGenerate(APP, {
				body: {
					merchant_id: merchant_id_key,
					point_of_initiation: '12',
					transaction_amount: data.total,
					referenceLabel: data.requestId
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== '00') return callback({
					code: 'GENERATE_QR_FAILED',
					data: {},
					info: {
						service: 'generateQr'
					}
				});

				data.qr = result.data;

				callback(null, data);
			})
		},
		// Enhancement Get Data Merchant
		function getDataMerchant(data, callback) {
			var query = APP.queries.rawQueris('check_merchant_by_code', {
                body: {
                    merchant_code: req.body.merchant_id
                }
            });

            APP.db.sequelize.query(query.query, query.bind).then(rows => { 
                data.merchant = rows[0][0]
                
                callback(null, data);
            })
		},
		function output (data, callback) {
			callback(null, {
				code: 'PURCHASE_INQUIRY_SUCCESS',
				data: {
					merchant_id: req.body.merchant_id,
					merchant: {
						merchant_address: data.merchant.address,
						merchant_kelurahan: data.merchant.kelurahan,
						merchant_kecamatan: data.merchant.kecamatan,
						merchant_city: data.merchant.city,
						merchant_province: data.merchant.province,
						merchant_zipcode: data.merchant.zipcode,
						merchant_lat: data.merchant.lat,
						merchant_long: data.merchant.long
					},
					user: req.body.customer_id,
					narration: req.body.narration,
					invoice: data.invoice,
					products: {
						rows: data.products,
						count: data.products.length
					},
					qr: data.qr,
					subtotal: data.total,
					total: data.total,
					tax: '0',
					fee: '0',
					status: '0'
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};