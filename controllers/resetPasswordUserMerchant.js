"use strict";

const async = require('async');
const { result } = require('lodash');
const updatePasswordUserMerchant = require('../controllers/userMerchant/updatePassword.js');
/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	var output = {};
	async.waterfall([
		function validation (callback) {
			
			if (!req.body.username) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "username"
					},
					info: {
						function: "createUserMerchant"
					}
				});

	
			if (!req.body.password) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "password"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			req.body.username = req.body.username.toUpperCase();

			callback(null, {});
		},
		function(data, callback) {
            let query = {
                where: {
                    username: req.body.username,
                }
            }
            APP.models.mysql.userMerchant.findOne(query).then(result => {
                if (result) {
                    result = result.toJSON();
                    if (result.active_status != "1") {
                        output = {
                            code: "USER_NOT_ACTIVE"
                        }
                        callback(output)
                    } else {
                        callback(null, result)
                    }
                } else {
                    output = {
                        code: 'NOT_FOUND'
                    }
                    callback(output);
                }

            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
		function checkingMerchant (user, callback) {
			if (user.email != null) {
				var result = user;
				callback(null, user, result)
			}else{
				APP.models.mysql.merchant.findOne({
					where: {
						code : user.username
					}
				}).then(result => {
				   if (result) {
						callback(null, user, result)
					} else {
						output = {
							code: 'NOT_FOUND'
						}
						callback(output);
					}
				});
			}
        },
		function update (user, merchant, callback) {
			updatePasswordUserMerchant(APP, {
				body: {
					dataQuery: {
						username: req.body.username,
					},
					dataUpdate: {
						password: APP.generator.generatePassword(req.body.password).encrypted
					}
				}
			}, (err, result) => {
				if (err){
					return callback(err);	
				}else if (result && result.code !== 'USER_MERCHANT_UPDATE_SUCCESS') {
					return callback(null, user, merchant);
				}else{
					return callback(null, user, merchant);
				}
			});
		},
		function output (user, merchant, callback) {
			callback(null, {
				code: 'UPDATE_PASSWORD_SUCCESS',
				data:   {
					email     : merchant.email,
                	username  : user.username,
                	name      : user.name
                }
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};