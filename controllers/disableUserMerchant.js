"use strict";

const async = require('async');
const checkUserMerchant = require('../controllers/checkUserMerchant.js');
const userMerchantUpdate = require('../controllers/userMerchant/update.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.user_merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "user_merchant_id"
					},
					info: {
						function: "disableUserMerchant"
					}
				});

			callback(null, {});
		},
		function checkingUserMerchant (data, callback) {
			checkUserMerchant(APP, {
				body: {
					id: req.body.user_merchant_id
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback({
							code: 'MERCHANT_ALREADY_EXISTS',
							data: req.body
						});

					return callback(err);
				}

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_ALREADY_EXISTS',
						data: req.body
					});

				callback(null, data);
			});
		},
		function insertingUserMerchant (data, callback) {
			userMerchantUpdate(APP, {
				body: {
					dataQuery: {
						id: req.body.user_merchant_id
					},
					dataUpdate: {
						active_status: '0'
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				callback(null, data);
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'USER_MERCHANT_DISABLE_SUCCESS',
				data: req.body
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};