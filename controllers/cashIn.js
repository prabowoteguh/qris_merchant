"use strict";

const async = require('async');
const checkVa = require('../controllers/checkVa.js');
const emoneyServices = require('../config/emoneyServices.json');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.refference_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "refference_id"
					},
					info: {
						service: "cashIn"
					}
				});

			if (!req.body.channel_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "channel_id"
					},
					info: {
						service: "cashIn"
					}
				});

			if (!req.body.channel_name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "channel_name"
					},
					info: {
						service: "cashIn"
					}
				});

			var validateRefferenceId = APP.validation.true(req.body.refference_id);
			var validateChannelId = APP.validation.true(req.body.channel_id);
			var validateChannelName = APP.validation.true(req.body.channel_name);

			if (validateRefferenceId !== true) return callback(validateRefferenceId);
			if (validateChannelId !== true) return callback(validateChannelId);
			if (validateChannelName !== true) return callback(validateChannelName);

			callback(null, {});
		},
		function checkingMerchant (data, callback) {
			if (req.body.channel_id.toLowerCase() === 'merchant') {
				checkVa(APP, {
					body: {
						merchant_va: req.body.emoney_va
					}
				}, (err, result) => {
					if (err) {
						if (err.code === 'NOT_FOUND') return callback({
								code: 'MERCHANT_DOESNT_EXISTS',
								data: req.body,
								info: {
									service: 'cashIn'
								}
							});

						return callback(err);
					}

					if (result && result.code !== 'FOUND') return callback(result);

					if (result && result.data.code.toLowerCase() !== req.body.channel_name.toLowerCase()) return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: req.body,
							info: {
								service: 'cashIn'
							}
						});

					data.channel_name = result.data.code;

					callback(null, data);
				});
			} else {
				checkVa(APP, {
					body: {
						merchant_va: req.body.emoney_va
					}
				}, (err, result) => {
					if (err) {
						if (err.code === 'NOT_FOUND') return callback({
								code: 'MERCHANT_DOESNT_EXISTS',
								data: req.body,
								info: {
									service: 'cashIn'
								}
							});

						return callback(err);
					}

					if (result && result.code !== 'FOUND') return callback(result);

					data.channel_name = result.data.code;

					callback(null, data);
				});
			}
		},
		function emoneyCashIn (data, callback) {
			APP.request.emoney(emoneyServices.cashIn, {
				refference_id: req.body.refference_id,
				channel_id: req.body.channel_id,
				channel_name: data.channel_name,
				emoney_va: req.body.emoney_va,
				from_account: req.body.from_account,
				amount: req.body.amount,
				narration: req.body.narration
			}, (err, result) => {
				if (err) {
					err.code = 'MERCHANT_CASHIN_FAILED';

					return callback(err);
				};

				if (result && result.code !== '00') return callback(result);

				data.cashIn = result.data;

				return callback(null, data);
			});
		},
		function insertMerchantTransaction (data, callback) {
			const params = APP.queries.storePro('insert_transaction', {
				body: {
					requestId: req.body.refference_id,
					account_number: data.channel_name,
					merchant_va: req.body.emoney_va,
					wallet_type_code: 'SOF01',
					transactions_type_code: 'TRT014',
					biller_type: '',
					biller_code: req.body.from_account,
					biller_name: '',
					nominal: req.body.amount,
					no_jurnal: data.cashIn.journal,
					note: req.body.narration,
					fee: '0',
					admin_fee: '0',
					data: '',
					status: '1',
					action_by: req.body.created_by,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: 'MERCHANT_CASHIN_FAILED',
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							service: 'cashIn'
						}
					});

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'MERCHANT_CASHIN_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'cashIn'
					}
				});
			});
		},
		function output (data, callback) {
			data.created_by = req.body.created_by;
			
			callback(null, {
				code: 'MERCHANT_CASHIN_SUCCESS',
				data: data.cashIn
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};