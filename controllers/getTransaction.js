"use strict";

const async = require('async');
const getMerchantTransaction = require('../controllers/merchantTransaction/get.js');
const _ = require('lodash');

/**
* @type: service
* @req: limit, offset, merchant_id, start_date, end_date
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {

			if(req.body.start_date){
				var validateDate = APP.validation.date(req.body.start_date);
				if (validateDate !== true) return callback(validateDate);
			}

			if(req.body.end_date){
				var validateEndDate = APP.validation.date(req.body.end_date);
				if (validateEndDate !== true) return callback(validateEndDate);
			}


			callback(null, true);
		},
		function gettingTransactions (index, callback) {
			let params = APP.queries.storePro('get_transaction', {
				body: {
					merchant_code: req.body.merchant_id,
					limit: req.body.limit,
					start_date: req.body.start_date,
					end_date: req.body.end_date,
					offset: req.body.offset,
					order: req.body.order,
					transactions_type_code: req.body.type,
					status: req.body.status,
					search: req.body.search
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction.length < 1) return callback(null, {
						code: 'NOT_FOUND',
						data: [],
						info: {
							function: "getTransaction"
						}
					});

				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: 'NOT_FOUND',
						message: transaction[0].message,
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							function: "getTransaction"
						}
					});

				callback(null, transaction);
			}).catch(err => {
				console.log(err)
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getTransaction"
					}
				});
			});
		},
		function gettingTransactionCount (transaction, callback) {
			let params = APP.queries.storePro('get_transaction_count', {
				body: {
					merchant_code: req.body.merchant_id,
					start_date: req.body.start_date,
					end_date: req.body.end_date,
					transactions_type_code: req.body.type,
					search: req.body.search
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transactionCount => {
				if (transactionCount[0].total == 0) {
					callback({
						code: '01',
						message: transactionCount[0].message,
						data: {
							rows: [],
							count: 0,
							total_amount: 0
						}
					});
				} else {
					callback(null, transaction, transactionCount[0]);
				}

				// if (transactionCount[0].code && transactionCount[0].code !== '00') return callback(null, {
				// 		code: 'NOT_FOUND',
				// 		message: transactionCount[0].message,
				// 		data: {
				// 			q_code: transactionCount[0].code,
				// 			q_message: transactionCount[0].message
				// 		},
				// 		info: {
				// 			function: "getTransaction"
				// 		}
				// 	});
			})
		},
		
		function output (transactions, transactionCount, callback) {
			if(transactionCount.total == 0){
				callback(null, {
					code: 'DATA_NOT_FOUND_CODE_01',
					data: {
						rows: transactions || transactions.data,
						count: transactionCount.total
					}
				});
			}else{
				callback(null, {
					code: 'PRODUCT_FOUND',
					data: {
						rows: transactions || transactions.data,
						count: transactionCount.total
					}
				});
			}
			// callback(null, {
			// 	code: transactions.code || 'MERCHANT_TRANSACTION_FOUND',
			// 	data: {
			// 		rows: transactions.data || transactions,
			// 		count: transactionCount.total
			// 	}
			// });
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};