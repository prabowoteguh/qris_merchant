"use strict";

const async  = require('async');
const moment = require('moment');
const checkMerchant = require('../controllers/checkMerchant.js');

/**
* @type: service
* @req: merchant_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	moment.locale('id');
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						service: "getPromo"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_code);
			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, true);
		},
		function checkingMerchant (index, callback) {
			checkMerchant(APP, {
				body: {
					merchant_id: req.body.merchant_code
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') {
					return callback({
						code: 'PROMO_MERCHANT_NOT_FOUND',
						data: {},
						info: {
							service: 'getPromo'
						}
					});
				}else{
					callback(null, result.data);
				}
			});
		},
		function output (merchant, callback) {
			return callback(null, {
				code: 'PROMO_MERCHANT_FOUND',
				data: {
					promo_id: merchant.promo_id,
					promo_name: merchant.promo_name,
					promo_description: merchant.promo_description,
					start_promo : moment(merchant.start_promo).format('DD MMM YYYY'),
					end_promo :  moment(merchant.end_promo).format('DD MMM YYYY'),
					start_date : merchant.start_promo,
					end_date : merchant.end_promo,
					image_promo : merchant.image_promo,
					max_transaction: merchant.max_transaction,
					min_nominal: merchant.min_nominal,
					point_reward: merchant.point_reward
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);
		callback(null, result);
	});
};