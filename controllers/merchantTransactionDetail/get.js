"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.select('merchant_transaction_detail', req, APP.models);

	APP.models.mysql.merchantTransactionDetail.findAll(params).then((rows) => {
		return callback(null, {
			code: (rows && (rows.length > 0)) ? 'FOUND' : 'NOT_FOUND',
			data: rows,
			info: {
				dataCount: rows.length
			}
		});
	}).catch((err) => {
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};