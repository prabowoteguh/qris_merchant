"use strict";

const async = require('async');
const addMembership = require('../controllers/membership/add.js');
/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "revokeMembership"
					}
				});

			if (!req.body.emoney_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "emoney_va"
					},
					info: {
						function: "revokeMembership"
					}
				});
		
			callback(null, {});
		},

		function add (data, callback) {
			addMembership(APP, {
				body: {
					merchant_id	: req.body.merchant_id,
					emoney_va 	: req.body.emoney_va,
					name 		: req.body.name,
					email 		: req.body.email
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'ADD_MEMBERSHIP_SUCCESS') return callback(result);

				callback(null, data);
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'ADD_MEMBERSHIP_SUCCESS',
				data: {
					emoney_va 	: req.body.emoney_va,
					name 		: req.body.name,
					email 		: req.body.email
				}
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};