"use strict";

const async = require('async');

/**
* @type: service
* @req: id, merchant_id, name
* @return callback object
*/
module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "id"
					},
					info: {
						function: "updateProductType"
					}
				});

			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						function: "updateProductType"
					}
				});

			if (!req.body.name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "name"
					},
					info: {
						function: "updateProductType"
					}
				});

			req.body.merchant_code = req.body.merchant_code.toUpperCase();

			callback(null, true);
		},
		function updatingProductTypes (index, callback) {
			const params = APP.queries.storePro('update_product_type', {
				body: {
					id: req.body.id,
					merchant_code: req.body.merchant_code,
					name: req.body.name
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(product => {
				if (product.length < 1) return callback(null, {
						code: 'UPDATE_PRODUCT_TYPE_FAILED',
						data: req.body,
						info: {
							function: "updateProductType"
						}
					});

				if (product[0].code && product[0].code !== '00') return callback({
						code: product[0].code,
						message: product[0].message,
						data: req.body,
						info: {
							function: "updateProductType"
						}
					});

				callback(null, {
					code: 'UPDATE_PRODUCT_TYPE_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "updateProductType"
					}
				});
			});
		},
		function output (product, callback) {
			callback(null, product);
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};