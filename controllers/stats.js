"use strict";

const async = require('async');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function gettingData (data, callback) {
			let params = APP.queries.storePro('merchant_stats', {
				body: {}
			});

			APP.db.sequelize.query(params.query, params.bind).then(stats => {
				if (stats.length < 1) return callback({
						code: 'MERCHANT_STATS_NOT_FOUND',
						data: [],
						info: {
							function: "merchantStats"
						}
					});

				if (stats[0].code && stats[0].code !== '00') return callback({
						code: 'MERCHANT_STATS_NOT_FOUND',
						message: stats[0].message,
						data: {
							q_code: stats[0].code,
							q_message: stats[0].message
						},
						info: {
							function: "merchantStats"
						}
					});

				data.data = stats[0];

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "merchantStats"
					}
				});
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'MERCHANT_STATS_SUCCESS',
				data: data.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};