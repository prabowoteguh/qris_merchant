"use strict";

const async = require('async');
const checkHp = require('../controllers/checkHp.js');
const checkUserMerchant = require('../controllers/checkUserMerchant.js');
const userMerchantCreate = require('../controllers/userMerchant/insert.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_reff_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_reff_id"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.type) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "type"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.username) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "username"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "name"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.msisdn) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "msisdn"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.password) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "password"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			if (!req.body.menu) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "menu"
					},
					info: {
						function: "createUserMerchant"
					}
				});

			var validateName = APP.validation.name(req.body.name);
			if (validateName !== true) return callback(validateName);

			var validateUsername = APP.validation.username(req.body.username);
			if (validateUsername !== true) return callback(validateUsername);

			var validatePhone = APP.validation.phoneNumber(req.body.msisdn);
			if (validatePhone !== true) return callback(validatePhone);

			var validatePassword = APP.validation.password(req.body.password);
			if (validatePassword !== true) return callback(validatePassword);
		
			req.body.username = req.body.username.toUpperCase();
			callback(null, {});
		},
		function checkingMsisdn (data, callback) {
			checkHp(APP, {
				body: {
					merchant_hp: req.body.msisdn
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback(null, data);

					return callback(err);
				}

				if (result && result.code !== 'FOUND') return callback(null, data);

				callback({
					code: 'USER_MERCHANT_ALREADY_EXIST',
					data: req.body
				});
			});
		},
		function checkingUserMerchant (data, callback) {
			checkUserMerchant(APP, {
				body: {
					username: req.body.username
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback(null, data);

					return callback(err);
				}

				if (result && result.code !== 'FOUND') return callback(null, data);

				callback({
					code: 'USER_MERCHANT_ALREADY_EXIST',
					data: req.body
				});
			});
		},
		function insertingUserMerchant (data, callback) {
			userMerchantCreate(APP, {
				body: {
					merchant_id: req.body.merchant_reff_id,
					type: req.body.type,
					username: req.body.username,
					name: req.body.name,
					msisdn: req.body.msisdn,
					password: APP.generator.generatePassword(req.body.password).encrypted,
					menu: req.body.menu,
					active_status: '1',
					image: req.body.image,
					action_by: req.body.merchant_reff_id,
					ip_address: req.body.ip_address
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'USER_MERCHANT_INSERT_SUCCESS') return callback(result);

				callback(null, data);
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'USER_MERCHANT_INSERT_SUCCESS',
				data: req.body
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};