	"use strict";

const async = require('async');
const userMerchantGet = require('../controllers/userMerchant/get.js');

/**
* @type: function
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_hp) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_hp"
					},
					info: {
						function: "checkHp"
					}
				});

			var validateMerchantHp = APP.validation.true(req.body.merchant_hp);

			if (validateMerchantHp !== true) return callback(validateMerchantHp);

			callback(null, {});
		},
		function gettingMerchantByHp (data, callback) {
			userMerchantGet(APP, {
				body: {
					msisdn: req.body.merchant_hp
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback(result);

				callback(null, result);
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data[0]
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};