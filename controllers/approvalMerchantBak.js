"use strict";

const async = require('async');
const checkMerchant = require('../controllers/checkMerchant.js');
const merchantApproval = require('../controllers/merchant/approval.js');
const updatePasswordApproval = require('../controllers/userMerchant/updatePasswordApproval.js');
const emoneyServices = require('../config/emoneyServices.json');
const checkHp = require('../controllers/checkHp.js');
/**
* @type: service
* @req: merchant_id, status
* @return callback object
*/

module.exports = function (APP, req, callback) {
	var merchantVA = '';
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						service: "approvalMerchant"
					}
				});

			if (!req.body.action_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "action_by"
					},
					info: {
						service: "approvalMerchant"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_code);

			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, true);
		},
		function checkingMerchant (index, callback) {
			APP.models.mysql.merchant.findOne({
				where : {
					code : req.body.merchant_code,
					status: 0
				}
			}).then((rows) => {
				if(rows){
					return callback(null, rows)
				}else{
					return callback({
							code: 'MERCHANT_APPROVAL_FAILED',
							data: req.body,
							info: {
								service: 'approvalMerchant'
							}
						});
				}
			}).catch((err) => {
				console.log(err)
				return callback({
					code: 'ERR_DATABASE',
					data: JSON.stringify(err)
				});
			});
		},
		function generatePassword (merchant, callback) {
			callback(null, merchant, APP.generator.generatePassword());
		},
		function generateMerchantVa (merchant, password, callback) {
			merchantVA = APP.generator.generateVA(merchant.msisdn);
			callback(null, merchant,password);
		},
		function updatingMerchant (merchant, password, callback) {
			console.log(password.encrypted)
			console.log(password.password)
			merchantApproval(APP, {
				body: {
					dataQuery: {
						merchant_id: merchant.id,
						username: merchant.code,
						code	: req.body.merchant_code,
						status  : 0
					},
					dataUpdate: {
						status: 1,
						password: password.encrypted,
						approval_by: req.body.action_by,
						approval_at: APP.db.Sequelize.fn('NOW'),
						account_number : merchantVA,
						bank_account_number: req.body.bank_account_number,
						bank_account_name: req.body.bank_account_name,
						image_nik: req.body.image_nik,
						image_npwp: req.body.image_npwp,
						image_merchant: req.body.image_merchant,
						image_pic: req.body.image_pic,
						ktp: req.body.ktp,
						npwp: req.body.npwp,
						omzet: req.body.omzet,
						nmid: req.body.nmid,
						merchant_category_id: req.body.merchant_category_id,
						merchant_criteria_id: req.body.merchant_criteria_id
					}
				}
			}, (err, result) => {
				console.log(err)
				if (err) return callback(err);
				if (result && (result.code === 'ER_EMPTY_QUERY' || result.code === 'ER_DUP_ENTRY' || result.code === 'ERR_DATABASE')) return callback(result);
				callback(null, merchant, password, result);
				
			});
		},
		
		function checkingHpInEmoneyCore (merchant,password, queryTransaction, callback) {
			APP.request.emoney(emoneyServices.checkHp, {
				phone_num: merchant.msisdn
			}, (err, result) => {
				if (err) {
					queryTransaction.rollback(); 
					if (err.code !== 'ERR_DATABASE') return callback(null, merchant);
					callback(err);
				};

				if (result && result.code === '00') {
					if (result.data.length > 0) {
						queryTransaction.rollback(); 
						return callback({
							code: 'HP_UNAVAILABLE',
							data: req.body,
							info: {
								service: 'registration'
							}
						});
					}
				}

				callback(null, merchant, password, queryTransaction,);
			});
		},
		function insertMerchantVAInEmoneyCore (merchant, password, queryTransaction,callback) {
			APP.request.emoney(emoneyServices.register, {
				phone_num: merchant.msisdn,
				first_name: merchant.name,
				email: merchant.email,
				type: "2",
				merchant_va: merchantVA
			}, (err, result) => {
				if (err){
					queryTransaction.rollback(); 
					return callback(err);	
				} 

				if (result && result.code !== '00') {
					queryTransaction.rollback(); 
					return callback(result);

				}
				callback(null, merchant, password, queryTransaction, result.data);
			});
		},
		// function generatePassword (merchant, queryTransaction, va, callback) {
		// 	callback(null, merchant, queryTransaction, va, APP.generator.generatePassword());
		// },
		// function update (merchant, queryTransaction, va, password, callback) {
		// 	updatePasswordApproval(APP, queryTransaction, {
		// 		body: {
		// 			dataQuery: {
		// 				merchant_id: merchant.id,
		// 				username: merchant.code,
		// 			},
		// 			dataUpdate: {
		// 				password: password.encrypted,
		// 				status : 1
		// 			}
		// 		}
		// 	}, (err, result) => {
		// 		if (err){
		// 			return callback(err);	
		// 		} 

		// 		if (result && result.code !== 'USER_MERCHANT_UPDATE_SUCCESS') {
		// 			return callback(result);
		// 		}
		// 		callback(null, merchant, queryTransaction, va, password);
		// 	});
		// },
		function output (merchant, password, queryTransaction, va, callback) {
			// console.log(merchant)
			APP.models.mysql.businessType.findOne({
				where : {
					id : merchant.business_type_id,
				}
			}).then((rows) => {
				queryTransaction.commit();
				// merchant = merchant.toJSON();
				merchant.dataValues.username = merchant.code;
				merchant.dataValues.password = password.password;
				merchant.dataValues.pin = va.pin;
				merchant.dataValues.account_number = merchantVA;
				merchant.dataValues.account_name = merchant.name;
				merchant.dataValues.merchant_jenis_usaha = rows.name;
				// console.log(merchant);
				callback(null, {
					code: 'MERCHANT_APPROVAL_SUCCESS',
					data: merchant
				});	
			}).catch((err) => {
				queryTransaction.commit();
				// merchant = merchant.toJSON();
				merchant.dataValues.username = merchant.code;
				merchant.dataValues.password = password.password;
				merchant.dataValues.pin = va.pin;
				merchant.dataValues.account_number = merchantVA;
				merchant.dataValues.account_name = merchant.name;
				merchant.dataValues.merchant_jenis_usaha = '';
				callback(null, {
					code: 'MERCHANT_APPROVAL_SUCCESS',
					data: merchant
				});
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};