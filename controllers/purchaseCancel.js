"use strict";

const async = require('async');
const purchaseInvoiceInquiry = require('../controllers/purchaseInvoiceInquiry.js');
const merchantTransactionUpdate = require('../controllers/merchantTransaction/update.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.invoice) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "invoice"
					},
					info: {
						service: "purchaseCancel"
					}
				});

			var validateInvoice = APP.validation.true(req.body.invoice);

			if (validateInvoice !== true) return callback(validateInvoice);

			callback(null, {});
		},
		function checkingInvoiceExists (data, callback) {
			purchaseInvoiceInquiry(APP, {
				body: {
					invoice: req.body.invoice
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'PURCHASE_INVOICE_INQUIRY_SUCCESS') return callback(result);

				data.transaction = result.data;

				callback(null, data);
			});
		},
		function updatingTransaction (data, callback) {
			merchantTransactionUpdate(APP, {
				body: {
					dataQuery: {
						id: data.transaction.transaction_id,
						status: 0,
					},
					dataUpdate: {
						status: '2',
						note_2: req.body.narration
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'MERCHANT_TRANSACTION_UPDATE_SUCCESS') return callback(result);

				callback(null, data);
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'CANCEL_TRANSACTION_SUCCESS',
				data: req.body
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};