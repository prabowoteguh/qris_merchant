"use strict";

const async = require('async');
const checkVa = require('../controllers/checkVa.js');
const emoneyServices = require('../config/emoneyServices.json');
const checkMerchant = require('../controllers/checkMerchant.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	let channel_name = '';
	let journal = APP.generator.generateJournal()
	async.waterfall([
		function validation (callback) {
			if (!req.body.refference_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "refference_id"
					},
					info: {
						service: "withdraw"
					}
				});

			if (!req.body.channel_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "channel_id"
					},
					info: {
						service: "withdraw"
					}
				});

			if (!req.body.channel_name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "channel_name"
					},
					info: {
						service: "withdraw"
					}
				});

			if (!req.body.created_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "created_by"
					},
					info: {
						service: "withdraw"
					}
				});

			var validateRefferenceId = APP.validation.true(req.body.refference_id);
			var validateChannelId = APP.validation.true(req.body.channel_id);
			var validateChannelName = APP.validation.true(req.body.channel_name);
			var validateAmount = APP.validation.amount(req.body.amount);
			

			if (validateRefferenceId !== true) return callback(validateRefferenceId);
			if (validateChannelId !== true) return callback(validateChannelId);
			if (validateChannelName !== true) return callback(validateChannelName);
			if (validateAmount !== true) return callback(validateAmount);

			callback(null, {});
		},
		function bundlingMerchantVA (data, callback) {
			data.merchant_va = req.body.emoney_va;

			callback(null, data);
		},
		function checkingMerchant (data, callback) {
			if (req.body.channel_id.toLowerCase() === 'merchant') {
				checkVa(APP, {
					body: {
						merchant_va: data.merchant_va
					}
				}, (err, result) => {
					if (err) {
						if (err.code === 'NOT_FOUND') return callback({
								code: 'MERCHANT_DOESNT_EXISTS',
								data: req.body,
								info: {
									service: 'withdraw'
								}
							});

						return callback(err);
					}

					if (result && result.code !== 'FOUND') return callback(result);

					if (result && result.data.code.toLowerCase() !== req.body.channel_name.toLowerCase()) return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: req.body,
							info: {
								service: 'withdraw'
							}
						});

					channel_name = result.data.code;
					data.username = result.data.code;

					callback(null, data);
				});
			} else {
				channel_name = req.body.channel_name;

				callback(null, data);
			}
		},
		function checkMerchantPassword (data, callback) {
			checkMerchant(APP, {
				body: {
					username: req.body.username,
					password: req.body.password,
					type: req.body.type,
					login: true,
					check: true
				}
			}, (err, result) => {
				if (err) {
					console.log(err)
					if (err.code === 'NOT_FOUND') return callback({
							code: 'PASSWORD_MERCHANT_FALSE',
							data: {}
						});

					return callback(err);
				}
				if (result && result.code !== 'FOUND') return callback({
						code: 'PASSWORD_MERCHANT_FALSE',
						data: {}
					});

				callback(null, data, result);
			});
		},
		function emoneyWithdraw (data, merchant, callback) {
			req.body.terminal_id = channel_name;
			req.body.type = 'MERCHANT';

			APP.request.emoney(emoneyServices.withdraw, req.body, (err, result) => {

				if (err) {
					err.code = 'PIN_FALSE';
					return callback(err);
				};

				if (result && result.code !== '00') return callback(result);

				data.withdraw = result.data;

				return callback(null, data, merchant);
			});
		},
		function sendAccountBank (data, merchant, callback) {
			// {
			// 	"IWSID":"BTNPAY",					// ID Service : "BTNPAY"
			// 	"IWSREF":"123456789012",            // No referensi (Unique per Transaksi)
			// 	"IAC2":"1401500597578",				// Nomer Rekening
			// 	"IAM1":"50000",						// Amount
			// 	"IHP":"08888489242"					// Nomer HP
			// }

			// Response
			// {
			//     "OWSID": "BTNPAY",					// ID Service : "BTNPAY"	
			//     "OWSREF": "123456789012",           // No referensi (Unique per Transaksi)
			//     "ORSP": "000",						// Response Code : "000" adalah sukses
			//     "ORSPDC": "Transaction Success."	// Response Description
			// }
			return callback(null, data);
			
			APP.request.withdraw({
				// IWSID: 'BTNPAY',
				// IWSREF: journal,
				// IAC2: merchant.data.bank_account_number,
				// IAM1: req.body.amount,
				// IHP: merchant.data.merchant_hp
				id: process.env.URL_ID_BTN,
				ref: journal,
				account: merchant.data.bank_account_number,
				amount: req.body.amount,
				hp: merchant.data.merchant_hp,
				merchant: data.username
			}, (err, result) => {
				if (err) {
					console.log(err)
					APP.request.emoney(emoneyServices.reversal, {
						emoney_va: req.body.emoney_va,
						amount: req.body.amount,
						narration: 'Reversal Withdraw Merchant ( '+req.body.refference_id+' )'
					}, (err, result) => {
						return callback({
							code: 'MERCHANT_WITHDRAW_FAILED',
							data: err,
							info: {
								service: 'withdraw'
							}
						});
					});
					
				} else if (result && result.rsp !== '000') {
					APP.request.emoney(emoneyServices.reversal, {
						emoney_va: req.body.emoney_va,
						amount: req.body.amount,
						narration: 'Reversal Withdraw Merchant ( '+req.body.refference_id+' )'
					}, (err, result) => {
						return callback({
							code: 'MERCHANT_WITHDRAW_FAILED',
							data: result,
							info: {
								service: 'withdraw'
							}
						});
					});
				}else{
					return callback(null, data);
				}

				
			});
			//return callback(null, data);
		},
		
		function insertMerchantWithdraw (data, callback) {
			const params = APP.queries.storePro('insert_withdraw', {
				body: {
					requestId: req.body.refference_id,
					account_number: channel_name,
					merchant_va: data.merchant_va,
					wallet_type_code: 'SOF01',
					transactions_type_code: 'TRT013',
					biller_type: 'BTN',
					biller_code: req.body.account_num,
					biller_name: 'BTN',
					nominal: data.withdraw.amount,
					no_jurnal: journal,
					note: req.body.narration,
					fee: '0',
					admin_fee: '0',
					status: '1',
					action_by: req.body.created_by,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: 'MERCHANT_WITHDRAW_FAILED',
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							service: 'withdraw'
						}
					});

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'MERCHANT_WITHDRAW_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'withdraw'
					}
				});
			});
		},
		function output (data, callback) {
			data.created_by = req.body.created_by;
			
			callback(null, {
				code: 'MERCHANT_WITHDRAW_SUCCESS',
				data: data.withdraw
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};