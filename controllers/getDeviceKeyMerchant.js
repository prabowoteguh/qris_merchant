"use strict";

const async = require('async');
const deviceKey = require('../controllers/merchant/deviceKey.js');

/**
* @type: service
* @req: limit, offset
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, true);
		},
		function validation (index, callback) {
			if(req.body.merchant_code){
				APP.models.mysql.merchant.findOne({
					where : {
						code : req.body.merchant_code
					}
				}).then((rows) => {
					if(rows){
						req.body.merchant_id = rows.id
						callback(null, true);
					}else{
						return callback({
							code: 'NOT_FOUND',
							data: {}
						});
					}
				}).catch((err) => {
					console.log(err)
					return callback({
						code: 'ERR_DATABASE',
						data: JSON.stringify(err)
					});
				});
			}
		},
		function gettingList (index, callback) {
			deviceKey(APP, {
				body: {
					limit			: req.body.limit,
					offset			: req.body.offset,
					username		: req.body.username,
					id				: req.body.id,
					merchant_id		: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) return callback(err);
				callback(null, result);
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};