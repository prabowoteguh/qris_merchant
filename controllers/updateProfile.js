"use strict";

const async = require('async');
const checkMerchant = require('../controllers/checkMerchant.js');
const email = require('../controllers/mail/merchant')

/**
* @type: service
* @req: merchant_nama, merchant_jenis_usaha, merchant_nama_pic, merchant_hp,
*       email, telepon, alamat, kelurahan, kecamatan, kota, provinsi,
*       merchant_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						service: "updateProfile"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_code);

			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, true);
		},
		function updatingMerchant (merchant, callback) {
			const params = APP.queries.storePro('updateProfile', {
				body: {
					code: req.body.merchant_code,
					name: req.body.merchant_nama,
					business_type_id: req.body.merchant_jenis_usaha,
					owner: req.body.merchant_nama_pic,
					msisdn: req.body.merchant_hp,
					email: req.body.email,
					phone: req.body.telepon,
					address: req.body.alamat,
					kelurahan: req.body.kelurahan,
					kecamatan: req.body.kecamatan,
					city: req.body.kota,
					province: req.body.provinsi,
					action_by: req.body.action_by,
					ip_address: req.body.ip_address,
					image: req.body.image,
					banner: req.body.banner,
					open_hours: req.body.open_hours,
					closed_hours: req.body.closed_hours,
					description: req.body.description,
					merchant_category_id: req.body.merchant_category_id,
					merchant_criteria_id: req.body.merchant_criteria_id,
					nmid: req.body.nmid,
					status: req.body.status,
					bank_acc_num: req.body.bank_acc_num,
					bank_acc_name: req.body.bank_acc_name,
					image_nik: req.body.image_nik,
					image_npwp: req.body.image_npwp,
					image_pic: req.body.image_pic,
					image_merchant: req.body.image_merchant,
					ktp: req.body.ktp,
					npwp: req.body.npwp,
					omzet: req.body.omzet,
					approval_by: req.body.approval_by
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(account => {
				if (account[0].code && account[0].code !== '00') return callback({
						code: account[0].code,
						message: account[0].message,
						data: {
							q_code: account[0].code,
							q_message: account[0].message
						},
						info: {
							service: 'updateProfile'
						}
					});

				callback(null, merchant, account);
			}).catch(err => {
				console.log(err);
				callback({
					code: 'MERCHANT_UPDATE_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'updateProfile'
					}
				});
			});
		},
		function sendMail (merchant, account, callback) {
			if (Number(req.body.status) == 2) {
				let payload = {
					body: {
						email: req.body.email,
						subject: '[BTN-MERCHANT] Registrasi Merchant BTN Gagal',
						merchant_nama_pic: req.body.merchant_nama_pic
					}
				}
	
				email.send(req, payload, (err, result) => { console.log(result) })
				callback(null, merchant, account);
			} else {
				callback(null, merchant, account);
			}
		},
		function output (merchant, account, callback) {
			callback(null, {
				code: 'MERCHANT_UPDATE_SUCCESS',
				data: req.body
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};
