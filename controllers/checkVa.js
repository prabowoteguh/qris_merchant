	"use strict";

const async = require('async');
const merchantGet = require('../controllers/merchant/get.js');

/**
* @type: function
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_va"
					},
					info: {
						function: "checkVa"
					}
				});

			var validateMerchantVa = APP.validation.va(req.body.merchant_va);

			if (validateMerchantVa !== true) return callback(validateMerchantVa);

			callback(null, {});
		},
		function gettingMerchantByHp (data, callback) {
			merchantGet(APP, {
				body: {
					account_number: req.body.merchant_va
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback(result);

				callback(null, result);
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data[0]
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};