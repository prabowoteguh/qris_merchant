"use strict";

const async = require('async');
const md5 = require('md5');
const serviceList = require('../config/services.json');
var output = {};

exports.services = function (APP, req, callback) {
	callback(null, {
		code: 'OK',
		data: req.body.service ? serviceList[req.body.service] : serviceList
	});
};