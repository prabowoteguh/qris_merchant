"use strict";

const async = require('async');

/**
* @type: service
* @req: limit, offset, merchant_id, product_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "getProduct"
					}
				});

			req.body.merchant_id = req.body.merchant_id.toUpperCase();

			callback(null, {});
		},
		function gettingProducts (data, callback) {
			const params = APP.queries.storePro('get_product', {
				body: {
					merchant_code: req.body.merchant_id,
					product_id: req.body.product_id,
					product_type: req.body.product_type,
					search : req.body.search,
					limit: req.body.limit,
					offset: req.body.offset,
					order: req.body.order,
					status: req.body.status,
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(product => {
				if (product.length < 1) return callback(null, {
						code: 'PRODUCT_NOT_FOUND',
						data: [],
						info: {
							function: "getProduct"
						}
					});

				if (product[0].code && product[0].code !== '00') return callback({
						code: 'PRODUCT_NOT_FOUND',
						message: product[0].message,
						data: {
							q_code: product[0].code,
							q_message: product[0].message
						},
						info: {
							function: "getProduct"
						}
					});

				data.product = product;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getProduct"
					}
				});
			});
		},
		function countingData (data, callback) {
			const params = APP.queries.storePro('get_product_count', {
				body: {
					merchant_code: req.body.merchant_id,
					product_id: req.body.product_id,
					product_type: req.body.product_type,
					search : req.body.search,
					status: req.body.status
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(productTotal => {
				if (productTotal.length < 1) return callback(null, {
						code: 'PRODUCT_NOT_FOUND',
						data: [],
						info: {
							function: "getProduct"
						}
					});

				if (productTotal[0].code && productTotal[0].code !== '00') return callback({
						code: 'PRODUCT_NOT_FOUND',
						message: productTotal[0].message,
						data: {
							q_code: productTotal[0].code,
							q_message: productTotal[0].message
						},
						info: {
							function: "getProduct"
						}
					});

				data.productTotal = productTotal[0].total;

				callback(null, data);
			}).catch(err => {console.log(err);
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getProduct"
					}
				});
			});
		},
		function output (data, callback) {
			if(data.productTotal == 0){
				callback(null, {
					code: 'DATA_NOT_FOUND_CODE_01',
					data: {
						image_url: process.env.URL_CDN_IMAGE,
						rows: data.product || [],
						count: data.productTotal
					}
				});
			}else{
				callback(null, {
					code: 'PRODUCT_FOUND',
					data: {
						image_url: process.env.URL_CDN_IMAGE,
						rows: data.product || [],
						count: data.productTotal
					}
				});
			}
			
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};