"use strict";

const async = require('async');

/**
* @type: service
* @req: id, merchant_id, name, price, narration, image, type, qty,
* promo_status, active_status
* @return callback object
*/
module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "id"
					},
					info: {
						function: "updateProduct"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "updateProduct"
					}
				});

			req.body.merchant_id = req.body.merchant_id.toUpperCase();
			
			var validatePrice = APP.validation.amount(req.body.price);
			if (validatePrice !== true) return callback(validatePrice);

			callback(null, true);
		},
		function updatingProducts (index, callback) {
			const params = APP.queries.storePro('update_product', {
				body: {
					id: req.body.id,
					merchant_code: req.body.merchant_id,
					product_type_id: req.body.type,
					name: req.body.name,
					narration: req.body.narration,
					price: req.body.price,
					quantity: req.body.qty,
					promo_status: req.body.promo_status,
					active_status: req.body.active_status,
					image: req.body.image,
					recommended: req.body.recommended
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(product => {
				if (product.length < 1) return callback(null, {
						code: 'UPDATE_PRODUCT_FAILED',
						data: req.body,
						info: {
							function: "updateProduct"
						}
					});

				if (product[0].code && product[0].code !== '00') return callback({
						code: product[0].code,
						message: product[0].message,
						data: req.body,
						info: {
							function: "updateProduct"
						}
					});

				callback(null, {
					code: 'UPDATE_PRODUCT_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "updateProduct"
					}
				});
			});
		},
		function output (product, callback) {
			callback(null, product);
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};