	"use strict";

const async = require('async');
const checkMerchantById = require('../checkMerchantById.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.setting_promo_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "setting_promo_id"
					},
					info: {
						function: "approvePromo"
					}
				});

			if (!req.body.status) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "status"
					},
					info: {
						function: "approvePromo"
					}
				});

			if (!req.body.updated_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "updated_by"
					},
					info: {
						function: "approvePromo"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "approvePromo"
					}
				});

			var validateSettingPromoId = APP.validation.true(req.body.setting_promo_id);

			if (validateSettingPromoId !== true) return callback(validateSettingPromoId);

			callback(null, {});
		},
		function checkingMerchantExists (data, callback) {
			checkMerchantById(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) callback(err);

				if (result && result.code !== 'FOUND') return callback(result);

				callback(null, data);
			});
		},
		function prefixs (data, callback) {
			if (req.body.status === '2') {
				data.prefix = 'REJECT';
			} else {
				data.prefix = 'APPROVE';
			}

			callback(null, data);
		},
		function approvingapprovePromo (data, callback) {
			var params = APP.queries.storePro('update_setting_promo', {
				body: {
					setting_promo_id: req.body.setting_promo_id,
					merchant_id: req.body.merchant_id,
					status: req.body.status,
					approved_by: req.body.updated_by,
					narration: req.body.narration,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(reply => {
				if (reply[0].code && reply[0].code !== '00') {
					if (reply[0].code === 'DBB15') return callback({
							code: reply[0].code,
							data: req.body,
							message: reply[0].message,
							info: {
								service: "approvePromo"
							}
						});
					
					return callback({
						code: 'ERR_' + data.prefix + '_PROMO',
						data: req.body,
						info: {
							db: reply[0],
							service: "approvePromo"
						}
					});
				}

				callback(null, {
					code: 'PROMO_' + data.prefix + '_SUCCESS',
					data: req.body
				});
			}).catch(err => {console.log(err)
				callback({
					code: 'ERR_' + data.prefix + '_PROMO',
					data: req.body,
					info: {
						err: JSON.stringify(err),
						service: "approvePromo"
					}
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: req.body
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};