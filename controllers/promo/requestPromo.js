	"use strict";

const async = require('async');
const checkMerchantById = require('../checkMerchantById.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "requestPromo"
					}
				});

			if (!req.body.promo_name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "promo_name"
					},
					info: {
						function: "requestPromo"
					}
				});

			if (!req.body.point_reward) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "point_reward"
					},
					info: {
						function: "requestPromo"
					}
				});

			if (!req.body.start_promo) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "start_promo"
					},
					info: {
						function: "requestPromo"
					}
				});

			if (!req.body.end_promo) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "end_promo"
					},
					info: {
						function: "requestPromo"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_id);

			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, {});
		},
		function checkingMerchantExists (data, callback) {
			checkMerchantById(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				console.log(err)
				if (err){
					return callback(err);
				}else if (result && result.code !== 'FOUND') {
					return callback(result);
				}else{
					return callback(null, data);			
				}
			});
		},
		function insertingrequestPromo (data, callback) {
			var params = APP.queries.storePro('create_setting_promo', {
				body: {
					merchant_id: req.body.merchant_id,
					promo_name: req.body.promo_name,
					description: req.body.description,
					min_nominal: req.body.min_nominal,
					point_reward: req.body.point_reward,
					max_transaction: req.body.max_transaction,
					start_promo: req.body.start_promo,
					end_promo: req.body.end_promo,
					image_promo: req.body.image_promo,
					action_by: req.body.created_by,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(reply => {
				if (reply[0].code && reply[0].code !== '00') {
					if (reply[0].code === 'DBB14') return callback({
							code: 'PENDING_PROMO_EXISTS',
							data: req.body,
							info: {
								service: "requestPromo"
							}
						});

					return callback({
						code: reply[0].code,
						message: reply[0].message,
						data: reply[0],
						info: {
							db: reply[0],
							service: "requestPromo"
						}
					});
				}

				callback(null, {
					code: 'PROMO_REQUEST_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'ERR_CREATE_PROMO',
					data: req.body,
					info: {
						err: JSON.stringify(err),
						service: "requestPromo"
					}
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};