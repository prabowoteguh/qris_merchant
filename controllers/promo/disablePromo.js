	"use strict";

const async = require('async');
const checkMerchantById = require('../checkMerchantById.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.setting_promo_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "setting_promo_id"
					},
					info: {
						function: "approvePromo"
					}
				});

			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "approvePromo"
					}
				});

			var validateSettingPromoId = APP.validation.true(req.body.setting_promo_id);

			if (validateSettingPromoId !== true) return callback(validateSettingPromoId);

			callback(null, {});
		},
		function checkingMerchantExists (data, callback) {
			checkMerchantById(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) callback(err);

				if (result && result.code !== 'FOUND') return callback(result);

				callback(null, data);
			});
		},
		function disablePromo (data, callback) {
			var params = APP.queries.storePro('disable_promo', {
				body: {
					setting_promo_id: req.body.setting_promo_id,
					merchant_id: req.body.merchant_id
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(reply => {
				callback(null, {
					code: 'PROMO_DISABLE_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'ERR_DISABLE_PROMO',
					data: req.body,
					info: {
						err: JSON.stringify(err),
						service: "requestPromo"
					}
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};