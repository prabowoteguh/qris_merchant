	"use strict";

const async = require('async');
const checkMerchantById = require('../checkMerchantById.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function checkingMerchantExists (data, callback) {
			if (req.body.merchant_id && req.body.merchant_id != "0") {
				checkMerchantById(APP, {
					body: {
						merchant_id: req.body.merchant_id
					}
				}, (err, result) => {
					if (err) callback(err);

					if (result && result.code !== 'FOUND') return callback(result);

					callback(null, data);
				});
			} else {
				callback(null, data);
			}
		},
		function gettingPromo (data, callback) {
			var params = APP.queries.storePro('get_setting_promo', {
				body: {
					status: req.body.status,
					setting_promo_id: req.body.setting_promo_id,
					merchant_id: req.body.merchant_id,
					start_date: req.body.start_date,
					end_date: req.body.end_date,
					offset: req.body.offset,
					limit: req.body.limit,
					order: req.body.order,
					search: req.body.search
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(rows => {
				if (rows.length < 1) return callback({
						code: 'PROMO_NOT_FOUND',
						data: {
							rows: [],
							count: '0'
						},
						info: {
							function: "getPromo"
						}
					});

				data.budgets = rows;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'ERR_GET_PROMO',
					data: req.body,
					info: {
						err: JSON.stringify(err),
						service: "getPromo"
					}
				});
			});
		},
		function gettingPromoCount (data, callback) {
			var params = APP.queries.storePro('get_setting_promo_count', {
				body: {
					status: req.body.status,
					setting_promo_id: req.body.setting_promo_id,
					merchant_id: req.body.merchant_id,
					start_date: req.body.start_date,
					end_date: req.body.end_date,
					search: req.body.search
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(count => {
				if (count[0].code && count[0].code !== '00') return callback({
						code: 'PROMO_NOT_FOUND',
						data: {
							rows: [],
							count: 0
						},
						info: {
							service: "getPromo"
						}
					});

				data.budgetsCount = count[0].total;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'ERR_GET_PROMO',
					data: req.body,
					info: {
						err: JSON.stringify(err),
						service: "getPromo"
					}
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: 'PROMO_GET_SUCCESS',
				data: {
					rows: output.budgets,
					count: output.budgetsCount || output.budgets.length
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};
