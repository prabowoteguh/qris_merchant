"use strict";

const async = require('async');
const checkMerchant = require('../controllers/checkMerchant.js');

/**
* @type: service
* @req: merchant_id
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						service: "getProfile"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_id);

			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, true);
		},
		function checkingMerchant (index, callback) {
			checkMerchant(APP, {
				body: {
					merchant_id: req.body.merchant_id
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: {},
						info: {
							service: 'getProfile'
						}
					});

				callback(null, result.data);
			});
		},
		function output (merchant, callback) {
			callback(null, {
				code: 'MERCHANT_FOUND',
				data: merchant
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};