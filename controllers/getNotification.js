"use strict";

const async = require('async');

/**
* @type: service
* @req: limit, offset, merchant_id, start_date, end_date
* @return callback object
*/

module.exports = function (APP, req, callback) {

	async.waterfall([
		function validation (callback) {
			var query = {
				where : {},
				order :[['created_at', 'DESC']]
			};
			if (req.body.type) {
				query.where.type = req.body.type
			}

			if (req.body.merchant_id) {
				query.where.account_number = req.body.merchant_id
			}

			if (req.body.type) {
				query.where = {
					type : req.body.type
				}
			}
			if (req.body.limit) {
				query.limit = Number(req.body.limit)
			}

			if (req.body.offset) {
				query.offset = Number(req.body.offset)
			}
			callback(null, query);
		},
		function gettingNotifications (query, callback) {
			APP.models.mysql.notification.findAndCountAll(query).then((rows) => {
				return callback(null, rows);
			}).catch((err) => {
				return callback({
					code: 'ERR_DATABASE',
					data: JSON.stringify(err)
				});
			});
			// callback(null, [				
			// 	{
			// 		id: '724',
			// 		referrer_id: '790',
			// 		title: "Transfer Antar Emoney",
			// 		content: "Transfer Antar Emoney kepada Dali 2 sebesar Rp.1 Sukses",
			// 		read: '0',
			// 		type: '1',
			// 		created_at: "2018-09-24T11:18:22.000Z"
			// 	},
			// 	{
			// 		id: '725',
			// 		referrer_id: '791',
			// 		title: "Withdraw",
			// 		content: "Withdraw sebesar Rp.1 Sukses",
			// 		read: '1',
			// 		type: '1',
			// 		created_at: "2018-09-24T11:18:22.000Z"
			// 	},
			// 	{
			// 		id: '726',
			// 		referrer_id: '792',
			// 		title: "Transfer Merchant",
			// 		content: "Transfer Merchant sebesar Rp.1 Sukses",
			// 		read: '0',
			// 		type: '1',
			// 		created_at: "2018-09-24T11:18:22.000Z"
			// 	}
			// ]);
		},
		function output (transactions, callback) {
			callback(null, {
				code: 'MERCHANT_NOTIFICATION_FOUND',
				data: transactions
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};