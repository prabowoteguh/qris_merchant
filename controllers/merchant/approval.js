"use strict";

module.exports = function (APP, req, callback) {
	return APP.db.sequelize.transaction().then(function (t) {
	  	var params = APP.queries.update('merchant_approval', req, APP.models);
		if (Object.keys(req.body.dataUpdate).length < 1) return callback({
			code: 'ERR_NOTHING',
			data: params
		});
		return APP.models.mysql.merchant.update(params.dataUpdate, params.dataQuery, {transaction: t})
		.then(merchant => {
			params = APP.queries.update('user_merchant_password_approval', req, APP.models);
			return APP.models.mysql.userMerchant.update(params.dataUpdate, params.dataQuery, {transaction: t}).then(merchant => {
				return callback(null, t);
			}).catch(err => {
				console.log(err)
				t.rollback();
				if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
						code: 'ERR_NOTHING',
						data: err
					});

				if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
							code: 'ERR_DUPLICATE',
							data: err
						});
				return callback({
					code: 'ERR_DATABASE',
					data: JSON.stringify(err)
				});
			});

		}).catch(err => {
			console.log(err)
			t.rollback();
			if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
					code: 'ERR_NOTHING',
					data: err
				});

			if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
						code: 'ERR_DUPLICATE',
						data: err
					});

			return callback({
				code: 'ERR_DATABASE',
				data: JSON.stringify(err)
			});
		});
	});
	
};
