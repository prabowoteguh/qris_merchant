"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.select('merchant', req, APP.models);

	APP.models.mysql.merchant.findAndCountAll(params).then((rows) => {
		return callback(null, {
			code: (rows && (rows.length > 0)) ? 'FOUND' : 'NOT_FOUND',
			data: rows,
			info: {
				dataCount: rows.length
			}
		});
	}).catch((err) => {
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};