"use strict";
module.exports = function (APP, req, callback) {
	let query = {
        attributes : ['id', 'merchant_id','username', 'device_key', 'device_type', 'updated_at'],
        order: [
            ['updated_at', 'ASC'],
        ]
    }
    if (req.body.limit) {
        query.limit =  parseInt(req.body.limit);
    }
    if (req.body.offset) {
        query.offset = parseInt(req.body.offset);
    }

    query.where = {
    	device_key : {
	        $ne: null
	    }
    };

    if (req.body.username) {
        query.where.username = req.body.username;
    }

    if (req.body.id) {
        query.where.id = req.body.id;
    }

    if (req.body.merchant_id) {
        query.where.merchant_id = req.body.merchant_id;
    }

	APP.models.mysql.userMerchant.findAndCountAll(query).then((rows) => {
		console.log(rows.count)
		return callback(null, {
			code: (rows.rows && (rows.rows.length > 0)) ? 'FOUND' : 'DATA_NOT_FOUND_CODE_01',
			data: rows
		});
	}).catch((err) => {
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};