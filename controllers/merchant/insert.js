"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.insert('merchant', req, APP.models);

	APP.models.mysql.merchant.build(params).save().then(merchant => {
		return callback(null, {
			code: 'MERCHANT_INSERT_SUCCESS',
			data: merchant.dataValues || params
		});
	}).catch(err => {
		if (err.original && err.original.code === 'ER_DUP_ENTRY') return callback({
				code: 'ERR_DUPLICATE',
				data: params
			});

		if (err.original && err.original.code === 'ER_EMPTY_QUERY') return callback({
				code: 'ERR_NOTHING',
				data: params
			});

		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};