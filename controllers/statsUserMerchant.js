"use strict";

const async = require('async');
const checkUserMerchant = require('../controllers/checkUserMerchant.js');
const userMerchantUpdate = require('../controllers/userMerchant/update.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			let query = {
				where : {}
			}
			callback(null, query);
		},
		function countUserMerchant (query, callback) {
			if (req.body.merchant_id) {
				query.where.merchant_id =  req.body.merchant_id;
			}
			APP.models.mysql.userMerchant.count(query).then(users => {
				return callback(null, {
					code: 'OK',
					data: users
				});
			}).catch(err => {
				return callback({
					code: 'ERR_DATABASE',
					data: JSON.stringify(err)
				});
			});
		},
	], (err, result) => {
		if (err) return callback(err);
		callback(null, result);
	});
};