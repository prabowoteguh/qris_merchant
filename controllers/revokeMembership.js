"use strict";

const async = require('async');
const revokeMembership = require('../controllers/membership/revoke.js');
/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "id"
					},
					info: {
						function: "revokeMembership"
					}
				});

			if (!req.body.emoney_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "emoney_va"
					},
					info: {
						function: "revokeMembership"
					}
				});
			if (!req.body.revoke_by) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "revoke_by"
					},
					info: {
						function: "revokeMembership"
					}
				});
		
			callback(null, {});
		},

		function revoke (data, callback) {
			revokeMembership(APP, {
				body: {
					dataQuery: {
						id 			: req.body.id,
						merchant_id	: req.body.merchant_id,
						emoney_va 	: req.body.emoney_va,
						status 		: 1,
					},
					dataUpdate: {
						status		: 0,
						revoke_by	: req.body.revoke_by,
						revoke_at	: APP.db.Sequelize.fn('NOW')
					}
				}
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== 'REVOKE_MEMBERSHIP_SUCCESS') return callback(result);

				callback(null, data);
			});
		},
		function output (data, callback) {
			callback(null, {
				code: 'REVOKE_MEMBERSHIP_SUCCESS',
				data: req.body
			});
		}
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};