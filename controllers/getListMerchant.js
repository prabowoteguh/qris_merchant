"use strict";

const async = require('async');
const merchantGet = require('../controllers/merchant/get.js');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function gettingList (data, callback) {
			const params = APP.queries.storePro('get_all_merchant', {
				body: {
					search: req.body.search,
					status: req.body.status,
					limit: req.body.limit,
					offset: req.body.offset,
					order: req.body.order,
					app: req.body.app,
					type: req.body.type,
					lat: req.body.lat,
					long: req.body.long,
					id_merchant: req.body.id_merchant
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(merchant => {
				if (merchant.length < 1) return callback(null, {
						code: 'MERCHANT_NOT_FOUND',
						data: {
							rows: [],
							count: '0'
						},
						info: {
							function: "getListMerchant"
						}
					});

				data.merchant = merchant;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getListMerchant"
					}
				});
			});
		},
		function countingData (data, callback) {
			let params = APP.queries.storePro('get_all_merchant_count', {
				body: {
					search: req.body.search,
					status: req.body.status,
					type: req.body.type,
					lat: req.body.lat,
					long: req.body.long
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(merchantCount => {
				if (merchantCount.length < 1) return callback(null, {
						code: 'NOT_FOUND',
						data: [],
						info: {
							function: "getListMerchant"
						}
					});

				if (merchantCount[0].code && merchantCount[0].code !== '00') return callback({
						code: 'NOT_FOUND',
						message: merchantCount[0].message,
						data: {
							q_code: merchantCount[0].code,
							q_message: merchantCount[0].message
						},
						info: {
							function: "getListMerchant"
						}
					});

				data.merchantTotal = merchantCount[0].total;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getListMerchant"
					}
				});
			});
		},
		function output (data, callback) {
			if(data.merchantTotal == 0){
				callback(null, {
					code: 'DATA_NOT_FOUND_CODE_01',
					data: {
						image_url: process.env.URL_CDN_IMAGE,
						rows: data.merchant || [],
						count: data.merchantTotal
					}
				});
			}else{
				callback(null, {
					code: 'MERCHANT_FOUND',
					data: {
						image_url: process.env.URL_CDN_IMAGE,
						rows: data.merchant || [],
						count: data.merchantTotal
					}
				});
			}
			
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};