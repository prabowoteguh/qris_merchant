"use strict";

const async 		    = require('async');
const randomstring 	    = require('randomstring');
const settingController = require('../controllers/setting/setting.js');
const checkMerchant     = require('../controllers/checkMerchant.js');
const moment            = require('moment');
/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	var json = req.body;
    var token_password = randomstring.generate({
        length: 6,
        charset: 'numeric'
    });
    var interval_token_admin;
    var output  = {};
    var dataEmail = {};

    async.waterfall([
        function(callback) {
            if (!json.username) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "username" } });
            callback(null, true)
        },
        function(data, callback) {
            let query = {
                where: {
                    username: json.username,
                }
            }
            APP.models.mysql.userMerchant.findOne(query).then(result => {
                if (result) {
                    console.log(result)
                    result = result.toJSON();
                    if (result.active_status != "1") {
                        output = {
                            code: "USER_NOT_ACTIVE"
                        }
                        callback(output)
                    } else {
                        callback(null, result)
                    }
                } else {
                    //Di anggap user ada
                    output = {
                        code: 'CREATE_TOKEN_PASSWORD'
                    }
                    callback(output);
                }

            }).catch(err => {
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
        function(user, callback) {
            if (user.email != null) {
                console.log('masuk kondisi memiliki email : ',user.email)
                dataEmail.email = user.email;
                dataEmail.username = user.username;
                dataEmail.name = user.name;

                callback(null, true)
            }else{
                console.log('masuk kondisi tidak memiliki email : ',json)
                APP.models.mysql.merchant.findOne({
                    where: {
                        code : json.username
                    }
                }).then(result => {
                    console.log('hasil masuk kondisi tidak memiliki email : ',result)
                   if (result) {
                        result = result.toJSON();
                        dataEmail.email = result.email;
                        dataEmail.username = result.code;
                        dataEmail.name = result.name;
                        callback(null, true)
                    } else {
                        output = {
                            code: 'CREATE_TOKEN_PASSWORD'
                        }
                        callback(output);
                    }
                });
            }
        },
        function(data, callback) {
            settingController(APP, req, (err, result) => {
                if (err) return callback(err);
                interval_token_admin = result.data.interval_token_admin;
                callback(null, true);
            });
        },
        function(data, callback) {
            let query = {
                otp_code: token_password,
                otp_expired: moment().add(interval_token_admin, 'minutes').format('Y-MM-DD HH:mm:ss')
            }
            let where = {
                where: {
                    username: json.username,
                }
            }

            APP.models.mysql.userMerchant.update(query, where).then(result => {
                dataEmail.otp_code = query.otp_code;
                dataEmail.otp_expired = query.otp_expired;
                console.log('Data email : ', dataEmail)
                output = {
                    code : "CREATE_TOKEN_PASSWORD",
                    data : dataEmail
                }
                callback(null, output);
            }).catch(err => {
                console.log(err)
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
            })
        },
    ], (err, result) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, result)
        }
    });
};