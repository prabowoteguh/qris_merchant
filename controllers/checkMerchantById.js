	"use strict";

const async = require('async');
const merchantGet = require('./merchant/get.js');

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_id) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_id"
					},
					info: {
						function: "checkMerchant"
					}
				});

			var validateMerchantId = APP.validation.true(req.body.merchant_id);

			if (validateMerchantId !== true) return callback(validateMerchantId);

			callback(null, {});
		},
		function gettingMerchantById (data, callback) {
			merchantGet(APP, {
				body: {
					id: req.body.merchant_id
				}
			}, (err, result) => {
				console.log(err);
				if (err) return callback(err);

				if (result && result.code !== 'FOUND') return callback(result);

				callback(null, {
					code: result.code,
					data: result.data
				});
			});
		},
		function output (output, callback) {
			callback(null, {
				code: output.code,
				data: output.data
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};