"use strict";

const async = require('async');

/**
* @type: service
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			callback(null, {});
		},
		function gettingData (data, callback) {
			let params = APP.queries.storePro('get_withdraw', {
				body: {
					search: req.body.search,
					merchant_code: req.body.merchant_id,
					limit: req.body.limit,
					start_date: req.body.start_date,
					end_date: req.body.end_date,
					offset: req.body.offset,
					order: req.body.order
				}
			});
			console.log(req.body)

			APP.db.sequelize.query(params.query, params.bind).then(transaction => {
				if (transaction.length < 1) return callback(null, {
						code: 'NOT_FOUND',
						data: [],
						info: {
							function: "getWithdraw"
						}
					});

				if (transaction[0].code && transaction[0].code !== '00') return callback({
						code: 'NOT_FOUND',
						message: transaction[0].message,
						data: {
							q_code: transaction[0].code,
							q_message: transaction[0].message
						},
						info: {
							function: "getWithdraw"
						}
					});

				callback(null, transaction);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getWithdraw"
					}
				});
			});
		},
		function gettingDataCount (transaction, callback) {
			let params = APP.queries.storePro('get_withdraw_count', {
				body: {
					merchant_code: req.body.merchant_id,
					search: req.body.search,
					start_date: req.body.start_date,
					end_date: req.body.end_date
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(transactionCount => {

				callback(null, transaction, transactionCount[0].total);
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "getWithdraw"
					}
				});
			});
		},
		function output (transactions, transactionCount, callback) {
			callback(null, {
				code: transactions.code || 'MERCHANT_WITHDRAW_FOUND',
				data: {
					rows: transactions.data || transactions,
					count: Number(transactionCount)
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};


