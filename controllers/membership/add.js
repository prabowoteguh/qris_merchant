"use strict";

const async = require('async');
const randomstring = require('randomstring');
const moment = require('moment');
const db = require('../../config/db.js')
var output = {};

module.exports = (APP, req, callback) => {
    var json = req.body;
    var query = {};
    async.waterfall([
        function(callback) {
            if (!json.merchant_id) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "merchant_id" } });
            if (!json.emoney_va) return callback({ code: 'MISSING_KEY', data: { missing_parameter: "emoney_va" } });
            callback(null, true);
        },
        function(index, callback) {

            APP.models.mysql.membership.findOne({
                where :
                {
                    merchant_id         : json.merchant_id,
                    emoney_va           : json.emoney_va,
                    status              : 1
                }
            }).then(result => {
                if(result){
                    output = {
                        code: 'ADD_MEMBERSHIP_FAILED_USER_ALREADY_REGISTERED',
                        data: result
                    }
                    callback(output)
                    return;
                }else{
                    callback(null, null)
                }
            }).catch(err => {
                console.log(err)
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
                return;
            })
        },
        function(index, callback) {

            APP.models.mysql.membership.create({
                merchant_id         : json.merchant_id,
                emoney_va           : json.emoney_va,
                name                : json.name,
                email               : json.email,
                status              : 1,
                action_by           : json.action_by,
                ip_address          : json.ip_address,

            }).then(result => {
                output = {
                    code : 'ADD_MEMBERSHIP_SUCCESS',
                    data : {
                        emoney_va   : json.emoney_va,
                        name        : json.name,
                        email       : json.email,
                    }
                }
                callback(null, output)
            }).catch(err => {
                console.log(err)
                output = {
                    code: 'ERR_DATABASE',
                    ori_message: err.message
                }
                callback(output)
                return null
            })
        }
    ], (err, result) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, result)
        }
    });
};