"use strict";

module.exports = function (APP, req, callback) {

    let query = {
        attributes: ['id', 'emoney_va', 'name', 'email', 'status',[APP.db.Sequelize.fn('date_format', APP.db.Sequelize.col('membership.created_at'), '%Y-%m-%d %H:%m:%s'), "created_at"], [APP.db.Sequelize.fn('date_format', APP.db.Sequelize.col('membership.updated_at'), '%Y-%m-%d %H:%m:%s'), "updated_at"]],
        order: [
            ['name', 'ASC'],
        ]
            
    }
    query.where = {};
    if (req.body.search) {
        query.where.$or = [
            {
                name: { $like: '%' + req.body.search + '%' }
            },
            {
                email: { $like: '%' + req.body.search + '%' }
            },
            {
                emoney_va: { $like: '%' + req.body.search + '%' }
            }
        ]
    }
    if (req.body.merchant_id) {
        query.where.merchant_id =  parseInt(req.body.merchant_id);
    }

    if (req.body.emoney_va) {
        query.include = [
            {
                model: APP.models.mysql.merchant,
                required: true,
                attributes: ['id','code','name','phone', 'address', 'city'],

            }
        ];
        query.where.emoney_va =  parseInt(req.body.emoney_va);
    }

    if (req.body.status) {
        query.where.status =  parseInt(req.body.status);
    }

    if (req.body.limit) {
        query.limit =  parseInt(req.body.limit);
    }
    if (req.body.offset) {
        query.offset = parseInt(req.body.offset);
    }

    if (req.body.order) {
        switch(req.body.order) {
            case 'name_asc':
                query.order =  [['name', 'ASC']]
                break;
            case 'name_desc':
                query.order =  [['name', 'DESC']]
                break;
            case 'email_asc':
                query.order =  [['email', 'ASC']]
                break;
            case 'email_desc':
                query.order =  [['email', 'DESC']]
                break;
            case 'created_asc':
                query.order =  [['created_at', 'ASC']]
                break;
            case 'created_desc':
                query.order =  [['created_at', 'DESC']]
                break;
            case 'emoney_asc':
                query.order =  [['emoney_va', 'ASC']]
                break;
            case 'emoney_desc':
                query.order =  [['emoney_va', 'DESC']]
                break;
        }
    }
    query.raw = true;

	APP.models.mysql.membership.findAndCountAll(query).then((rows) => {
        // if(rows.rows && (rows.rows.length > 0)){
        //     return callback(null, {
        //         code: 'FOUND',
        //         data: rows,
        //     });
        // }else{
        //     return callback(null, {
        //         code: 'NOT_FOUND',
        //         data: rows,
        //     });
        // }
         return callback(null, {
               code: (rows.rows && (rows.rows.length > 0)) ? 'FOUND' : 'DATA_NOT_FOUND_CODE_01',
                data: rows,
            });
       
	}).catch((err) => {
        console.log(err)
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};