"use strict";

const async = require('async');
const vascommkit = require('vascommkit');
const moment = require('moment');
const updateMerchantTransaction = require('../controllers/merchantTransaction/update.js');
const detailMerchantTransaction = require('../controllers/merchantTransactionDetail/get.js');
const emoneyServices = require('../config/emoneyServices.json');
const checkVa = require('../controllers/checkVa.js');
const insertNotification = require('../controllers/notification/insert.js');

/**
* @type: service
* @req: invoice, emoney_va
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.invoice) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "invoice"
					},
					info: {
						service: "purchasePayment"
					}
				});

			if (!req.body.emoney_va) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "emoney_va"
					},
					info: {
						service: "purchasePayment"
					}
				});

			if (!req.body.poin) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "poin"
					},
					info: {
						service: "purchasePayment"
					}
				});

			var validateInvoice = APP.validation.true(req.body.invoice);
			var validateEmoneyVa = APP.validation.true(req.body.merchant_id);

			if (validateInvoice !== true) return callback(validateInvoice);
			if (validateEmoneyVa !== true) return callback(validateEmoneyVa);

			if (req.body.type) {
				if (req.body.type !== 'split_bill' && req.body.type !== 'reversal') {
					if (!req.body.pin) return callback({
							code: 'MISSING_KEY',
							data: {
								missing_parameter: "pin"
							},
							info: {
								service: "purchasePayment"
							}
						});

					var validatePin = APP.validation.true(req.body.pin);

					if (validatePin !== true) return callback(validatePin);
				}
			} else {
				if (!req.body.pin) return callback({
						code: 'MISSING_KEY',
						data: {
							missing_parameter: "pin"
						},
						info: {
							service: "purchasePayment"
						}
					});

				var validatePin = APP.validation.true(req.body.pin);

				if (validatePin !== true) return callback(validatePin);
			}

			callback(null, {});
		},
		function checkingVaAccount (data, callback) {
			APP.request.emoney(emoneyServices.checkAccount, {
				emoney_va: req.body.emoney_va
			}, (err, result) => {
				if (err) return callback(err);

				if (result && result.code !== '00') return callback(result);

				data.account = result.data;

				callback(null, data);
			});
		},
		function checkingInvoice (data, callback) {
			const params = APP.queries.storePro('merchant_invoice_check', {
				body: {
					invoice: req.body.invoice
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(reply => {
				if (reply[0].code && reply[0].code !== '00') return callback({
						code: 'PURCHASE_PAYMENT_FAILED',
						data: {
							q_code: reply[0].code,
							q_message: reply[0].message
						},
						info: {
							service: 'purchasePayment'
						}
					});

				data.transaction_id = reply[0].id;
				data.amount = String(reply[0].nominal);
				data.account_number = reply[0].account_number;
				data.merchant_va = reply[0].merchant_va;

				data.merchant_id = reply[0].merchant_code;
				data.merchant_name = reply[0].name;
				data.account_name = reply[0].account_name;
				data.user_merchant = reply[0].user_merchant;

				callback(null, data);
			}).catch(err => {
				callback({
					code: 'PURCHASE_PAYMENT_FAILED',
					data: {},
					info: {
						err: JSON.stringify(err),
						service: 'purchasePayment'
					}
				});
			});
		},
		function dataAmountDebeted (data, callback) {
			data.amountDebeted = String(Number(data.amount) - Number(req.body.poin));

			callback(null, data);
		},
		function checkingMerchantVa (data, callback) {
			checkVa(APP, {
				body: {
					merchant_va: data.merchant_va
				}
			}, (err, result) => {
				if (err) {
					if (err.code === 'NOT_FOUND') return callback({
							code: 'MERCHANT_DOESNT_EXISTS',
							data: req.body,
							info: {
								service: 'purchasePayment'
							}
						});

					return callback(err);
				}

				if (result && result.code !== 'FOUND') return callback({
						code: 'MERCHANT_DOESNT_EXISTS',
						data: req.body,
						info: {
							service: 'purchasePayment'
						}
					});

				data.merchant_reff_id = result.data.id;

				callback(null, data);
			});
		},
		function generateReffnum (data, callback) {
			data.reffnum = APP.generator.generateReffnum();

			callback(null, data);
		},
		function emoneyTFE2E (data, callback) {
			if (req.body.type && req.body.type === 'source') {
				APP.request.emoney(emoneyServices.debetSource, {
					jenis_source: req.body.jenis_source,
					card_num: req.body.card_num,
					account_num: req.body.account_num,
					amount: data.amountDebeted,
					reff_id: data.transaction_id,
					merchant_id: data.merchant_id
				}, (err, result) => {
					if (err) {
						err.code = 'PURCHASE_PAYMENT_FAILED';

						return callback(err);
					};

					if (result && result.code !== '00') return callback(result);

					data.journal = APP.generator.generateJournal(req.body.emoney_va);
					data.emoney_va = req.body.emoney_va;
					// data.amount = data.amountDebeted;

					return callback(null, data);
				});
			} else {
				APP.request.emoney(emoneyServices.e2e, {
					from_va: req.body.emoney_va,
					to_va: data.merchant_va,
					amount: data.amountDebeted,
					narration: req.body.narration || "Merchant purchase payment",
					pin: req.body.pin,
					type: req.body.type
				}, (err, result) => {
					if (err) {
						err.code = 'PURCHASE_PAYMENT_FAILED';

						return callback(err);
					};

					if (result && result.code !== '00') return callback(result);

					data.journal = result.data.journal;
					data.emoney_va = result.data.emoney_va || req.body.emoney_va;
					// data.amount = result.data.amount;

					return callback(null, data);
				});
			}
		},
		function updateTransactionStatus (data, callback) {
			const paymentAt = APP.db.Sequelize.fn('NOW');
			const paymentAtMoment = moment().format('YYYY-MM-DD HH:mm:ss');

			updateMerchantTransaction(APP, {
				body: {
					dataQuery: {
						id: data.transaction_id,
						status: '0'
					},
					dataUpdate: {
						status: '1',
						payment_at: paymentAt,
						biller_name: data.account.name,
						biller_code: data.emoney_va,
						requestId: req.body.sid,
						merchant_va: data.merchant_va,
						wallet_type_code: 'SOF01',
						action_by: data.merchant_reff_id,
						no_jurnal: data.journal
					}
				}
			}, (err, result) => {
				if (err) {
					if (err && err.code === 'ERR_NOTHING') return callback({
							code: 'INVALID_TRANSACTION',
							data: req.body
						});

					return callback(err);
				};

				if (result && result.code !== 'MERCHANT_TRANSACTION_UPDATE_SUCCESS') return callback({
						code: 'INVALID_TRANSACTION',
						data: req.body
					});

				data.datetime = paymentAtMoment;

				callback(null, data);
			});
		},
		function insertingNotification (data, callback) {
			insertNotification(APP, {
				body: {
					referrer_id: data.reffnum,
					account_number: data.account_number,
					merchant_va: req.body.emoney_va,
					type: 1,
					title: 'Bayar Merchant',
					content: 'Transaksi Pembelian invoice '+req.body.invoice+' Sebesar Rp '+ (data.amount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' Berhasil di Bayar',
					read: 0,
					action_by: req.body.action_by,
					ip_address: req.body.ip_address
				}
			}, (err, result) => {
				callback(null, data);
			});
		},
		function getProduct (data, callback) {
			detailMerchantTransaction(APP, {
				body: {
					transactions_merchant_id:  data.transaction_id
				}
			}, (err, product) => {
				callback(null, data, product);
			});
		},
		function output (data, product, callback) {
			callback(null, {
				code: 'PURCHASE_PAYMENT_SUCCESS',
				data: {
					transaction_id: data.transaction_id,
					reffnum: data.reffnum,
					invoice: req.body.invoice,
					emoney_va: req.body.emoney_va,
					journal: data.journal,
					amount: data.amount,
					datetime: data.datetime,
					account_number: data.account_number,
					account_name: data.account_name,
					merchant_id: data.merchant_id,
					merchant_name: data.merchant_name,
					va_name: data.account.name,
					merchant_va : data.merchant_va,
					user_merchant: data.user_merchant,
					product : product
				}
			});
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};
