"use strict";

const async = require('async');

/**
* @type: service
* @req: merchant_code, name
* @return callback object
*/

module.exports = function (APP, req, callback) {
	async.waterfall([
		function validation (callback) {
			if (!req.body.merchant_code) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "merchant_code"
					},
					info: {
						function: "createProductType"
					}
				});

			if (!req.body.name) return callback({
					code: 'MISSING_KEY',
					data: {
						missing_parameter: "name"
					},
					info: {
						function: "createProductType"
					}
				});

			req.body.merchant_code = req.body.merchant_code.toUpperCase();
			req.body.name = (req.body.name).replace(/\b\w/g, l => l.toUpperCase());
			callback(null, true);
		},
		function insertingProductTypes (index, callback) {
			const params = APP.queries.storePro('create_product_type', {
				body: {
					merchant_code: req.body.merchant_code,
					name: req.body.name,
					ip_address: req.body.ip_address
				}
			});

			APP.db.sequelize.query(params.query, params.bind).then(product => {
				if (product.length < 1) return callback(null, {
						code: 'CREATE_PRODUCT_TYPE_FAILED',
						data: req.body,
						info: {
							function: "createProductType"
						}
					});

				if (product[0].code && product[0].code !== '00') return callback({
						code: product[0].code,
						message: product[0].message,
						data: req.body,
						info: {
							function: "createProductType"
						}
					});

				callback(null, {
					code: 'CREATE_PRODUCT_TYPE_SUCCESS',
					data: req.body
				});
			}).catch(err => {
				callback({
					code: 'GENERAL_ERR',
					data: {},
					info: {
						err: JSON.stringify(err),
						function: "createProductType"
					}
				});
			});
		},
		function output (product, callback) {
			callback(null, product);
		},
	], (err, result) => {
		if (err) return callback(err);

		callback(null, result);
	});
};