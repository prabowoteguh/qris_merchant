"use strict";

module.exports = function (APP, req, callback) {
	var params = APP.queries.delete('merchant_transaction', req, APP.models);

	APP.models.mysql.vaMutation.destroy(params).then(deleted => {
		if (!deleted) return callback(null, {
				code: 'ERR_NOTHING',
				data: params.where
			});

		return callback(null, {
			code: 'MERCHANT_TRANSACTION_DELETE_SUCCESS',
			data: params.where
		});
	}).catch(err => {
		return callback({
			code: 'ERR_DATABASE',
			data: JSON.stringify(err)
		});
	});
};