"use strict";

module.exports = function (sequelize, Sequelize) {
  var Merchant = sequelize.define('merchant', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    code: {
      type: Sequelize.STRING(8)
    },
    name: {
      type: Sequelize.STRING(255)
    },
    business_type_id: {
      type: Sequelize.INTEGER(11)
    },
    owner: {
      type: Sequelize.STRING(255)
    },
    msisdn: {
      type: Sequelize.STRING(20)
    },
    email: {
      type: Sequelize.STRING(100)
    },
    phone: {
      type: Sequelize.STRING(20)
    },
    address: {
      type: Sequelize.STRING(100)
    },
    kelurahan: {
      type: Sequelize.STRING(50)
    },
    kecamatan: {
      type: Sequelize.STRING(50)
    },
    city: {
      type: Sequelize.STRING(50)
    },
    province: {
      type: Sequelize.STRING(50)
    },
    lat: {
      type: Sequelize.STRING(255)
    },
    long: {
      type: Sequelize.STRING(255)
    },
    open_hours: {
      type: Sequelize.STRING(10)
    },
    closed_hours: {
      type: Sequelize.STRING(2)
    },
    image: {
      type: Sequelize.STRING(255)
    },
    banner: {
      type: Sequelize.STRING(255)
    },
    account_name: {
      type: Sequelize.STRING(100)
    },
    account_number: {
      type: Sequelize.STRING(45)
    },
    bank_code_id: {
      type: Sequelize.INTEGER(11)
    },
    bank_account_number: {
      type: Sequelize.STRING(45)
    },
    bank_account_name: {
      type: Sequelize.STRING(255)
    },
    image_nik: {
      type: Sequelize.STRING(255)
    },
    image_npwp: {
      type: Sequelize.STRING(255)
    },
    image_merchant: {
      type: Sequelize.STRING(255)
    },
    image_pic: {
      type: Sequelize.STRING(255)
    },
    ktp: {
      type: Sequelize.STRING(255)
    },
    npwp: {
      type: Sequelize.STRING(255)
    },
    nmid: {
      type: Sequelize.STRING(255)
    },
    merchant_criteria_id: {
      type: Sequelize.INTEGER(11)
    },
    merchant_category_id: {
      type: Sequelize.INTEGER(1)
    },
    omzet: {
      type: Sequelize.INTEGER(11)
    },
    status: {
      type: Sequelize.INTEGER(2)
    },
    note: {
      type: Sequelize.STRING(255)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    approval_by: {
      type: Sequelize.STRING(10)
    },
    approval_at: {
      type: Sequelize.DATE,
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  Merchant.associate = function (models) {};

  return Merchant;
};