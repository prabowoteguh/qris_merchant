"use strict";

module.exports = function (sequelize, Sequelize) {
    var merchant_criteria = sequelize.define('merchant_criteria', {
        id: {
            type : Sequelize.INTEGER(11),
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            type : Sequelize.STRING
        },
        name: {
            type : Sequelize.STRING
        },
    }, {});

    // Test.associate = function (models) {};

    return merchant_criteria;
};
