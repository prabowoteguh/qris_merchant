"use strict";

module.exports = function (sequelize, Sequelize) {
  var MerchantTransaction = sequelize.define('transactions_merchant', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    requestId: {
      type: Sequelize.STRING(16)
    },
    account_number: {
      type: Sequelize.STRING(20)
    },
    merchant_va: {
      type: Sequelize.STRING(20)
    },
    wallet_type_code: {
      type: Sequelize.STRING(10)
    },
    transactions_type_code: {
      type: Sequelize.STRING(10)
    },
    biller_type: {
      type: Sequelize.STRING(255)
    },
    biller_code: {
      type: Sequelize.STRING(255)
    },
    biller_name: {
      type: Sequelize.STRING(255)
    },
    nominal: {
      type: Sequelize.INTEGER(100)
    },
    no_jurnal: {
      type: Sequelize.STRING(6)
    },
    note: {
      type: Sequelize.TEXT('tiny')
    },
    note_2: {
      type: Sequelize.TEXT('tiny')
    },
    fee: {
      type: Sequelize.INTEGER(100)
    },
    admin_fee: {
      type: Sequelize.INTEGER(100)
    },
    status: {
      type: Sequelize.INTEGER(2)
    },
    sequence: {
      type: Sequelize.INTEGER(255)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    payment_at: {
      type: Sequelize.DATE
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  MerchantTransaction.associate = function (models) {};

  return MerchantTransaction;
};