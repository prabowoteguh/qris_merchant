"use strict";

module.exports = function (sequelize, Sequelize) {
  var UserMerchant = sequelize.define('user_merchant', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    merchant_id: {
      type: Sequelize.STRING(45)
    },
    type: {
      type: Sequelize.INTEGER(11)
    },
    username: {
      type: Sequelize.STRING(100)
    },
    name: {
      type: Sequelize.STRING(75)
    },
    msisdn: {
      type: Sequelize.STRING(20)
    },
    password: {
      type: Sequelize.STRING(255)
    },
    menu: {
      type: Sequelize.TEXT('long')
    },
    active_status: {
      type: Sequelize.INTEGER(1)
    },
    image: {
      type: Sequelize.TEXT('tiny')
    },
    device_key: {
      type: Sequelize.STRING(255)
    },
    device_type: {
      type: Sequelize.STRING(255)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    otp_code: {
      type: Sequelize.INTEGER(11)
    },
    otp_expired: {
     type: Sequelize.STRING(50)
    },
    email: {
      type: Sequelize.STRING(200)
     },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  UserMerchant.associate = function (models) {};

  return UserMerchant;
};