"use strict";

module.exports = function (sequelize, Sequelize) {
    var bankCode = sequelize.define('bank_code', {
        id: {
            type : Sequelize.INTEGER(11),
            primaryKey: true,
            autoIncrement: true
        },
        bank_code: {
            type : Sequelize.STRING
        },
        bank_name: {
            type : Sequelize.STRING
        },
        global_unique_identifier: {
            type : Sequelize.STRING
        }
    }, {});

    // Test.associate = function (models) {};

    return bankCode;
};
