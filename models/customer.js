"use strict";

module.exports = function(sequelize, Sequelize) {
  var Customer = sequelize.define('customer', {
    id: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true, unique: true },
    emoney_va: Sequelize.STRING(45),
    //username: Sequelize.STRING(45),
    msisdn: Sequelize.STRING(45),
    password: Sequelize.STRING(45),
    active_status: Sequelize.INTEGER,
    update_status: Sequelize.INTEGER,
    otp_code: Sequelize.STRING(45),
    expired_at: Sequelize.STRING,
    ssn_1: { type: Sequelize.STRING(255), field: 'code_ssn_1' },
    ssn_2: { type: Sequelize.STRING(255), field: 'code_ssn_2' },
    device_key: Sequelize.STRING,
    device_type: Sequelize.STRING,
    token_fingerprint: Sequelize.STRING,
    expired_token: Sequelize.STRING,
    attempt_status_send_otp: Sequelize.INTEGER,
    attempt_status_check_otp: Sequelize.INTEGER,
    attempt_status_login: Sequelize.INTEGER, //login status
    blocked_until_check_otp: Sequelize.STRING,
    blocked_until_send_otp: Sequelize.STRING,
    last_attempt_status_send_otp: Sequelize.STRING,
    last_attempt_status_check_otp: Sequelize.STRING,
    image: Sequelize.STRING,
    banner: Sequelize.STRING,
    referral_code: Sequelize.STRING,
    referenced_by: Sequelize.STRING,
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    action_by: Sequelize.INTEGER,
    ip_address: Sequelize.STRING(45)
  }, {});

  Customer.associate = function(models) {};

  return Customer;
};