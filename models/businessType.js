"use strict";

module.exports = function (sequelize, Sequelize) {
  var BusinessType = sequelize.define('business_type', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING(75)
    },
    status: {
      type: Sequelize.INTEGER(2)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  BusinessType.associate = function (models) {};

  return BusinessType;
};