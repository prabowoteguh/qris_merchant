"use strict";

module.exports = function (sequelize, Sequelize) {
    var merchant_category = sequelize.define('merchant_category', {
        id: {
            type : Sequelize.INTEGER(11),
            primaryKey: true,
            autoIncrement: true
        },
        mcc: {
            type : Sequelize.STRING
        },
        merchant_category: {
            type : Sequelize.STRING
        },
    }, {});

    // Test.associate = function (models) {};

    return merchant_category;
};
