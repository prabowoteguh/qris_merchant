"use strict";

module.exports = function (sequelize, Sequelize) {
  var ProductType = sequelize.define('product_type', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    merchant_code: {
      type: Sequelize.INTEGER(11)
    },
    name: {
      type: Sequelize.STRING(50)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  ProductType.associate = function (models) {};

  return ProductType;
};