"use strict";

module.exports = function (mongo) {
	if (mongo.models.AccessToken) return mongo.models.AccessToken;

  const ModelSchema = mongo.Schema({
  	accessToken: String,
    accessTokenExpiresAt: Date,
    scope: [String],
    clientId: String,
    userId: String
	});

	return mongo.model('AccessToken', ModelSchema);
};