"use strict";

module.exports = function (mongo) {
	if (mongo.models.RefreshToken) return mongo.models.RefreshToken;

  const ModelSchema = mongo.Schema({
  	refreshToken: String,
    refreshTokenExpiresAt: Date,
    scope: [String],
    clientId: String,
    userId: String
	});

	return mongo.model('RefreshToken', ModelSchema);
};