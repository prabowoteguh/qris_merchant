"use strict";

module.exports = function (mongo) {
	if (mongo.models.Scope) return mongo.models.Scope;

  const ModelSchema = mongo.Schema({
	  scope: [String],
  	default: [String]
	});

	return mongo.model('Scope', ModelSchema);
};