"use strict";

module.exports = function(mongo) {
    //if (mongo.models.Log) return mongo.models.Log;

    const ModelSchema = mongo.Schema({
        endpoint: String,
        email: String,
        subject: String,
        body: String,
        status: String,
        date: { type: Date, default: Date.now }
    });


    //return mongo.model('Log', ModelSchema);

    try {
        if (mongo.model('log_email')) return mongo.model('log_email');
    } catch (e) {
        if (e.name === 'MissingSchemaError') {
            return mongo.model('log_email', ModelSchema, 'log_email');
        }
    }
};