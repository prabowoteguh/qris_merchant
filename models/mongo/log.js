"use strict";

module.exports = function (mongo) {
	if (mongo.models.Log) return mongo.models.Log;

  const ModelSchema = mongo.Schema({
  	endpoint: String,
	  request: Object,
	  response: Object,
	  status: String,
	  date: { type: Date, default: Date.now },
	  time: String
	});

	return mongo.model('Log', ModelSchema);
};