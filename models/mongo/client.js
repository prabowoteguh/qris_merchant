"use strict";

module.exports = function (mongo) {
	if (mongo.models.Client) return mongo.models.Client;

  const ModelSchema = mongo.Schema({
	  name: String,
	  clientId: String,
	  clientSecret: String,
	  userId: String,
	  scope: [String],
	  grants: [String],
	  redirectUris: [String]
	});

	return mongo.model('Client', ModelSchema);
};