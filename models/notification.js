"use strict";

module.exports = function (sequelize, Sequelize) {
  var Notification = sequelize.define('notification_merchant', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    referrer_id: {
      type: Sequelize.INTEGER(11)
    },
    account_number: {
      type: Sequelize.STRING(20)
    },
    merchant_va: {
      type: Sequelize.STRING(20)
    },
    type: {
      type: Sequelize.INTEGER(1)
    },
    title: {
      type: Sequelize.STRING(255)
    },
    content: {
      type: Sequelize.STRING(255)
    },
    read: {
      type: Sequelize.INTEGER(10)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  Notification.associate = function (models) {};

  return Notification;
};