"use strict";

module.exports = function(sequelize, Sequelize) {
  var Setting = sequelize.define('setting', {
    id: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true, unique: true },
    params: Sequelize.STRING(45),
    value: Sequelize.STRING(45),
    note: Sequelize.STRING(45),
    created_at: Sequelize.DATE,
    updated_at: Sequelize.DATE,
    action_by: Sequelize.INTEGER,
    ip_address: Sequelize.STRING(45),
  }, {});

  Setting.associate = function(models) {};

  return Setting;
};