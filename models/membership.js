"use strict";

module.exports = function (sequelize, Sequelize) {
  var Membership = sequelize.define('membership', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    merchant_id: {
      type: Sequelize.INTEGER(11)
    },
    emoney_va: {
      type: Sequelize.STRING(16)
    },
    name: {
      type: Sequelize.STRING(255)
    },
    email: {
      type: Sequelize.STRING(255)
    },
    status: {
      type: Sequelize.INTEGER(1)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    revoke_by: {
      type: Sequelize.STRING(10)
    },
    revoke_at: {
      type: Sequelize.DATE
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    created_at: {
      type: Sequelize.DATE,
    },
    updated_at: {
      type: Sequelize.DATE
    }
  }, {});

  Membership.associate = function (models) {
    Membership.belongsTo(models.merchant, {
        foreignKey: 'merchant_id',
      });
  };
  return Membership;
};