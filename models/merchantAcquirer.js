'use strict';
module.exports = (sequelize, Sequelize) => {
  const MerchantAcquirer = sequelize.define('merchant_acquirer', {
    id: {
        type : Sequelize.INTEGER(11),
        primaryKey: true,
        autoIncrement: true
    },
    merchant_id: {
        type : Sequelize.INTEGER(11)
    },
    merchant_id_acquirer: {
        type : Sequelize.INTEGER(11)
    },
    merchant_pan_input: {
        type : Sequelize.INTEGER(11)
    },
    merchant_pan: {
        type : Sequelize.INTEGER(11)
    },
    merchant_name: {
        type : Sequelize.STRING
    },
    merchant_criteria_id: {
        type : Sequelize.INTEGER(11)
    },
    merchant_pan_format: {
        type : Sequelize.STRING
    },
    bank_id : {
        type : Sequelize.INTEGER(11)
    },
    status: {
        type : Sequelize.STRING
    },
    action_by: {
        type : Sequelize.STRING
    },
    ip_address: {
        type : Sequelize.STRING
    }
  }, {});
  MerchantAcquirer.associate = function(models) {
    MerchantAcquirer.belongsTo(models.bankCode, {foreignKey: 'bank_id'})
    MerchantAcquirer.belongsTo(models.merchantCriteria, {foreignKey: 'merchant_criteria_id'})
  };

  return MerchantAcquirer;
};