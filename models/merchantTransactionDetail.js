"use strict";

module.exports = function (sequelize, Sequelize) {
  var MerchantTransaction = sequelize.define('transactions_merchant_detail', {
    id: {
    	type: Sequelize.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      unique: true,
      autoIncrement: true
    },
    transactions_merchant_id: {
      type: Sequelize.INTEGER(11)
    },
    product_id: {
      type: Sequelize.INTEGER(11)
    },
    quantity: {
      type: Sequelize.INTEGER(255)
    },
    name: {
      type: Sequelize.STRING(255)
    },
    price: {
      type: Sequelize.INTEGER(255)
    },
    total: {
      type: Sequelize.INTEGER(255)
    },
    status: {
      type: Sequelize.INTEGER(2)
    },
    action_by: {
      type: Sequelize.STRING(10)
    },
    ip_address: {
      type: Sequelize.STRING(255)
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }, {});

  MerchantTransaction.associate = function (models) {};

  return MerchantTransaction;
};