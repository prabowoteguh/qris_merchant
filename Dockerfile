FROM gitlab.vascomm.co.id:4567/backend-docker-image/node-8-alpine:latest

# set linux proxy
#ENV http_proxy http://192.168.13.6:3128
#ENV https_proxy http://192.168.13.6:3128

# set npm proxy
#RUN npm config set proxy http://192.168.13.6:3128 && npm config set https-proxy http://192.168.13.6:3128

# install dependencies
#RUN apt-get update -y && apt-get install -y git

# change our application's nodejs dependencies:
WORKDIR /usr/src/app
COPY package.json /usr/src/app
COPY package-lock.json /usr/src/app
RUN npm install --production

COPY . /usr/src/app


#ecosystem file ecosystem.json
CMD ["pm2-docker", "start", "ecosystem.docker.json"]