"use strict";

exports.excludes = {
	user_merchant: [
		'password'
	],
	test: [
		'id'
	],
	'test.mongo': [
		'id'
	],
	'log.mongo': [
		'_id'
	]
};

exports.rawQueris = function (model, req) {
	switch (model) {
		case 'check_merchant': 
			return { 
				query: "select * from btn_pay.`merchant` where id = :merchant_id",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id
					}
				}
			}
			break;

		case 'check_merchant_by_code': 
			return { 
				query: "select * from btn_pay.`merchant` where code = :merchant_code",
				bind: {
					replacements: {
						merchant_code: req.body.merchant_code
					}
				}
			}
			break;

		case 'get_user_merchant': 
			return {
				query: "select * from user_merchant where merchant_id = :merchant_id",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id || ''
					}
				}
			}
			break;

		case 'get_merchant_merchant_deactive': 
			return {
				query: "select b.`active_status` as status_user_merchant, a.`status`, b.`password` from merchant a left join user_merchant b on a.`code` = b.`username` where a.`id` = :merchant_id limit 1",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id || ''
					}
				}
			}
			break;

		case 'get_merchant_by_user': 
			return { 
				query: "select a.*, b.`bank_account_name`, b.`bank_account_number`, b.`email`, b.`account_number` from btn_pay.`user_merchant` as a join btn_pay.`merchant` as b on a.`username` = b.`code` where a.`username` = :username",
				bind: {
					replacements: {
						username: req.body.username || ''
					}
				}
			}
			break;

		case 'check_merchant_pan_merchant_id_acquirer': 
			return { 
				query: "select b.`account_number` as merchant_va, b.`id` as id_merchant, c.`account_number` as code, a.*, b.`account_number`, d.`device_key`, d.`device_type`, b.`email`,  c.`biller_name`, b.`name` as merchant_name from merchant_acquirer a left join merchant b on a.`merchant_id` = b.`id` left join `transactions_merchant` c on b.`code` = c.`account_number` join `user_merchant` as d on b.`code` = d.`username` where a.`merchant_pan_format` = :merchant_pan and a.`merchant_id_acquirer` = :merchant_id_acquirer limit 1",
				bind: {
					replacements: {
						merchant_pan: req.body.merchant_pan || '',
						merchant_id_acquirer: req.body.merchant_id_acquirer || ''
					}
				}
			}
			break;

		case 'check_max_merchant_acquier': 
			return { 
				query: "select merchant_id, merchant_id_acquirer from merchant_acquirer where merchant_id = :merchant_id and status = 1",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id || ''
					}
				}
			}
			break;

		case 'get_log_transactions_merchant': 
			return { 
				query: "select a.*, b.`account_number`, b.`code`, b.`name` as merchant_name from transaction_merchant_log a join merchant b on a.`merchant_name` = b.`name` where reference_id = :reference_id limit 1",
				bind: {
					replacements: {
						reference_id: req.body.reference_id || ''
					}
				}
			}
			break;

		case 'check_merchant_id_acquirer': 
			return { 
				query: "select * from merchant_acquirer where merchant_id_acquirer = :merchant_id_acquirer",
				bind: {
					replacements: {
						merchant_id_acquirer: req.body.merchant_id_acquirer || ''
					}
				}
			}
			break;

		case 'update_payment_at_transaction': 
			return { 
				query: "update transactions_merchant set payment_at = :payment_at where id = :transaction_id",
				bind: {
					replacements: {
						payment_at: req.body.payment_at || '',
						transaction_id: req.body.transaction_id || ''
					}
				}
			}
			break;

		case 'check_merchant_category': 
			return { 
				query: "select * from btn_pay.`merchant_category` where id = :merchant_category_id",
				bind: {
					replacements: {
						merchant_category_id: req.body.merchant_category_id
					}
				}
			}
			break;

		case 'check_merchant_criteria': 
			return { 
				query: "select * from btn_pay.`merchant_criteria` where id = :merchant_criteria_id",
				bind: {
					replacements: {
						merchant_criteria_id: req.body.merchant_criteria_id
					}
				}
			}
			break;

		case 'check_bank': 
			return { 
				query: "select * from btn_pay.`bank_code` where id = :bank_id",
				bind: {
					replacements: {
						bank_id: req.body.bank_id
					}
				}
			}
			break;

		case 'check_bank_id': 
			return { 
				query: "select * from btn_pay.`bank_code` where id = :bank_id",
				bind: {
					replacements: {
						bank_id: req.body.bank_id
					}
				}
			}
			break;

		case 'check_bank_name': 
			return { 
				query: "select * from btn_pay.`bank_code` where bank_name = :bank_name",
				bind: {
					replacements: {
						bank_name: req.body.bank_name
					}
				}
			}
			break;

		case 'check_bank_id_name': 
			return { 
				query: "select * from btn_pay.`bank_code` where id = :bank_id and bank_name = :bank_name",
				bind: {
					replacements: {
						bank_id: req.body.bank_id,
						bank_name: req.body.bank_name
					}
				}
			}
			break;

		case 'get_bank': 
			return { 
				query: "select * from btn_pay.`bank_code`",
				bind: {
					replacements: {}
				}
			}
			break;

		case 'check_merchant_acquirer': 
			return { 
				query: "select * from btn_pay.`merchant_acquirer` where bank_id = :bank_id and merchant_id = :merchant_id and status = 1",
				bind: {
					replacements: {
						bank_id: req.body.bank_id,
						merchant_id: req.body.merchant_id
					}
				}
			}
			break;

		case 'check_merchant_acquirer_code': 
			return { 
				query: "select * from btn_pay.`merchant_acquirer` where merchant_id_acquirer = :merchant_id_acquirer and status = 1",
				bind: {
					replacements: {
						merchant_id_acquirer: req.body.merchant_id_acquirer
					}
				}
			}
			break;

		case 'inquiry_merchant': 
			return { 
				query: "select m.name as merchant_name, m.city as merchant_city, m.zipcode, m.nmid, mc.name, bc.global_unique_identifier, mr.merchant_pan, mr.merchant_id_acquirer, mc.name, mct.`merchant_category`, mcc.`name` as merchant_criteria_name from merchant as m right join merchant_acquirer as mr on m.`id` = mr.`merchant_id` left join merchant_criteria as mc on mr.merchant_criteria_id = mc.id left join merchant_criteria as mcc on m.merchant_criteria_id = mcc.id left join bank_code as bc on m.bank_code_id = bc.id left join merchant_category mct on m.merchant_category_id = mct.id where mr.merchant_id = :merchant_id",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id
					}
				}
			}
			break;

		case 'inquiry_merchant_arr': 
			return { 
				query: "select bc.`global_unique_identifier` as globalUniqueIdentifier, mr.`merchant_pan_format` as merchantPan, mr.`merchant_id_acquirer` as merchantId, mc.`code` as merchantCriteria from merchant_acquirer as mr join bank_code as bc on mr.bank_id = bc.id join merchant_criteria as mc on mr.merchant_criteria_id = mc.id where mr.`merchant_id` = :merchant_id and status = 1",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id
					}
				}
			}
			break;

		case 'inquiry_merchant_master': 
			return { 
				query: "select bc.`global_unique_identifier` as globalUniqueIdentifier, m.`nmid` as nmid, mc.`code` as merchantCriteria, mcc.`mcc` as merchantCategoryCode, m.`name` as merchantName, m.`city` as merchantCity, m.`zipcode` as postalCode from merchant as m join bank_code as bc on m.bank_code_id = bc.bank_code join merchant_criteria as mc on m.merchant_criteria_id = mc.id join merchant_category as mcc on m.merchant_category_id = mcc.id where m.`id` = :merchant_id",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id
					}
				}
			}
			break;

		case 'check_global_unique_identifier': 
			return { 
				query: "select * from bank_code where bank_name = 'BANK BTN' and bank_code = '200'",
				bind: {
					replacements: {}
				}
			}
			break;

		case 'get_merchant_id': 
			return { 
				query: "select id as merchant_id from merchant where name = :merchant_name and nmid = :nmid",
				bind: {
					replacements: {
						merchant_name: req.body.merchant_name,
						nmid: req.body.nmid
					}
				}
			}
			break;

		case 'check_reff_amount': 
			return { 
				query: "select * from btn_pay.`transactions_merchant` where requestId = :reference_id",
				bind: {
					replacements: {
						reference_id: req.body.reference_id || ''
					}
				}
			}
			break;

		case 'get_merchant_id_acquirer': 
			return { 
				query: "select * from btn_pay.`merchant_acquirer` where merchant_id_acquirer = :merchant_id limit 1",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id
					}
				}
			}
			break;

		case 'get_transaction_merchant_log': 
			return { 
				query: "select * from btn_pay.`transaction_merchant_log` where reference_label = :reference_label and status = 1 limit 1",
				bind: {
					replacements: {
						reference_label: req.body.reference_label || ''
					}
				}
			}
			break;

		case 'transaction_merchant_log':
			return { 
				query: "select a.`id` as transaction_merchant_id, a.`created_at` as date_merchant, a.`requestId`, b.* from `transactions_merchant` as a left join `transaction_merchant_log` as b on a.`requestId` = b.`reference_label` where a.`id` >= :first and a.`id` <= :last and b.`status` = 1 order by transaction_merchant_id desc",
				bind: {
					replacements: {
						first: req.body.first,
						last: req.body.last
					}
				}
			}

		case 'transaction_merchant_status':
			return { 
				query: "select status from transactions_merchant where requestId = :request_id",
				bind: {
					replacements: {
						request_id: req.body.request_id
					}
				}
			}

		case 'transaction_merchant':
			return { 
				query: "select * from transactions_merchant where requestId = :request_id",
				bind: {
					replacements: {
						request_id: req.body.request_id
					}
				}
			}

		case "get_permission_residence":
			return {
				query:
				"SELECT rp.role_id, rp.permission_id, p.permission_name FROM btn_smart_residence.role_permission rp JOIN btn_smart_residence.permission p ON rp.permission_id = p.id where rp.role_id = :role_id ORDER BY p.sequence",
				bind: {
				replacements: {
					role_id: req.body.role_id
				},
				nest : true
				},
			};

		default: 
			return {}
	}
}

exports.storePro = function (model, req) {
	switch(model) {
		case 'registration':
			return {
				query: "CALL btn_pay.registration_merchant($code, $name, $business_type_id, $owner, $msisdn, $email, $phone, $address, $kelurahan, $kecamatan, $city, $province, $zipcode, $lat, $long, $account_name, $account_number, $ip_address)",
				bind: {
					bind: {
						code: req.body.code,
						name: req.body.name,
						business_type_id: req.body.business_type_id,
						owner: req.body.owner,
						msisdn: req.body.msisdn,
						email: req.body.email,
						phone: req.body.phone,
						address: req.body.address,
						kelurahan: req.body.kelurahan,
						kecamatan: req.body.kecamatan,
						city: req.body.city,
						zipcode: req.body.zipcode || null,
						province: req.body.province,
						lat: req.body.lat,
						long: req.body.long,
						account_name: req.body.account_name,
						account_number: req.body.account_number,
						ip_address: req.body.ip_address
					}
				}
			};
			break;

		case 'registration_user_merchant':
			return {
				query: "CALL btn_pay.registration_user_merchant($username, $msisdn, $password, $menu, $image, $action_by, $ip_address)",
				bind: {
					bind: {
						username: req.body.username,
						msisdn: req.body.msisdn,
						password: req.body.password,
						menu: req.body.menu,
						image: req.body.image,
						action_by: req.body.action_by,
						ip_address: req.body.ip_address
					}
				}
			};
			break;

		case 'getProfile':
			return {
				query: "CALL btn_pay.get_merchant($code)",
				bind: {
					bind: {
						code: req.body.code
					}
				}
			};
			break;

		case 'updateProfile':
			return {
				query: "CALL btn_pay.update_merchant($code, $name, $business_type_id, $owner, $msisdn, $email, $phone, $address, $kelurahan, $kecamatan, $city, $province, $action_by, $ip_address, $image, $banner, $open_hours, $closed_hours, $description, $merchant_category_id, $merchant_criteria_id, $nmid, $status, $bank_acc_num, $bank_acc_name, $image_nik, $image_npwp, $image_merchant, $image_pic, $ktp, $npwp, $omzet, $approval_by)",
				bind: {
					bind: {
						code: req.body.code,
						name: req.body.name || null,
						business_type_id: req.body.business_type_id || null,
						owner: req.body.owner || null,
						msisdn: req.body.msisdn || null,
						email: req.body.email || null,
						phone: req.body.phone || null,
						address: req.body.address || null,
						kelurahan: req.body.kelurahan || null,
						kecamatan: req.body.kecamatan || null,
						city: req.body.city || null,
						province: req.body.province || null,
						action_by: req.body.action_by || null,
						ip_address: req.body.ip_address || null,
						image: req.body.image || null,
						banner: req.body.banner || null,
						open_hours: req.body.open_hours || null,
						closed_hours: req.body.closed_hours || null,
						description: req.body.description || null,
						merchant_category_id: req.body.merchant_category_id || null,
						merchant_criteria_id: req.body.merchant_criteria_id || null,
						nmid: req.body.nmid || null,
						status: req.body.status || null, // 0 = deactive ; 1 = active ; 2 = reject ; 3 = banned; 4 = process; 5 = review
						bank_acc_num: req.body.bank_acc_num || null,
						bank_acc_name: req.body.bank_acc_name || null,
						image_nik: req.body.image_nik || null,
						image_npwp: req.body.image_npwp || null,
						image_pic: req.body.image_pic || null,
						image_merchant: req.body.image_merchant || null,
						ktp: req.body.ktp || null,
						npwp: req.body.npwp || null,
						omzet: req.body.omzet || null,
						approval_by: req.body.approval_by || null
					}
				}
			};
			break;

		case 'insert_transaction':
			return {
				query: "CALL btn_pay.create_transactions_merchant($requestId, $account_number, $merchant_va, $wallet_type_code, $transactions_type_code, $biller_type, $biller_code, $biller_name, $nominal, $no_jurnal, $note, $fee, $admin_fee, $data, $status, $action_by, $ip_address)",
				bind: {
					bind: {
						requestId: req.body.requestId || '',
						account_number: req.body.account_number || '',
						merchant_va: req.body.merchant_va || '',
						wallet_type_code: req.body.wallet_type_code || '',
						transactions_type_code: req.body.transactions_type_code || '',
						biller_type: req.body.biller_type || '',
						biller_code: req.body.biller_code || null,
						biller_name: req.body.biller_name || null,
						nominal: req.body.nominal || '0',
						no_jurnal: req.body.no_jurnal || '',
						note: req.body.note || '',
						fee: req.body.fee || '0',
						admin_fee: req.body.admin_fee || '0',
						data: req.body.data || '',
						status: req.body.status || '0',
						action_by: req.body.action_by || '0',
						ip_address: req.body.ip_address || ''
					}
				}
			};
			break;

		case 'create_product':
			return {
				query: "CALL btn_pay.create_product(:merchant_code, :product_type_id, :name, :narration, :price, :quantity, :promo_status, :active_status, :image, :action_by, :ip_address, :recommended )",
				bind: {
					replacements: {
						merchant_code: req.body.merchant_code || '',
						product_type_id: req.body.product_type_id || '',
						name: req.body.name || '',
						narration: req.body.narration || '',
						price: req.body.price || '',
						quantity: req.body.quantity || '',
						promo_status: req.body.promo_status || '',
						active_status: req.body.active_status || '',
						image: req.body.image || '',
						action_by: req.body.action_by || '1',
						ip_address: req.body.ip_address || '',
						recommended: req.body.recommended || 2
					}
				}
			};
			break;

		case 'create_product_type':
			return {
				query: "CALL btn_pay.create_product_type($merchant_code, $name, $action_by, $ip_address)",
				bind: {
					bind: {
						merchant_code: req.body.merchant_code || '',
						name: req.body.name || '',
						action_by: req.body.action_by || '1',
						ip_address: req.body.ip_address || ''
					}
				}
			};
			break;

		case 'get_product':
			return {
				query: "CALL btn_pay.get_product(:merchant_code, :search, :status, :product_id, :product_type, :order, :offset, :limit)",
				bind: {
					replacements: {
						merchant_code 	: req.body.merchant_code || null,
						search 			: req.body.search || null,
						status 			: req.body.status || null,
						product_type 	: req.body.product_type || 0,
						product_id 		: req.body.product_id || 0,
						order 			: req.body.order || 0,
						limit 			: req.body.limit || 0,
						offset  		: req.body.offset || 0
					}
				}
			};
			break;

		case 'get_product_count':
			return {
				query: "CALL `btn_pay`.`get_product_count`(:merchant_code, :search, :status, :product_id, :product_type)",
				bind: {
					replacements: {
						merchant_code 	: req.body.merchant_code || null,
						search 			: req.body.search || null,
						product_id 		: req.body.product_id || 0,
						product_type 	: req.body.product_type || 0,
						status 			: req.body.status || null
					}
				}
			};
			break;

		case 'update_transaction_merchant_log':
			return {
				query: "CALL `btn_pay`.`update_transaction_log`(:transaction_log_id, :transaction_merchant_id, :reference_label)",
				bind: {
					replacements: {
						transaction_log_id: req.body.transaction_log_id || null,
						transaction_merchant_id: req.body.transaction_merchant_id || null,
						reference_label: req.body.reference_label || null
					}
				}
			};
			break;

		case 'get_product_type':
			return {
				query: "CALL btn_pay.get_product_type($merchant_code)",
				bind: {
					bind: {
						merchant_code: req.body.merchant_code || ''
					}
				}
			};
			break;

		case 'update_product':
			return {
				query: "CALL btn_pay.update_product(:id, :merchant_code, :product_type_id, :name, :narration, :price, :quantity, :promo_status, :active_status, :image, :recommended)",
				bind: {
					replacements: {
						id: req.body.id || null,
						merchant_code: req.body.merchant_code || null,
						product_type_id: req.body.product_type_id || null,
						name: req.body.name || null,
						narration: req.body.narration || null,
						price: req.body.price || null,
						quantity: req.body.quantity || null,
						promo_status: req.body.promo_status || null,
						active_status: req.body.active_status || null,
						image: req.body.image || null,
						recommended: req.body.recommended || null
					}
				}
			};
			break;

		case 'update_product_type':
			return {
				query: "CALL btn_pay.update_product_type($id, $merchant_code, $name)",
				bind: {
					bind: {
						id: req.body.id || null,
						merchant_code: req.body.merchant_code || null,
						name: req.body.name || null
					}
				}
			};
			break;

		case 'delete_product':
			return {
				query: "CALL btn_pay.delete_product($id, $merchant_code)",
				bind: {
					bind: {
						id: req.body.id || '',
						merchant_code: req.body.merchant_code || ''
					}
				}
			};
			break;

		case 'get_transaction':
			return {
				query: "CALL btn_pay.r_transactions_merchant(:merchant_code, :transactions_type_code, :status, :start_date, :end_date, :search, :offset, :limit, :order)",
				bind: {
					replacements: {
						merchant_code: req.body.merchant_code || '0',
						transactions_type_code: req.body.transactions_type_code || '0',
						status: req.body.status || null,
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99',
						limit: req.body.limit || 0,
						offset: req.body.offset || 0,
						order: req.body.order || 0,
						search: req.body.search || null
					}
				}
			};
			break;

		case 'get_transaction_count':
			return {
				query: "CALL btn_pay.r_transactions_merchant_count(:merchant_code, :transactions_type_code, :status, :start_date, :end_date, :search)",
				bind: {
					replacements: {
						merchant_code: req.body.merchant_code || '0',
						transactions_type_code: req.body.transactions_type_code || '0',
						status: req.body.status || null,
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99',
						search: req.body.search || null
					}
				}
			};
			break;

		case 'payment_log_insert':
			return {
				query: "CALL btn_pay.create_transaction_log(:reference_id, :request_datetime, :transaction_date, :transaction_time, :acquirer_name, :merchant_name, :merchant_city, :merchant_pan, :merchant_id, :terminal_id, :issuer_name, :customer_name, :customer_pan, :system_trace_audit_number, :retrieval_reference_number, :transaction_amount, :bill_number, :mobile_number, :store_label, :loyalty_number, :reference_label, :customer_label, :terminal_label, :purpose_of_transaction, :additional_customer_data_request, :proprietary_data, :status)",
				bind: {
					replacements: {
						reference_id: req.body.reference_id || null,
						request_datetime: req.body.request_datetime || null,
						transaction_date: req.body.transaction_date || null,
						transaction_time: req.body.transaction_time || null,
						acquirer_name: req.body.acquirer_name || null,
						merchant_name: req.body.merchant_name || null,
						merchant_city: req.body.merchant_city || null,
						merchant_pan: req.body.merchant_pan || null,
						merchant_id: req.body.merchant_id || null,
						terminal_id: req.body.terminal_id || null,
						issuer_name: req.body.issuer_name || null,
						customer_name: req.body.customer_name || null,
						customer_pan: req.body.customer_pan || null,
						system_trace_audit_number: req.body.system_trace_audit_number || null,
						retrieval_reference_number: req.body.retrieval_reference_number || null,
						transaction_amount: req.body.transaction_amount || null,
						bill_number: req.body.bill_number || null,
						mobile_number: req.body.mobile_number || null,
						store_label: req.body.store_label || null,
						loyalty_number: req.body.loyalty_number || null,
						reference_label: req.body.reference_label || null,
						customer_label: req.body.customer_label || null,
						terminal_label: req.body.terminal_label || null,
						purpose_of_transaction: req.body.purpose_of_transaction || null,
						additional_customer_data_request: req.body.additional_customer_data_request || null,
						proprietary_data: req.body.proprietary_data || null,
						status: req.body.status || null
					}
				}
			};
			break;

		case 'get_payment_log':
			return {
				query: "CALL btn_pay.get_transaction_log(:reference_id, :offset, :limit)",
				bind: {
					replacements: {
						reference_id: req.body.reference_id || null,
						offset: req.body.offset || 0,
						limit: req.body.limit || 0
					}
				}
			};
			break;

		case 'update_payment':
			return {
				query: "CALL btn_pay.transactions_merchant_payment(:requestId, :va, :invoice, :ip_address)",
				bind: {
					replacements: {
						requestId: req.body.requestId || null,
						va: req.body.va || null,
						invoice: req.body.invoice || null,
						ip_address: req.body.ip_address || null
					}
				}
			};
			break;

		case 'merchant_invoice_check':
			return {
				query: "CALL btn_pay.merchant_invoice_check($invoice)",
				bind: {
					bind: {
						invoice: req.body.invoice || ''
					}
				}
			};
			break;

		case 'get_invoice':
			return {
				query: "CALL `btn_pay`.`get_merchant_emoney`(:id_merchant)",
				bind: {
					replacements: {
						id_merchant: req.body.id_merchant || null
					}
				}
			};
			break;

		case 'merchant_login':
			return {
				query: "CALL `btn_pay`.`merchant_login`(:username, :password, :type)",
				bind: {
					replacements: {
						username: req.body.username || '',
						password: req.body.password || '',
						type: req.body.type || null
					}
				}
			};
			break;

		case 'get_transaction_all':
			return {
				query: "CALL `btn_pay`.`r_transactions_merchant_all`($transactions_type_code, $start_date, $end_date, $offset, $limit, $order)",
				bind: {
					bind: {
						transactions_type_code: req.body.transactions_type_code || '0',
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99',
						limit: req.body.limit || 0,
						offset: req.body.offset || 0,
						order: req.body.order || 0
					}
				}
			};
			break;

		case 'get_transaction_all_count':
			return {
				query: "CALL `btn_pay`.`r_transactions_merchant_all_count`($start_date, $end_date)",
				bind: {
					bind: {
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99'
					}
				}
			};
			break;

		case 'transaction_chart':
			return {
				query: "CALL `btn_pay`.`r_chart_transactions_merchant`($merchant_code, $start_date, $end_date, $offset, $limit, $order)",
				bind: {
					bind: {
						merchant_code: req.body.merchant_code || null,
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99',
						limit: req.body.limit || 0,
						offset: req.body.offset || 0,
						order: req.body.order || 0
					}
				}
			};
			break;

		case 'total_income':
			return {
				query: "CALL `btn_pay`.`r_sum_transactions_merchant`($mode, $merchant_code, $start_date, $end_date)",
				bind: {
					bind: {
						mode: req.body.mode || null,
						merchant_code: req.body.merchant_code || null,
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99'
					}
				}
			};
			break;

		case 'top_product':
			return {
				query: "CALL `btn_pay`.`r_top_product`($mode, $merchant_code, $start_date, $end_date, $offset, $limit, $order)",
				bind: {
					bind: {
						mode: req.body.mode || null,
						merchant_code: req.body.merchant_code || null,
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99',
						limit: req.body.limit || 0,
						offset: req.body.offset || 0,
						order: req.body.order || 0
					}
				}
			};
			break;

		case 'insert_withdraw':
			return {
				query: "CALL `btn_pay`.`create_withdrawal_merchant`($requestId, $account_number, $merchant_va, $wallet_type_code, $transactions_type_code, $biller_type, $biller_code, $biller_name, $nominal, $no_jurnal, $note, $fee, $admin_fee, $status, $action_by, $ip_address)",
				bind: {
					bind: {
						requestId: req.body.requestId || '',
						account_number: req.body.account_number || '',
						merchant_va: req.body.merchant_va || '',
						wallet_type_code: req.body.wallet_type_code || '',
						transactions_type_code: req.body.transactions_type_code || '',
						biller_type: req.body.biller_type || '',
						biller_code: req.body.biller_code || null,
						biller_name: req.body.biller_name || null,
						nominal: req.body.nominal || '0',
						no_jurnal: req.body.no_jurnal || '',
						note: req.body.note || '',
						fee: req.body.fee || '0',
						admin_fee: req.body.admin_fee || '0',
						status: req.body.status || '0',
						action_by: req.body.action_by || '0',
						ip_address: req.body.ip_address || ''
					}
				}
			};
			break;

		case 'get_withdraw':
			return {
				query: "CALL `btn_pay`.`r_withdrawal_merchant`(:account_number, :start_date, :end_date, :search, :offset, :limit, :order)",
				bind: {
					replacements: {
						search: req.body.search || null,
						account_number: req.body.merchant_code || '0',
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99',
						limit: req.body.limit || 0,
						offset: req.body.offset || 0,
						order: req.body.order || 0
					}
				}
			};
			break;
		case 'get_withdraw_count':
			return {
				query: "CALL `btn_pay`.`r_withdrawal_merchant_count`(:account_number, :start_date, :end_date, :search)",
				bind: {
					replacements: {
						account_number: req.body.merchant_code || '0',
						search: req.body.search || null,
						account_number: req.body.merchant_code || '0',
						start_date: req.body.start_date || '0000-00-00',
						end_date: req.body.end_date || '9999-99-99'
					}
				}
			};
			break;
		case 'check_invoice':
			return {
				query: "CALL `btn_pay`.`get_transactions_merchant`($invoice, $request_id)",
				bind: {
					bind: {
						invoice: req.body.invoice || null,
						request_id: req.body.request_id || null
					}
				}
			};
			break;

		case 'get_all_merchant':
			return {
				query: "CALL `btn_pay`.`get_all_merchant`(:status, :offset, :limit, :order, :search, :app, :type, :lat, :long, :id_merchant)",
				bind: {
					replacements: {
						search 	: req.body.search || null,
						status 	: req.body.status || null,
						app 	: req.body.app || null,
						offset	: req.body.offset || 0,
						limit 	: req.body.limit || 0,
						order 	: req.body.order || 0,
						type 	: req.body.type || null,
						lat 	: req.body.lat || null,
						long 	: req.body.long || null,
						id_merchant 	: req.body.id_merchant || null
					}
				}
			};
			break;

		case 'get_all_merchant_count':
			return {
				query: "CALL `btn_pay`.`get_all_merchant_count`(:status, :search, :type, :lat, :long)",
				bind: {
					replacements: {
						search: req.body.search || null,
						status: req.body.status || null,
						type: req.body.type || null,
						lat: req.body.lat || null,
						long: req.body.long || null
					}
				}
			};
			break;

		case 'merchant_stats':
			return {
				query: "CALL `btn_pay`.`merchant_stats`()",
				bind: {
					bind: {
					}
				}
			};
			break;

		case 'get_merchant_detail_statistic':
			return {
				query: "CALL `btn_pay`.`get_merchant_detail_statistic`($merchant_code)",
				bind: {
					bind: {
						merchant_code: req.body.merchant_code || null,
					}
				}
			};
			break;

		case 'get_setting_promo':
			return {
				query: "CALL btn_pay.get_setting_promo(:status, :setting_promo_id, :merchant_id, :start_date, :end_date, :offset, :limit, :order, :search)",
				bind: {
					replacements: {
						status: req.body.status || null,
						setting_promo_id: req.body.setting_promo_id || null,
						merchant_id: req.body.merchant_id || null,
						start_date: req.body.start_date || 0,
						end_date: req.body.end_date || 0,
						offset: req.body.offset || 0,
						limit: req.body.limit || 0,
						order: req.body.order || 0,
						search: req.body.search || null
					}
				}
			};
			break;

		case 'get_setting_promo_count':
			return {
				query: "CALL btn_pay.get_setting_promo_count(:status, :setting_promo_id, :merchant_id, :start_date, :end_date, :search)",
				bind: {
					replacements: {
						status: req.body.status || null,
						setting_promo_id: req.body.setting_promo_id || null,
						merchant_id: req.body.merchant_id || null,
						start_date: req.body.start_date || 0,
						end_date: req.body.end_date || 0,
						search: req.body.search || null
					}
				}
			};
			break;

		case 'update_setting_promo':
			return {
				query: "CALL btn_pay.update_setting_promo($setting_promo_id, $merchant_id, $status, $approved_by, $narration, $ip_address)",
				bind: {
					bind: {
						setting_promo_id: req.body.setting_promo_id || null,
						merchant_id: req.body.merchant_id || null,
						status: req.body.status || null,
						approved_by: req.body.approved_by || null,
						narration: req.body.narration || null,
						ip_address: req.body.ip_address || null
					}
				}
			};
			break;

		case 'create_setting_promo':
			return {
				query: "CALL btn_pay.create_setting_promo(:merchant_id, :promo_name, :description, :min_nominal, :point_reward, :max_transaction, :start_promo, :end_promo, :action_by, :ip_address, :image_promo)",
				bind: {
					replacements: {
						merchant_id: req.body.merchant_id || null,
						promo_name: req.body.promo_name || null,
						description: req.body.description || null,
						min_nominal: req.body.min_nominal || null,
						point_reward: req.body.point_reward || null,
						max_transaction: req.body.max_transaction || null,
						start_promo: req.body.start_promo || null,
						end_promo: req.body.end_promo || null,
						image_promo: req.body.image_promo || null,
						action_by: req.body.action_by || null,
						ip_address: req.body.ip_address || null
					}
				}
			};
			break;

		case 'disable_promo':
			return {
				query: "UPDATE setting_promo SET status = '5' WHERE id = $setting_promo_id AND merchant_id = $merchant_id",
				bind: {
					bind: {
						setting_promo_id: req.body.setting_promo_id || '0',
						merchant_id: req.body.merchant_id || '0'
					}
				}
			};
			break;

		case 'create_customer_merchant_budget':
			return {
				query: "CALL btn_pay.create_customer_merchant_budget($merchant_id, $emoney_va, $refference_id, $operation, $update_point, $source, $destination, $description_req, $action_by, $ip_address)",
				bind: {
					bind: {
						merchant_id: req.body.merchant_id || null,
						emoney_va: req.body.emoney_va || null,
						refference_id: req.body.refference_id || null,
						operation: req.body.operation || '0',
						update_point: req.body.update_point || null,
						source: req.body.source || null,
						destination: req.body.destination || null,
						description_req: req.body.description_req || null,
						action_by: req.body.action_by || null,
						ip_address: req.body.ip_address || null
					}
				}
			};
			break;
	}
};

exports.insert = function (model, req, models) {
	var query = {};

	switch (model) {
		// Sequelize (MySql) Example.
		case 'test':
			query = {
				name: req.body.name,
				value: req.body.value
			};

			return query;
			break;

		case 'transaction_merchant_detail':
			query = {
				transactions_merchant_id: req.body.transactions_merchant_id,
				product_id: req.body.product_id,
				quantity: req.body.quantity,
				name: req.body.name,
				price: req.body.price,
				total: req.body.total,
				status: req.body.status,
				action_by: req.body.action_by,
				ip_address: req.body.ip_address
			};

			return query;
			break;

		case 'notification':
			query = {
				referrer_id: req.body.referrer_id,
				account_number: req.body.account_number,
				merchant_va: req.body.merchant_va,
				type: req.body.type,
				title: req.body.title,
				content: req.body.content,
				read: req.body.read,
				action_by: req.body.action_by,
				ip_address: req.body.ip_address
			};

			return query;
			break;

		case 'user_merchant':
			query = {
				merchant_id: req.body.merchant_id,
				type: req.body.type,
				username: req.body.username,
				name: req.body.name,
				msisdn: req.body.msisdn,
				password: req.body.password,
				menu: req.body.menu,
				active_status: req.body.active_status,
				image: req.body.image,
				action_by: req.body.action_by,
				ip_address: req.body.ip_address
			};

			return query;
			break;

		// Mongoose (Mongo) Example.
		case 'test.mongo':
			query = {
				name: req.body.name,
				value: req.body.value
			};

			return query;
			break;

		case 'log.mongo':
			query = {
				endpoint: req.body.endpoint,
				request: req.body.request,
				response: req.body.response,
				status: req.body.status,
				date: req.body.date,
				time: req.body.time
			};

			return query;
			break;

		default:
			return {};
			break;
	}
};

exports.select = function (model, req, models) {
	var query = {}
	query.where = {};
	query.limit = req.body.take ? Number(req.body.take) : 10;
	query.offset = req.body.skip ? Number(req.body.skip) : 0;
	query.order = [];
	query.attributes = {
		exclude: module.exports.excludes[model]
	};

	if (req.body.orderBy && !req.body.sort) query.order = [[req.body.orderBy, 'DESC']];
	if (!req.body.orderBy && req.body.sort) query.order = [['id', req.body.sort]];
	if (req.body.orderBy && req.body.sort) query.order = [[req.body.orderBy, req.body.sort]];
	if (req.body.id) query.where.id = req.body.id;
	if (req.body.groupBy) query.group = req.body.groupBy;

	switch (model) {
		// Sequelize (MySql) Example.
		case 'test':
			if (req.body.name) query.where.name = req.body.name;
			if (req.body.value) query.where.value = req.body.value;

			return query;
			break;

		case 'merchant':
			if (req.body.msisdn) query.where.msisdn = req.body.msisdn;
			if (req.body.code) query.where.code = req.body.code;
			if (req.body.account_number) query.where.account_number = req.body.account_number;
			if (req.body.status) query.where.status = req.body.status;

			return query;
			break;

		case 'user_merchant':
			if (req.body.msisdn) query.where.msisdn = req.body.msisdn;
			if (req.body.username) query.where.username = req.body.username;
			if (req.body.merchant_id) query.where.merchant_id = req.body.merchant_id;
			if (req.body.active_status) query.where.active_status = req.body.active_status;

			return query;
			break;

		case 'merchant_transaction':
			if (req.body.account_number) query.where.account_number = req.body.account_number;
			if (req.body.biller_code) query.where.biller_code = req.body.biller_code;

			return query;
			break;

		case 'merchant_transaction_detail':
			if (req.body.transactions_merchant_id) query.where.transactions_merchant_id = req.body.transactions_merchant_id;


			return query;
			break;

		case 'business_type':
			if (req.body.id) query.where.id = req.body.id;
			if (req.body.status) query.where.status = req.body.status;

			return query;
			break;

		// Mongoose (Mongo) Example.
		case 'test.mongo':
			query = {}; // Overwrite the 'query' variable.

			if (req.body._id) query._id = req.body._id;
			if (req.body.name) query.name = req.body.name;
			if (req.body.value) query.value = req.body.value;

			return query;
			break;

		case 'log.mongo':
			query = {}; // Overwrite the 'query' variable.

			if (req.body._id) query._id = req.body._id;
			if (req.body.endpoint) query.endpoint = req.body.endpoint;
			if (req.body.request) query.request = req.body.request;
			if (req.body.response) query.response = req.body.response;
			if (req.body.status) query.status = req.body.status;
			if (req.body.date) query.date = req.body.date;
			if (req.body.time) query.time = req.body.time;

			return query;
			break;

		default:
			return query;
			break;
	}
};

exports.update = function (model, req, models) {
	var params = {};
	params.dataUpdate = {};
	params.dataQuery = {};
	params.dataQuery.where = {};

	if (req.body.dataQuery.id) params.dataQuery.where.id = req.body.dataQuery.id;

	switch (model) {
		case 'test':
			if (req.body.dataQuery.name) params.dataQuery.where.name = req.body.dataQuery.name;

			if (req.body.dataUpdate.name) params.dataUpdate.name = req.body.dataUpdate.name;

			return params;
			break;

		case 'merchant':
			if (req.body.dataQuery.code) params.dataQuery.where.code = req.body.dataQuery.code;
			if (req.body.dataQuery.status) params.dataQuery.status = req.body.dataQuery.status;


			if (req.body.dataUpdate.status) params.dataUpdate.status = req.body.dataUpdate.status;
			if (req.body.dataUpdate.note) params.dataUpdate.note = req.body.dataUpdate.note;
			if (req.body.dataUpdate.approval_by) params.dataUpdate.approval_by = req.body.dataUpdate.approval_by;
			if (req.body.dataUpdate.approval_at) params.dataUpdate.approval_at = req.body.dataUpdate.approval_at;

			return params;
			break;
		case 'merchant_approval':
			params.dataQuery.where.code = req.body.dataQuery.code;
			params.dataQuery.where.status = req.body.dataQuery.status;

			params.dataUpdate.status = req.body.dataUpdate.status;
			params.dataUpdate.approval_by = req.body.dataUpdate.approval_by;
			params.dataUpdate.approval_at = req.body.dataUpdate.approval_at;
			params.dataUpdate.account_number = req.body.dataUpdate.account_number;
			params.dataUpdate.bank_account_number = req.body.dataUpdate.bank_account_number;
			params.dataUpdate.bank_account_name = req.body.dataUpdate.bank_account_name;

			params.dataUpdate.image_nik = req.body.dataUpdate.image_nik;
			params.dataUpdate.image_merchant = req.body.dataUpdate.image_merchant;
			params.dataUpdate.image_npwp = req.body.dataUpdate.image_npwp;
			params.dataUpdate.image_merchant = req.body.dataUpdate.image_merchant;
			params.dataUpdate.image_pic = req.body.dataUpdate.image_pic;
			params.dataUpdate.ktp = req.body.dataUpdate.ktp;
			params.dataUpdate.npwp = req.body.dataUpdate.npwp;
			params.dataUpdate.omzet = req.body.dataUpdate.omzet;
			params.dataUpdate.nmid = req.body.dataUpdate.nmid;
			params.dataUpdate.merchant_criteria_id = req.body.dataUpdate.merchant_criteria_id;
			params.dataUpdate.merchant_category_id = req.body.dataUpdate.merchant_category_id;

			return params;
			break;
		case 'user_merchant':
			if (req.body.dataQuery.username) params.dataQuery.where.username = req.body.dataQuery.username;

			if (req.body.dataUpdate.type) params.dataUpdate.type = req.body.dataUpdate.type;
			if (req.body.dataUpdate.name) params.dataUpdate.name = req.body.dataUpdate.name;
			if (req.body.dataUpdate.menu) params.dataUpdate.menu = req.body.dataUpdate.menu;
			if (req.body.dataUpdate.image) params.dataUpdate.image = req.body.dataUpdate.image;
			if (req.body.dataUpdate.active_status) params.dataUpdate.active_status = req.body.dataUpdate.active_status;

			return params;
			break;

		case 'user_merchant_password':
			if (req.body.dataQuery.username) params.dataQuery.where.username = req.body.dataQuery.username;
			if (req.body.dataQuery.id) params.dataQuery.where.id = req.body.dataQuery.id;
			if (req.body.dataQuery.old_password) params.dataQuery.where.password = req.body.dataQuery.old_password;

			if (req.body.dataUpdate.password) params.dataUpdate.password = req.body.dataUpdate.password;

			return params;
			break;
		case 'user_merchant_password_approval':
			if (req.body.dataQuery.username) params.dataQuery.where.username = req.body.dataQuery.username;
			if (req.body.dataQuery.merchant_id) params.dataQuery.where.merchant_id = req.body.dataQuery.merchant_id;

			if (req.body.dataUpdate.password) params.dataUpdate.password = req.body.dataUpdate.password;
			if (req.body.dataUpdate.status) params.dataUpdate.active_status = req.body.dataUpdate.status;

			return params;
			break;

		case 'merchant_transaction':
			if (req.body.dataQuery.status) params.dataQuery.where.status = req.body.dataQuery.status;

			if (req.body.dataUpdate.note) params.dataUpdate.note = req.body.dataUpdate.note;
			if (req.body.dataUpdate.note_2) params.dataUpdate.note_2 = req.body.dataUpdate.note_2;
			if (req.body.dataUpdate.action_by) params.dataUpdate.action_by = req.body.dataUpdate.action_by;
			if (req.body.dataUpdate.merchant_va) params.dataUpdate.merchant_va = req.body.dataUpdate.merchant_va;
			if (req.body.dataUpdate.wallet_type_code) params.dataUpdate.wallet_type_code = req.body.dataUpdate.wallet_type_code;
			if (req.body.dataUpdate.status) params.dataUpdate.status = req.body.dataUpdate.status;
			if (req.body.dataUpdate.payment_at) params.dataUpdate.payment_at = req.body.dataUpdate.payment_at;
			if (req.body.dataUpdate.biller_name) params.dataUpdate.biller_name = req.body.dataUpdate.biller_name;
			if (req.body.dataUpdate.biller_code) params.dataUpdate.biller_code = req.body.dataUpdate.biller_code;
			if (req.body.dataUpdate.requestId) params.dataUpdate.requestId = req.body.dataUpdate.requestId;
			if (req.body.dataUpdate.no_jurnal) params.dataUpdate.no_jurnal = req.body.dataUpdate.no_jurnal;

			return params;
			break;

		case 'revoke_membership':
			if (req.body.dataQuery.status) params.dataQuery.where.status = req.body.dataQuery.status;
			if (req.body.dataQuery.id) params.dataQuery.where.id = req.body.dataQuery.id;
			if (req.body.dataQuery.merchant_id) params.dataQuery.where.merchant_id = req.body.dataQuery.merchant_id;
			if (req.body.dataQuery.emoney_va) params.dataQuery.where.emoney_va = req.body.dataQuery.emoney_va;

			params.dataUpdate.status = 0;
			if (req.body.dataUpdate.revoke_by) params.dataUpdate.revoke_by = req.body.dataUpdate.revoke_by;
			if (req.body.dataUpdate.revoke_at) params.dataUpdate.revoke_at = req.body.dataUpdate.revoke_at;

			return params;
			break;

		default:
			return params;
			break;
	}
};

exports.delete = function (model, req, models) {
	var query = {};
	query.where = {};

	if (req.body.id) query.where.id = req.body.id;

	switch (model) {
		// Sequelize (MySql) Example.
		case 'test':
			if (req.body.name) query.where.name = req.body.name;

			return query;
			break;

		case 'merchant_transaction':
			if (req.body.requestId) query.where.requestId = req.body.requestId;

			return query;
			break;

		// Mongoose (Mongo) Example.
		case 'test.mongo':
			query.where = undefined; // Overwrite the 'query' variable.

			if (req.body._id) query._id = req.body._id;

			return query;
			break;

		case 'log.mongo':
			query.where = undefined; // Overwrite the 'query' variable.

			if (req.body._id) query._id = req.body._id;
			if (req.body.date) query.date = req.body.date;
			if (req.body.time) query.time = req.body.time;

			return query;
			break;

		default:
			return query;
			break;
	}
};
