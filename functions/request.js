"use strict";

const unirest = require('unirest');

exports.emoney = function (path, body, callback) {
	let headers = {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
		'cache-control': 'no-cache'
	};
	let url = process.env.EMONEY_HOST + ':' + process.env.EMONEY_PORT + path;
	
	unirest.post(url).headers(headers).send({
		data: body
	}).end(function (response) {
		let output = {
	  	code: response.body.code,
	  	message: response.body.message,
	  	data: response.body.data,
		  from: process.env.EMONEY_NAME
	  };

	  if (response.error) return callback(output);

	  callback(null, output);
	});
};

exports.withdraw = function (body, callback) {
	let headers = {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
		'cache-control': 'no-cache'
	};
	let url = process.env.WITHDRAW;
	
	unirest.post(url).headers(headers).send(body).end(function (response) {
		let output = response.body
	  	if (response.error) return callback(response.error);
	  	callback(null, output);
	});
};

exports.post = function(url, key, callback) {
	unirest.post(url)
		.headers({ 'Accept': 'application/json', 'Content-Type': 'application/json', 'cache-control': 'no-cache' })
		.send(key)
		.end(function(response) {
			console.log('!!!!!!!!!!!!!!!!!!!---- start Fungsi HIT ----!!!!!!!!!!!!!!!!!!!')
			console.log(url)
			console.log(key)
			// console.log(response.error)
			console.log(response.body)
			console.log('!!!!!!!!!!!!!!!!!!!---- end Fungsi HIT ----!!!!!!!!!!!!!!!!!!!')
			if (response.error) {
				if(response.body) {
					response.body.status = response.statusCode || 500;
					return callback(response.body);
				} else {
					response.error.status = response.statusCode || 500;
					return callback(response.error);
				}
			} else {
				response.body.status = response.statusCode;
				return callback(null, response.body);
			}
		});
};

exports.notif = function(url, key, callback) {
	unirest.post(url)
		.headers({
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			"Authorization": "key=AAAAY20uDPY:APA91bFyOBKeyiNs3LpwB8Qsdz6BF1LKjj0CHj1VhWoKgcyDwIbGEMKjxsLN8xo9NmoxPvQvs4Ogd9oCcQyHOKZsvlWQnG_Y1l5WWCAxHNU4i9sXAdca7HhUtR1iCI_zV4EBSABh8yTA"
			//"key=AAAASLfSzbk:APA91bFCQTYkmuuOeeguuvRA9v9F0oJyRzM0xez3N6xH-W33AQwuSXQ0T5l8cYEJb5F0mb85pt9K0Sp55YeIlb31-X-bFvPOKgg5FA3BFZJzhKPzOIuhcOnHW4tEM16w9wBQ_E8uOXCM"
		})
		.proxy('http://192.168.13.6:3128')
		.send(key)
		.end(function(response) {
			console.log("####################### start data FIREBASE ########################")
			console.log(key)
			console.log(response.error);
			console.log(response.code);
			console.log(response.body);
			console.log("####################### end data FIREBASE ########################")
			if (response.error) return callback({
				status: response.statusCode,
				code: response.code,
				message: response.message,
				ori_message: response.message
			});

			response.body.status = response.statusCode;
			callback(null, response.body);
		});
};