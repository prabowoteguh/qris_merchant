"use strict";
const randomstring = require('randomstring');
const moment = require('moment')

exports.reffnum = function() {
    let reffnum = moment().format('YYYYMMDDHHmmss') + randomstring.generate({ length: 6, charset: 'numeric' });
    return reffnum;
};

exports.refbtn = function() {
    let refbtn = moment().format('YYMMDDHHmmss');
    return refbtn;
};

// 12000 ==>> 12.000 
exports.putComma = function(number) {
    return parseInt(number).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ".");
};

// 12000 ==>> Rp. 12.000 
exports.putRP = function(number) {
    return 'Rp ' + parseInt(number).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ".");
};

exports.separatorRp = function(str) {
	str += '';
	var split = str.split('.');

	if (split.length < 2) {
		split.push('00')
	}

	var x = split[0];
	var y = split[1];
	var beforeComma = parseInt(x).toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ".")
	var afterComma = y[1];

	return 'Rp ' + beforeComma + ',' + afterComma;
}