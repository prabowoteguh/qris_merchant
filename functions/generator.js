"use strict";

const vascommkit = require('vascommkit');
const moment = require('moment');
const md5 = require('md5');

exports.generateMerchantId = function (merchantName) {
	const str = merchantName.replace(/\s+/g, '').toUpperCase();
	const consonan = str.replace(/[aAiIuUeEoO]/g, '');
	const id = consonan.slice(0, 4);

	return id;
};

exports.generateMerchantIdFull = function (merchantName) {
	const str = merchantName.replace(/\s+/g, '').toUpperCase();
	const consonan = str.replace(/[aAiIuUeEoO]/g, '');
	const id = consonan.slice(0, 4);

	return id + vascommkit.string.randomNumeric(4);
};

exports.generateSessionId = function () {
	return vascommkit.string.random(32);
};

exports.generateInvoice = function (merchantId, id) {
	const datetime =  moment().format('YYYYMMDDHHmmss');
	const invoice = '' + merchantId + datetime + id;

	return invoice;
};

exports.generateReffnum = function () {
	const datetime =  moment().format('YYYYMMDDHHmmss');
	const invoice = '' + datetime + vascommkit.string.randomNumeric(6);
	return invoice;
};
exports.generateJournal = function () {
	const datetime =  moment().format('YYMMDDHHmm');
	const invoice = '' + datetime + vascommkit.string.randomNumeric(2);
	return invoice;
};

exports.generatePassword = function (password) {
	const psswd = password ? password : vascommkit.string.random(8);

	return {
		password: psswd,
		encrypted: md5(psswd)
	};
};

exports.generateVA = function (num) {
	return String(process.env.VA_PREFIX) + String(Number(num));
};

exports.bundleVa = function (APP, va, callback) {
	var validateVA = APP.validation.phoneNumber(va);

	if (validateVA !== true) {
			validateVA = APP.validation.va(va);

			if (validateVA !== true) return callback({
					code: 'INVALID_VA',
					data: {
						emoney_va: va
					}
				});

			return callback(null, {
				code: 'VALID_VA',
				data: {
					emoney_va: va
				}
			});
		}

	// If phone number.
	var va = module.exports.generateVA(va);

	return callback(null, {
		code: 'VALID_VA',
		data: {
			emoney_va: va
		}
	});
};