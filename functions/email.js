'use strict';

var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
    // Mail Trap
    // host: "smtp.mailtrap.io",
    // port: 2525,
    // auth: {
    //     user: "63032741af4417",
    //     pass: "cda50c63ee5548"
    // }
    // End Mail Trap

    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    //secure: false, // secure:true for port 465, secure:false for port 587
    secure: true,
    auth: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASSWORD
    },
    tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
    },
    debug: true
});

function log(req, recepient, subject, message, status) {
    let payload = {
        endpoint: req.originalUrl,
        email: recepient,
        subject: subject,
        body: message,
        status: status
    };

    req.APP.models.mongo.log_email.create(payload, (err, rows) => {
        console.log(err)
        console.log(rows)
    });
};

exports.send = function(req, payload, callback) {
    var output = new Object;

    var mailOptions = {
        from: '"' + process.env.MAIL_NAME + '" <' + process.env.MAIL_USER + '>',
        to: payload.body.email,
        subject: payload.body.subject,
        html: payload.body.body_email,
    };

    if (payload.body.path != undefined) {
        mailOptions.attachments = [{
            path: payload.body.path
        }]
    }
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        console.log('!!!!!!!!!!!!!!!!!!!---- Start Send Email ----!!!!!!!!!!!!!!!!!!!')
        console.log(mailOptions.subject)
        console.log(error)
        console.log(info)
        console.log('!!!!!!!!!!!!!!!!!!!---- End Send Email ----!!!!!!!!!!!!!!!!!!!')
        if (error) {
            output = {
                code: error.code,
                message: error.message
            }
            log(req, payload.body.email, payload.body.subject, payload.body.body_email, "ERROR - " + JSON.stringify(error))
            callback(output);
        } else {
            output = {
                code: '00',
                message: info.messageId,
                data: info
            }
            log(req, payload.body.email, payload.body.subject, payload.body.body_email, "OK - " + info.messageId)
            callback(null, output);
        }
    });
}