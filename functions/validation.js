"use strict";

const validateJS = require('validate.js');

exports.true = function (str) {
	return true;
};

exports.username = function (str) {
	return (!str || str == '' || str.length < 4 || typeof str !== 'string' || / /g.test(str) || str.length > 20)
		? {
			code: 'INVALID_REQUEST',
			data: {
				invalid_parameter: 'username'
			}
		} : true;
};

exports.string = function (str, key) {
	return (!str || str == '' || typeof str !== 'string')
		? {
			code: 'INVALID_REQUEST',
			data: {
				invalid_parameter: key
			}
		} : true;
};

exports.email = function(string)
    {
        // Validate the email string.
        if(new RegExp("[a-zA-Z0-9]+[@][a-zA-Z0-9]+[.][a-zA-Z0-9]+").test(string) === false
        || string.length > 40 || string.length < 6 || Object.prototype.toString.call(string) !== '[object String]')
        {
            // Return false if the string is not valid.
            return { code: 'INVALID_REQUEST', data: { invalidParameter: 'Email' } };
        }

        // Return true if everything is ok.
        return true;
    },

exports.name = function (str) {
	return (!str || str == '' || typeof str !== 'string')
		? {
			code: 'INVALID_REQUEST',
			data: {
				invalid_parameter: 'username'
			}
		} : true;
};

exports.email = function (str) {
	return (!str || str == '' || str.length < 5 || typeof str !== 'string' || !/^(([^<>()[\].,;:\s@"]+(.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i.test(str) || str.length > 80)
		? {
			code: 'INVALID_REQUEST',
			data: {
				invalid_parameter: 'email'
			}
		} : true;
};

exports.password = function (str) {
	return (!str || str == '' || str.length < 3 || (str && typeof str !== 'string') || / /g.test(str))
		? {
			code: 'INVALID_REQUEST',
			data: {
				invalid_parameter: 'password'
			}
		} : true;
};

exports.phoneNumber = function (num) {
	return (num == '' || num.length < 5 || num == undefined || (num && / /g).test(num) || (num && num.length > 13))
		? { code: 'INVALID_REQUEST', data: { invalidParameter: 'Phone Number' } } : true;
};

exports.va = function (num) {
	return (num == '' || num == undefined || (num && / /g).test(num) || (num && String(num).slice(0, process.env.VA_PREFIX_LENGTH) !== process.env.VA_PREFIX))
		? { code: 'INVALID_REQUEST', data: { invalidParameter: 'VA Number' } } : true;
};

exports.amount = function (num) {
	return (num == '' || num == undefined  || !Number(num) || num <= 0 || !parseInt(num, 10))
		? { code: 'NOMINAL_NOT_VALID', data: { invalidParameter: 'Amount' } } : true;
};

exports.date = function (dateString) {

	if(dateString == null || dateString == '' || dateString == '0'){
		return true;
	}
	var regEx = /^\d{4}-\d{2}-\d{2}$/;
	if(!dateString.match(regEx)) return { code: 'INVALID_REQUEST', data: { invalidParameter: 'Date' } };  // Invalid format
	var d = new Date(dateString);
	if(Number.isNaN(d.getTime())) return { code: 'INVALID_REQUEST', data: { invalidParameter: 'Date' } }; // Invalid date
	return d.toISOString().slice(0,10) === dateString ? true : { code: 'INVALID_REQUEST', data: { invalidParameter: 'Date' } };
};
