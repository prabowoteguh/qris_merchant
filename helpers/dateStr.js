"use strict";

exports.reserveDate = (date) => {
    return date.split('-').reverse().join('-');
}

exports.parse = (date) => {
    var spl = date.split(' ');
    var splOne = spl[0];
    var getOne = splOne.split('-');
    var getDate = getOne[0];
    var getMonth = getOne[1];
    var getYear = getOne[2];
    var monthStr = null;

    switch(getMonth) {
        case '01': {
            monthStr = getDate.concat(' Januari ');
            break;
        }
        case '02': {
            monthStr = getDate.concat(' Februari ');
            break;
        }
        case '03': {
            monthStr = getDate.concat(' Maret ');
            break;
        }
        case '04': {
            monthStr = getDate.concat(' April ');
            break;
        }
        case '05': {
            monthStr = getDate.concat(' Mei ');
            break;
        }case '06': {
            monthStr = getDate.concat(' Juni ');
            break;
        }
        case '07': {
            monthStr = getDate.concat(' Juli ');
            break;
        }
        case '08': {
            monthStr = getDate.concat(' Agustus ');
            break;
        }
        case '09': {
            monthStr = getDate.concat(' September ');
            break;
        }
        case '10': {
            monthStr = getDate.concat(' Oktober ');
            break;
        }
        case '11': {
            monthStr = getDate.concat(' November ');
            break;
        }
        case '12': {
            monthStr = getDate.concat(' Desember ');
            break;
        }
        default: {
            monthStr = getDate.concat(' Bulan ');
        }
    }

    return monthStr.concat(getYear);
}

exports.parseISOString = (dateSt) => {
    var date = new Date(dateSt);
    var year = date.getFullYear();
    var month = date.getMonth()+1;
    var dt = date.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }

    return dt+'-' + month + '-'+year
}

exports.parseAbbreviation = (date) => {
    var spl = date.split(' ');
    var splOne = spl[0];
    var getOne = splOne.split('-');
    var getDate = getOne[0];
    var getMonth = getOne[1];
    var getYear = getOne[2];
    var monthStr = null;

    switch(getMonth) {
        case '01': {
            monthStr = getDate.concat(', Jan ');
            break;
        }
        case '02': {
            monthStr = getDate.concat(', Feb ');
            break;
        }
        case '03': {
            monthStr = getDate.concat(', Mar ');
            break;
        }
        case '04': {
            monthStr = getDate.concat(', Apr');
            break;
        }
        case '05': {
            monthStr = getDate.concat(', Mei ');
            break;
        }case '06': {
            monthStr = getDate.concat(', Jun ');
            break;
        }
        case '07': {
            monthStr = getDate.concat(', Jul ');
            break;
        }
        case '08': {
            monthStr = getDate.concat(', Agu ');
            break;
        }
        case '09': {
            monthStr = getDate.concat(', Sep ');
            break;
        }
        case '10': {
            monthStr = getDate.concat(', Okt ');
            break;
        }
        case '11': {
            monthStr = getDate.concat(', Nov ');
            break;
        }
        case '12': {
            monthStr = getDate.concat(', Des ');
            break;
        }
        default: {
            monthStr = getDate.concat(', Bulan ');
        }
    }

    return monthStr.concat(getYear);
}

exports.parsePeriode = (date) => {
    const b = date.split('-');
    const month = b[0];
    var year = b[1];
    var monthStr = null;

    switch(month) {
        case '01': {
            monthStr = 'Januari ';
            break;
        }
        case '02': {
            monthStr = 'Februari ';
            break;
        }
        case '03': {
            monthStr = 'Maret ';
            break;
        }
        case '04': {
            monthStr = 'April ';
            break;
        }
        case '05': {
            monthStr = 'Mei ';
            break;
        }case '06': {
            monthStr = 'Juni ';
            break;
        }
        case '07': {
            monthStr = 'Juli ';
            break;
        }
        case '08': {
            monthStr = 'Agustus ';
            break;
        }
        case '09': {
            monthStr = 'September ';
            break;
        }
        case '10': {
            monthStr = 'Oktober ';
            break;
        }
        case '11': {
            monthStr = 'November ';
            break;
        }
        case '12': {
            monthStr = 'Desember ';
            break;
        }
        default: {
            monthStr = 'Periode tidak sesuai ';
        }
    }

    return monthStr.concat(year);
}
