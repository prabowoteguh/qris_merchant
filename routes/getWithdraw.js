"use strict";

const express = require('express');
const router = express.Router();

router.post('/', (req, res, next) => {
	const getWithdraw = require('../controllers/getWithdraw.js');

	getWithdraw(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;