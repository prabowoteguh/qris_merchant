"use strict";

const express = require('express');
const router = express.Router();

router.post('/', (req, res, next) => {
	const getMembership = require('../controllers/getMembership.js');
	getMembership(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/add', (req, res, next) => {
	const addMembership = require('../controllers/addMembership.js');
	addMembership(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/revoke', (req, res, next) => {
	const revokeMembership = require('../controllers/revokeMembership.js');
	revokeMembership(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;