"use strict";

const express = require('express');
const router = express.Router();

router.post('/merchant', (req, res, next) => {
	const merchant = require('../controllers/transfer/merchant.js');

	merchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;