"use strict";

const express = require('express');
const router = express.Router();
const testController = require('../controllers/testController.js');

router.post('/connection', (req, res, next) => {
	return req.APP.output.print(req, res, {
		code: 'OK'
	});
});

router.post('/services', (req, res, next) => {
	if (process.env.NODE_ENV == 'production') return res.json({
			hello: 'Something went wrong!'
		});
		
	testController.services(req.APP, req, (err, result) => {
		if (err) return res.send(err);

		return res.send(result);
	});
});

module.exports = router;