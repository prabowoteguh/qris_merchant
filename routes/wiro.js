"use strict";

const express = require('express');
const router = express.Router();
const testController = require('../controllers/testController.js');
const certReader = require('../functions/certReader.js');
const hitter = require('../functions/request.js');
var RSA_SHA = require('RSA-SHA');
const messages = require('../config/messages.json');

router.post('/encrypt-request', (req, res, next) => {
	console.log(process.env.WEB_CERT_REQUEST)
	certReader.read(__dirname+'/../storage/cert/web/private.pem', process.env.WEB_CERT_REQUEST,  (err, output) => {
		console.log(output.publicKey)
		var encypted = RSA_SHA.encryptRSA(JSON.stringify(req.body), output.publicKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : encypted
		});
	});
	
});

router.post('/decrypt-request', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/web/private.pem', process.env.WEB_CERT_REQUEST,  (err, output) => {
		var decrypt = RSA_SHA.decryptRSA((req.body), output.privateKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : JSON.parse(decrypt)
		});
	});
});

router.post('/encrypt-response', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/web/privResponse.pem', process.env.WEB_CERT_RESPONSE,  (err, output) => {
		var encypted = RSA_SHA.encryptRSA(JSON.stringify(req.body), output.publicKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : encypted
		});
	});
	
});

router.post('/decrypt-response', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/web/privResponse.pem', process.env.WEB_CERT_RESPONSE,  (err, output) => {
		console.log(output.privateKey)
		var decrypt = RSA_SHA.decryptRSA((req.body), output.privateKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : JSON.parse(decrypt)
		});
	});
});

router.post('/create-signature', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/web/privSignature.pem', process.env.WEB_CERT_SIGNATURE,  (err, output) => {
		console.log(output.privateKey)
		var signed = RSA_SHA.signSHA(JSON.stringify(req.body), output.privateKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : signed
		});
	});
	
});

router.post('/verify-signature', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/web/privSignature.pem', process.env.WEB_CERT_SIGNATURE,  (err, output) => {
		var verify = RSA_SHA.verifySHA(JSON.stringify(req.body.request), req.body.signature, output.publicKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : verify
		});
	});
});



// {
// 	request 	: {"va" : "5557", bank: "xxxx"},
// 	signature 	: base64
// 	module    	: 'checkAccount',
// 	client_id   : 'nizar'
// }

router.post('/v1/translate', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/web/private.pem', process.env.WEB_CERT_REQUEST,  (err, outputPrivReq) => {
		req.body = JSON.parse(RSA_SHA.decryptRSA((req.body), outputPrivReq.privateKey));

		certReader.read(__dirname+'/../storage/cert/web/privSignature.pem', process.env.WEB_CERT_SIGNATURE,  (err, outputPrivSign) => {

			var verify = RSA_SHA.verifySHA(JSON.stringify(req.body.request), req.body.signature, outputPrivSign.publicKey);
			if(verify == true){

				switch(req.body.module) {
				    case 'register':
				        req.body.module = '/users/register'
				        break;
				    case 'login':
				        req.body.module = '/users/login'
				        break;
				    case 'OTPCreate':
				        req.body.module = '/otp/create'
				        break;
				    case 'OTPVerify':
				        req.body.module = '/otp/verify'
				        break;
				    default:
				        req.body.module = req.body.module
				}


				hitter.post(process.env.BPC_HOST+req.body.module, req.body.request, null, (err, output) => {
					certReader.read(__dirname+'/../storage/cert/web/privResponse.pem', process.env.WEB_CERT_RESPONSE,  (err, cert) => {
						console.log(output)
						if(output.code == '00'){
							let response = {}
							let message = {
								company: messages['OK']
							};
							response.code = message.company.code;
							response.message = message.company.message;
							response.data = output || message.company.plain;

							var encypted = RSA_SHA.encryptRSA(JSON.stringify(response), cert.publicKey);
							return req.APP.output.print(req, res, {
								code : 'OK', 
								data : encypted,
								plain : output
							});
						}else{
							let response = {}
							response.code = output.code;
							response.message = output.message;
							response.data = output.data;

							var encypted = RSA_SHA.encryptRSA(JSON.stringify(response), cert.publicKey);
							return req.APP.output.print(req, res, {
								code : 'OK', 
								data : encypted,
								plain : response
							});
						}
						
					});
				});
			}else{
				certReader.read(__dirname+'/../storage/cert/web/privResponse.pem', process.env.WEB_CERT_RESPONSE,  (err, cert) => {
					let response = {}
					let message = {
						company: messages['SIGNATURE_FALSE']
					};
					response.code = message.company.code;
					response.message = message.company.message;
					response.data = message.company.plain;

					var encypted = RSA_SHA.encryptRSA(JSON.stringify(response), cert.publicKey);
					return req.APP.output.print(req, res, {
						code : 'SIGNATURE_FALSE', 
						data : encypted,
						plain : message.company.plain
					});
				});
			}
		});
	});
});

module.exports = router;

