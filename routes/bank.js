"use strict";

const express = require('express');
const router = express.Router();

router.post('/', (req, res, next) => {
	const masterBank = require('../controllers/qrisManagement/masterBank.js');

	masterBank.listBankCode(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/inquiry', (req, res, next) => {
	const masterBank = require('../controllers/qrisManagement/masterBank.js');

	masterBank.inquiryBankCode(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/update/globalUniqueIdentifier', (req, res, next) => {
	const masterBank = require('../controllers/qrisManagement/masterBank.js');

	masterBank.updateBankGlobalUniqueIdentifier(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;
