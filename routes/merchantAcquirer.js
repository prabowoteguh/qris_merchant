"use strict";

const express = require('express');
const router = express.Router();

router.post('/', (req, res, next) => {
	const merchantAcquirer = require('../controllers/qrisManagement/merchantAcquirer.js');

	merchantAcquirer.listMerchantAcquirer(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/add', (req, res, next) => {
	const merchantAcquirer = require('../controllers/qrisManagement/merchantAcquirer.js');

	merchantAcquirer.addMerchantAcquirer(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/update', (req, res, next) => {
	const merchantAcquirer = require('../controllers/qrisManagement/merchantAcquirer.js');

	merchantAcquirer.updateMerchantAcquirer(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/delete', (req, res, next) => {
	const merchantAcquirer = require('../controllers/qrisManagement/merchantAcquirer.js');

	merchantAcquirer.deleteMerchantAcquirer(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;
