"use strict";

const express = require('express');
const router = express.Router();

router.post('/requestPromo', (req, res, next) => {
	const requestPromo = require('../controllers/promo/requestPromo.js');

	requestPromo(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/approvePromo', (req, res, next) => {
	const approvePromo = require('../controllers/promo/approvePromo.js');

	approvePromo(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/getPromoRequest', (req, res, next) => {
	const getPromoRequest = require('../controllers/promo/getPromoRequest.js');

	getPromoRequest(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/disablePromo', (req, res, next) => {
	const disablePromo = require('../controllers/promo/disablePromo.js');

	disablePromo(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;