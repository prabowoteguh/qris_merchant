"use strict";

const express = require('express');
const router = express.Router();

router.post('/get', (req, res, next) => {
	const getProduct = require('../controllers/getProduct.js');

	getProduct(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/getType', (req, res, next) => {
	const getProductType = require('../controllers/getProductType.js');

	getProductType(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/create', (req, res, next) => {
	const createProduct = require('../controllers/createProduct.js');

	createProduct(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/createType', (req, res, next) => {
	const createProductType = require('../controllers/createProductType.js');

	createProductType(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/update', (req, res, next) => {
	const updateProduct = require('../controllers/updateProduct.js');

	updateProduct(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/updateType', (req, res, next) => {
	const updateProductType = require('../controllers/updateProductType.js');

	updateProductType(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/delete', (req, res, next) => {
	const deleteProduct = require('../controllers/deleteProduct.js');

	deleteProduct(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/deleteType', (req, res, next) => {
	const deleteProductType = require('../controllers/deleteProductType.js');

	deleteProductType(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;