"use strict";

const express = require('express');
const router = express.Router();

router.post('/flag', (req, res, next) => {
	const payment = require('../controllers/qrisManagement/paymentMid.js');

	payment.flaggingPayment(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/update', (req, res, next) => {
	const payment = require('../controllers/qrisManagement/paymentUpdate');

	payment(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/log/list', (req, res, next) => {
	const payment = require('../controllers/qrisManagement/paymentMid.js');

	payment.getlogPayment(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;