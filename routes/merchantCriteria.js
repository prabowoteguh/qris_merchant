"use strict";

const express = require('express');
const router = express.Router();

router.post('/', (req, res, next) => {
	const merchantCriteria = require('../controllers/qrisManagement/merchantCriteria.js');

	merchantCriteria.listMerchantCriteria(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;
