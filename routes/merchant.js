"use strict";

const express = require('express');
const router = express.Router();

router.post('/getProfile', (req, res, next) => {
	const getProfile = require('../controllers/getProfile.js');

	getProfile(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/getListMerchant', (req, res, next) => {
	const getListMerchant = require('../controllers/getListMerchant.js');

	getListMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/getListUserMerchant', (req, res, next) => {
	const getListUserMerchant = require('../controllers/getListUserMerchant.js');

	getListUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/createUserMerchant', (req, res, next) => {
	const createUserMerchant = require('../controllers/createUserMerchant.js');

	createUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/updateProfile', (req, res, next) => {
	const updateProfile = require('../controllers/updateProfile.js');

	updateProfile(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/updateUserMerchant', (req, res, next) => {
	const updateUserMerchant = require('../controllers/updateUserMerchant.js');

	updateUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/updateStatus', (req, res, next) => {
	const updateStatus = require('../controllers/updateStatus.js');

	updateStatus(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/approval', (req, res, next) => {
	const approvalMerchant = require('../controllers/approvalMerchant.js');
	approvalMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);
		return req.APP.output.print(req, res, result);
	});
});

router.post('/reject', (req, res, next) => {
	const rejectMerchant = require('../controllers/rejectMerchant.js');
	rejectMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);
		return req.APP.output.print(req, res, result);
	});
});

router.post('/updateStatusUserMerchant', (req, res, next) => {
	const updateStatusUserMerchant = require('../controllers/updateStatusUserMerchant.js');

	updateStatusUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/disableUserMerchant', (req, res, next) => {
	const disableUserMerchant = require('../controllers/disableUserMerchant.js');

	disableUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/activeUserMerchant', (req, res, next) => {
	const activeUserMerchant = require('../controllers/activeUserMerchant.js');

	activeUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/stats', (req, res, next) => {
	const stats = require('../controllers/stats.js');

	stats(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/statsDetail', (req, res, next) => {
	const stats = require('../controllers/statsDetail.js');

	stats(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/getBusinessType', (req, res, next) => {
	const getBusinessType = require('../controllers/getBusinessType.js');

	getBusinessType(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});


router.post('/updatePasswordUserMerchant', (req, res, next) => {
	const updatePasswordUserMerchant = require('../controllers/updatePasswordUserMerchant.js');

	updatePasswordUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/promo', (req, res, next) => {
	const getPromo = require('../controllers/getPromo.js');
	getPromo(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/otp/create', (req, res, next) => {
	const createOTP = require('../controllers/createOTP.js');
	createOTP(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);
		return req.APP.output.print(req, res, result);
	});
});

router.post('/otp/verify', (req, res, next) => {
	const verifyOTP = require('../controllers/verifyOTP.js');
	verifyOTP(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);
		return req.APP.output.print(req, res, result);
	});
});

router.post('/resetPasswordUserMerchant', (req, res, next) => {
	const resetPasswordUserMerchant = require('../controllers/resetPasswordUserMerchant.js');
	resetPasswordUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/devicekey', (req, res, next) => {
	const getDeviceKeyMerchant = require('../controllers/getDeviceKeyMerchant.js');
	getDeviceKeyMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);
		return req.APP.output.print(req, res, result);
	});
});

router.post('/checkHp', (req, res, next) => {
	const checkHp = require('../controllers/checkHp.js');
	checkHp(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);
		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;