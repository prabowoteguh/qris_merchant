"use strict";

const express = require('express');
const router = express.Router();
const testController = require('../controllers/testController.js');
const certReader = require('../functions/certReader.js');
const hitter = require('../functions/request.js');
var RSA_SHA = require('RSA-SHA');
const messages = require('../config/messages.json');

router.post('/encrypt-request', (req, res, next) => {
	console.log(process.env.MOBILE_CERT_REQUEST)
	certReader.read(__dirname+'/../storage/cert/mobile/private.pem', process.env.MOBILE_CERT_REQUEST,  (err, output) => {
		console.log(output.publicKey)
		var encypted = RSA_SHA.encryptRSA(JSON.stringify(req.body), output.publicKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : encypted
		});
	});
	
});

router.post('/decrypt-request', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/mobile/private.pem', process.env.MOBILE_CERT_REQUEST,  (err, output) => {
		var decrypt = RSA_SHA.decryptRSA((req.body), output.privateKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : JSON.parse(decrypt)
		});
	});
});

router.post('/encrypt-response', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/mobile/privResponse.pem', process.env.MOBILE_CERT_RESPONSE,  (err, output) => {
		var encypted = RSA_SHA.encryptRSA(JSON.stringify(req.body), output.publicKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : encypted
		});
	});
	
});

router.post('/decrypt-response', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/mobile/privResponse.pem', process.env.MOBILE_CERT_RESPONSE,  (err, output) => {
		console.log(output.privateKey)
		var decrypt = RSA_SHA.decryptRSA((req.body), output.privateKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : JSON.parse(decrypt)
		});
	});
});

router.post('/create-signature', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/mobile/privSignature.pem', process.env.MOBILE_CERT_SIGNATURE,  (err, output) => {
		console.log(output.privateKey)
		var signed = RSA_SHA.signSHA(JSON.stringify(req.body), output.privateKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : signed
		});
	});
	
});

router.post('/verify-signature', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/mobile/privSignature.pem', process.env.MOBILE_CERT_SIGNATURE,  (err, output) => {
		var verify = RSA_SHA.verifySHA(JSON.stringify(req.body.request), req.body.signature, output.publicKey);
		return req.APP.output.print(req, res, {
			code : 'OK', 
			data : verify
		});
	});
});


// {
// 	request 	: {"va" : "5557", bank: "xxxx"},
// 	signature 	: base64
// 	module    	: 'checkAccount',
// 	client_id   : 'nizar'
// }

router.post('/v1/translate', (req, res, next) => {
	certReader.read(__dirname+'/../storage/cert/mobile/private.pem', process.env.MOBILE_CERT_REQUEST,  (err, outputPrivReq) => {
		req.body = JSON.parse(RSA_SHA.decryptRSA((req.body), outputPrivReq.privateKey));
		certReader.read(__dirname+'/../storage/cert/mobile/privSignature.pem', process.env.MOBILE_CERT_SIGNATURE,  (err, outputPrivSign) => {
			console.log(req.body)
			var verify = RSA_SHA.verifySHA(JSON.stringify(req.body.request), req.body.signature, outputPrivSign.publicKey);
			if(verify == true){

				switch(req.body.module) {
				    case 'register':
				        req.body.module = '/users/register'
				        break;
				    case 'login':
				        req.body.module = '/users/login'
				        break;
				    case 'OTPCreate':
				        req.body.module = '/otp/create'
				        break;
				    case 'OTPVerify':
				        req.body.module = '/otp/verify'
				        break;
				    case 'AccountCheck':
				        req.body.module = '/emoney/checkAccount'
				        break;
				    case 'PasswordChange':
				        req.body.module = '/users/password/change'
				        break;
				    case 'PasswordReset':
				        req.body.module = '/users/password/reset'
				        break;
				    case 'PinChange':
				        req.body.module = '/users/pin/change'
				        break;
				    case 'PinReset':
				        req.body.module = '/users/pin/reset'
				        break;
				    case 'ProfileUpdate':
				        req.body.module = '/users/profile/update'
				        break;
				    case 'Profile':
				        req.body.module = '/users/profile'
				        break;
				    case 'Logout':
				        req.body.module = '/users/logout'
				        break;
				    default:
				        req.body.module = req.body.module
				}

				let options = {}
				if(req.get('X-BPC-APP') != '' || typeof req.get('X-BPC-APP') !== 'undefined'){
					options.headers = {
						session_id : req.get('X-BPC-APP')
					}
				} 
				hitter.post(process.env.BPC_HOST+req.body.module, req.body.request, options, (err, output) => {
					certReader.read(__dirname+'/../storage/cert/mobile/privResponse.pem', process.env.MOBILE_CERT_RESPONSE,  (err, cert) => {
						console.log(output)
						if(output.code == '00'){
							let response = {}
							response.code = output.code;
							response.message = output.message;
							response.data = output.data || message.company.plain;

							var encypted = RSA_SHA.encryptRSA(JSON.stringify(response), cert.publicKey);
							return req.APP.output.print(req, res, {
								code : 'OK', 
								data : encypted,
								plain : response
							});
						}else{
							let response = {}
							response.code = output.code;
							response.message = output.message;
							response.data = output.data;

							var encypted = RSA_SHA.encryptRSA(JSON.stringify(response), cert.publicKey);
							return req.APP.output.print(req, res, {
								code : 'OK', 
								data : encypted,
								plain : response
							});
						}
						
					});
				});
			}else{
				certReader.read(__dirname+'/../storage/cert/mobile/privResponse.pem', process.env.MOBILE_CERT_RESPONSE,  (err, cert) => {
					let response = {}
					let message = {
						company: messages['SIGNATURE_FALSE']
					};
					response.code = message.company.code;
					response.message = message.company.message;
					response.data = message.company.plain;

					var encypted = RSA_SHA.encryptRSA(JSON.stringify(response), cert.publicKey);
					return req.APP.output.print(req, res, {
						code : 'SIGNATURE_FALSE', 
						data : encypted,
						plain : message.company.plain
					});
				});
			}
		});
	});
});

module.exports = router;