"use strict";

const express = require('express');
const router = express.Router();

router.post('/transactionChart', (req, res, next) => {
	const transactionChart = require('../controllers/transactionChart.js');

	transactionChart(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/totalIncome', (req, res, next) => {
	const totalIncome = require('../controllers/totalIncome.js');

	totalIncome(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/topProduct', (req, res, next) => {
	const topProduct = require('../controllers/topProduct.js');

	topProduct(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;