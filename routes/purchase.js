"use strict";

const express = require('express');
const router = express.Router();

router.post('/inquiry', (req, res, next) => {
	const purchaseInquiry = require('../controllers/purchaseInquiry.js');

	purchaseInquiry(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/invoiceInquiry', (req, res, next) => {
	const purchaseInvoiceInquiry = require('../controllers/purchaseInvoiceInquiry.js');

	purchaseInvoiceInquiry(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/payment', (req, res, next) => {
	const purchasePayment = require('../controllers/purchasePayment.js');

	purchasePayment(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/cancel', (req, res, next) => {
	const purchaseCancel = require('../controllers/purchaseCancel.js');

	purchaseCancel(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/poinTemp', (req, res, next) => {
	const purchasePoinTemp = require('../controllers/purchasePoinTemp.js');

	purchasePoinTemp(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/reversal', (req, res, next) => {
	const purchaseReversal = require('../controllers/purchaseReversal.js');

	purchaseReversal(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;