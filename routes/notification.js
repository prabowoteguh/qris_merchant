"use strict";

const express = require('express');
const router = express.Router();

router.post('/get', (req, res, next) => {
	const getNotification = require('../controllers/getNotification.js');

	getNotification(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;