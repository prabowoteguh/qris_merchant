"use strict";

const express = require('express');
const router = express.Router();

router.post('/user', (req, res, next) => {
	const statsUserMerchant = require('../controllers/statsUserMerchant.js');
	statsUserMerchant(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;