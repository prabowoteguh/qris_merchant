"use strict";

const express = require('express');
const router = express.Router();

router.post('/get', (req, res, next) => {
	const getTransaction = require('../controllers/getTransaction.js');

	getTransaction(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/getDetail', (req, res, next) => {
	const getTransactionDetail = require('../controllers/getTransactionDetail.js');

	getTransactionDetail(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;