"use strict";

const express = require('express');
const router = express.Router();

router.post('/generate', (req, res, next) => {
	const generateQr = require('../controllers/qrisManagement/generateQr.js');

	generateQr(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

router.post('/read', (req, res, next) => {
	const readQr = require('../controllers/qrisManagement/readQr.js');

	readQr(req.APP, req, (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;
