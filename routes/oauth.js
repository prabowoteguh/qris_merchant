"use strict";

const express = require('express');
const router = express.Router();
const async = require('async');
const md5 = require('md5');
const OAuthServer = require('oauth2-server');
const Request = OAuthServer.Request;
const Response = OAuthServer.Response;

router.post('/token', (req, res, next) => {
	if (!req.headers.authorization || !req.body.client_id || !req.body.client_secret) return req.APP.output.print(req, res, {
      code: 'INVALID_REQUEST',
      data: {}
    });

   const buffer = 'Basic ' + new Buffer(req.body.client_id + ':' + req.body.client_secret).toString('base64');

   if (buffer !== req.headers.authorization) return req.APP.output.print(req, res, {
      code: 'ERR_ACCESS',
      data: {}
    });

	let request = new Request(req);
	let response = new Response(req);
	const options = new Object;

	req.APP.oauth.token(request, response, options).then((result) => {
		req.clientId = result.client.id;

    return req.APP.output.print(req, res, {
      code: 'ACCESS_TOKEN_GENERATED',
      data: {
        token_type: 'bearer',
        expires_in: result.accessTokenExpiresAt,
        access_token: result.accessToken,
        refresh_token: result.refreshToken,
        user: result.user
      }
    });
	}).catch((err) => {
		return req.APP.output.print(req, res, {
	      code: 'ERR_ACCESS',
	      data: err
	    });
	});
});


router.post('/register', (req, res, next) => {
	
	if (process.env.NODE_ENV == 'production' ) return req.APP.output.print(req, res, {
			code: 'SERVICE_NOT_FOUND',
			data: {}
		});
	
	async.waterfall([
		function validations (callback) {
			if (!req.body.name) return callback({
		      code: 'MISSING_KEY',
		      data: {
		      	missingKey: 'name'
		      }
		    });

			if (!req.body.clientId) return callback({
		      code: 'MISSING_KEY',
		      data: {
		      	missingKey: 'clientId'
		      }
		    });

		   if (!req.body.clientSecret) return callback({
		      code: 'MISSING_KEY',
		      data: {
		      	missingKey: 'clientSecret'
		      }
		    });

		   if (!req.body.grants) return callback({
		      code: 'MISSING_KEY',
		      data: {
		      	missingKey: 'grants'
		      }
		    });

		  callback(null, true);
		},
		function checkingClientExists (index, callback) {
			var params = req.APP.queries.select('client', {
				body: {
					clientId: req.body.clientId
				}
			}, req.APP.models);

			req.APP.models.mongo.client.find(params).limit(1).skip(0).sort({
				_id: -1
			}).lean().exec((err, rows) => {
			  if (err) return callback({
						code: 'ERR_DATABASE',
						data: JSON.stringify(err)
					});

				callback(null, {
					code: (rows && (rows.length > 0)) ? 'FOUND' : 'NOT_FOUND',
					data: rows,
					info: {
						dataCount: rows.length
					}
				});
			});
		},
		function insertingData (client, callback) {
			if (client.code === 'FOUND') return callback({
					code: 'CLIENT_ALREADY_EXISTS'
				});

			// var params = req.APP.queries.insert('client', {
			// 	body: {
			// 		name: req.body.name,
			// 		clientId: req.body.clientId,
			//     	clientSecret: md5(req.body.clientSecret),
			//     	grants: req.body.grants
			// 	}
			// }, req.APP.models);

			req.APP.models.mongo.client.create({
					name: req.body.name,
					clientId: req.body.clientId,
			    	clientSecret: md5(req.body.clientSecret),
			    	grants: req.body.grants
			}, (err, result) => {
		    if (err) return callback({
						code: 'ERR_DATABASE',
						data: JSON.stringify(err)
					});

		    return callback(null, {
		    	code: 'CLIENT_REGISTER_SUCCESS',
		    	data: {
		    		name: req.body.name,
						clientId: req.body.clientId,
				    clientSecret: md5(req.body.clientSecret),
				    grants: req.body.grants
		    	}
		    });
		  });
		}
	], (err, result) => {
		if (err) return req.APP.output.print(req, res, err);

		return req.APP.output.print(req, res, result);
	});
});

module.exports = router;